#include "factory_settings.h"
#include "km_error.h"
#include "nrf_drv_rng.h"
#include "spi_flash_s25fl.h"
#include "flash_address_definition.h"
#include "motor_control.h"

#define NRF_LOG_MODULE_NAME "FA"
#include "nrf_log.h"
#include "nrf_log_ctrl.h" 

#include "nrf_delay.h"

// 電源起動時チェックし、MAGIC WORD が書き込まれていなければ初期設定モードになる
#define MAGIC_WORD 0x37

#define SERIAL_NUMBER_SIZE    12      /**< Random numbers buffer size. */
#define LETTERS_NUMBER 36

/** @brief Function for getting vector of random numbers.
 *
 * @param[out] p_buff       Pointer to unit8_t buffer for storing the bytes.
 * @param[in]  length       Number of bytes to take from pool and place in p_buff.
 *
 * @retval     Number of bytes actually placed in p_buff.
 */
static uint8_t random_vector_generate(uint8_t * p_buff, uint8_t size)
{
    uint32_t err_code;
    uint8_t  available;

    nrf_drv_rng_bytes_available(&available);
    uint8_t length = MIN(size, available);

    err_code = nrf_drv_rng_rand(p_buff, length);
    APP_ERROR_CHECK(err_code);

    return length;
}


void fa_settings_phase_diff(){
	// TODO 位相調整値を自動で設定する
	motor_enable();
    motor_set_mode(MOTOR_CONTROL_MODE_PHASEDIFF_AUTO);
	NRF_LOG_DEBUG("Auto setting phaseDiff... \r\n");
}


uint32_t fa_settings_serial_number(){

	uint32_t err_code;
	err_code = nrf_drv_rng_init(NULL);
    APP_ERROR_CHECK(err_code);
	
	uint8_t sn[SERIAL_NUMBER_SIZE];
	const char letters[LETTERS_NUMBER] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // 36文字

	/* uint8_t 乱数配列を生成する */
	uint8_t base_number[SERIAL_NUMBER_SIZE];	
	nrf_delay_ms(10); // ないと動作しない
	uint8_t length = random_vector_generate(base_number, SERIAL_NUMBER_SIZE);
	/*
    NRF_LOG_INFO("Random Vector:\r\n");
	NRF_LOG_HEXDUMP_DEBUG(base_number, length); //
	NRF_LOG_DEBUG("\r\n");
	NRF_LOG_FLUSH();
	NRF_LOG_DEBUG("pass:\r\n");
	*/
	
	for(int i = 0; i < SERIAL_NUMBER_SIZE; i++){
		uint8_t index = base_number[i] % LETTERS_NUMBER;
		sn[i] = letters[index];
		//NRF_LOG_DEBUG("%d\r\n", index); // 
		//NRF_LOG_DEBUG("%d: %d\r\n", i, sn[i]); // 
		//NRF_LOG_FLUSH();		 
	}
	NRF_LOG_DEBUG("end. \r\n");
	//NRF_LOG_DEBUG("%d", (char)sn);
    //NRF_LOG_HEXDUMP_DEBUG(sn, SERIAL_NUMBER_SIZE); //

    char *p_my_text = "Testing string";
    NRF_LOG_DEBUG(" %s \r\n", (uint32_t)p_my_text);
    NRF_LOG_FLUSH();	
}


uint32_t fa_settings_check()
{
	//NRF_LOG_DEBUG("Factory auto setting check. 1\r\n");
	
	fa_settings_serial_number();

	/*
	//flash_s25fl_init_with_blocking(); //フラッシュをブロッキングモードで初期化
    uint8_t word;
    flash_s25fl_read(SETTING_ADDR_MAGIC_NUMBER, &word, 1);
	
    if (word == MAGIC_WORD)
    {
		// 初期設定完了済みである（工場出荷設定が完了している）
		NRF_LOG_DEBUG("Factory auto setting is already done. \r\n");
        return KM_SUCCESS;
    }
    else
    {
		NRF_LOG_DEBUG("Factory auto setting is not yet...\r\n");
		// 初期設定を今から行う
        //flash_s25fl_erase_all(); // フラッシュをフォーマット
        fa_settings_serial_number();
        //fa_settings_phase_diff();
    }
	*/

}


