#include "teaching_playback.h"
#include "motor_control.h"
#include "app_timer.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "led_ws2812b.h"
#include "fast_math.h"
#include "SEGGER_RTT.h"

/* 入力保存再生テスト */
//#define RECORD_TEST
#define RECORD_MAX_COUNTS 2000 // 記憶時間 CONTROL_INT_MS * 10 = 2.2 ms ならば、3000 * 2.2 = 6600ms
#define RECORD_INTERVAL_MS 2 /* TODO 2msec に１回記録を行う */
#define PLAYBACK_DEFAULT_SPEED_RPM 200


teaching_playback_mode_t mode = TEACHING_PLAYBACK_MODE_NONE;


/* APP_TIMER */
APP_TIMER_DEF(teaching_playback_timer_id);

/*
記録再生モード
*/
int recording_position[RECORD_MAX_COUNTS] = {0};
int recording_count = 0;
int replay_count = 10;
int rec_timer_count = 0;
int rec_end_count = 0;
int rec_start_position = 0;
int rec_last_position = 0;
bool rec_end_flag = false;


static void timer_start(){
	uint32_t err_code;
    err_code = app_timer_start(teaching_playback_timer_id, APP_TIMER_TICKS(RECORD_INTERVAL_MS), NULL);
    APP_ERROR_CHECK(err_code);
}

static void timer_stop(){
	app_timer_stop(teaching_playback_timer_id);
}

static void record_position()
{
    if (rec_timer_count % 10 == 0)
    {
        if (recording_count == 0)
        {
            //NRF_LOG("0");
            rec_start_position = motor_get_raw_position();
            recording_count += 1;
        }
        else if (rec_end_flag)
        {
            NRF_LOG_INFO("rec_end_flag\n");
            motor_set_mode(MOTOR_CONTROL_MODE_NONE);
            rec_end_count = recording_count;
            recording_count = 0;
        }
        else if (recording_count < RECORD_MAX_COUNTS)
        {
            recording_position[recording_count] = motor_get_raw_position();
			float recorded_pos = TICKS_TO_RADIANS(recording_position[recording_count]);
            NRF_LOG_DEBUG("%d,%d,%d\n", recording_count, recording_position[recording_count], (int)(recorded_pos * 10000));
            recording_count += 1;
        }
        else if (recording_count == RECORD_MAX_COUNTS)
        {
            NRF_LOG_INFO("RECORD_MAX_COUNTS\n");
			NRF_LOG_INFO("Teaching stopped..\r\n");
            motor_set_mode(MOTOR_CONTROL_MODE_NONE);
            rec_end_count = RECORD_MAX_COUNTS;
            recording_count = 0;
            led_ws2812b_drive_set_color(0, 0, 128);
			mode = TEACHING_PLAYBACK_MODE_NONE;
        }
        else
        {
            NRF_LOG_INFO("others\r\n");
        }
    }
}

static void playback_position(){
	
	float target_pos = TICKS_TO_RADIANS(recording_position[replay_count]);
	
	if (replay_count < RECORD_MAX_COUNTS){
		
		 motor_move_to(target_pos, RPM_TO_RADPERSEC(PLAYBACK_DEFAULT_SPEED_RPM));
		 NRF_LOG_DEBUG("%d, %d, %d\r\n", replay_count, recording_position[replay_count], (int)(target_pos * 10000));
		 replay_count += 1;
		
	} else if (replay_count == rec_end_count){
		//control_mode = C_NONE;
		//motorStop();
		//active=false;
		//rec_last_position = abs_position; // TODO
		//W_ref = 0;;
	    NRF_LOG_INFO("Playback stopped..\r\n");
		replay_count = 0;
		rec_end_count = 0;
		mode = TEACHING_PLAYBACK_MODE_NONE;
	}


	//NRF_LOG_FLUSH();
	//SEGGER_RTT_printf(0, "replay_count: %d\r\n", replay_count);

}



static void teaching_playback_timeout_handler(){
	
	if(mode == TEACHING_PLAYBACK_MODE_TEACHING){
		record_position();
		
	} else if (mode == TEACHING_PLAYBACK_MODE_PLAYBACK)
	{
        playback_position();
	}
}


teaching_playback_mode_t teaching_playback_get_mode(void){
	
	return mode;
}


void teaching_playback_init(void){

	uint32_t err_code;
    err_code = app_timer_create(&teaching_playback_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                teaching_playback_timeout_handler);

}



void teaching_start(uint16_t motion){

	if(mode != TEACHING_PLAYBACK_MODE_NONE){
		return;
	}
	// 記憶配列を初期化
	for (int i = 0; i < RECORD_MAX_COUNTS; i++){
		recording_position[i] = 0;
	}
		
	mode = TEACHING_PLAYBACK_MODE_TEACHING;
	timer_start();
    NRF_LOG_INFO("Teaching started..\r\n");
	led_ws2812b_drive_set_color(128, 0, 0);

}




void playback_start(uint16_t motion){

	if(mode != TEACHING_PLAYBACK_MODE_NONE){
		return;
		
	}	
	mode = TEACHING_PLAYBACK_MODE_PLAYBACK;
	motor_enable();
	//motor_control_start();
	motor_set_mode(MOTOR_CONTROL_MODE_POSITION);
	timer_start();
	NRF_LOG_INFO("Playback started..\r\n");
	led_ws2812b_drive_set_color(0, 128, 0);
}


