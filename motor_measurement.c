#include "motor_measurement.h"
#include "km_ble_motor.h"
#include "motor_control.h"
#include "app_timer.h"

/* App Timer （RTC1によるタイマー） */
APP_TIMER_DEF(measurement_timer_id);

void motor_measurement_init(void){

    // Initialize timer module, making it use the scheduler
    // main.c で済。APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

    // Create timer.
    err_code = app_timer_create(&measurement_timer_id, APP_TIMER_MODE_REPEATED, measurement_timer_handler);
    APP_ERROR_CHECK(err_code);
}

static void motor_meas_timeout_handler(void *p_context)
{
    UNUSED_PARAMETER(p_context);
    measurement_update();
}

void measurement_update(void)
{
    //km_mpu6500_read_who();
    uint32_t err_code;
    float pos = motor_get_position(); // TODO
    float spd = motor_get_velocity();                  // TODO
    float trq = motor_get_torque();                                      // TODOchar text[100];

    err_code = ble_motor_measurement_update(pos, spd, trq);
	
    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != NRF_ERROR_RESOURCES) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
       )
    {
        APP_ERROR_HANDLER(err_code);
    }
}



/**@brief Function for starting application timers.
 */
void motor_measurement_timer_start(void)
{

    uint32_t err_code;
    err_code = app_timer_start(motor_meas_timer_id, MOTOR_NOTIFICATION_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
}

void motor_measurement_timer_stop(void)
{
    uint32_t err_code;
    err_code = app_timer_stop(motor_meas_timer_id);
    APP_ERROR_CHECK(err_code);
}