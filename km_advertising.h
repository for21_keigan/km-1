#ifndef km_advertising_h
#define km_advertising_h

#include <ble.h>
#include <ble_advertising.h>

void on_advertising_event(ble_adv_evt_t ble_adv_evt);
void advertising_init(void);
void advertising_start(void);
void advertising_stop(void);

#endif /* km_advertising_h */
