#ifndef motor_setting_h
#define motor_setting_h

#include "flash_address_definition.h"
#include "app_util_platform.h"

/*
// {アドレス, 値} 
int factory_setting[][] = {

    {}, // 

};
*/

void setting_factory_mode_on(bool is_enabled);
bool read_factory_mode_on();

uint32_t setting_write_serial_number(uint8_t *sn, uint32_t length);
uint32_t setting_write_phase_diff(uint8_t *diff);

/*
void setting_write_physical_model(uint16_t model); //
void setting_write_ff_current(uint8_t *i_q, uint32_t length); // 大きい領域
void setting_write_role(uint8_t role);
void setting_write_slave_1(uint32_t id);
void setting_write_slave_2(uint32_t id);
void setting_write_slave_3(uint32_t id);
void setting_write_slave_4(uint32_t id);
void setting_write_slave_5(uint32_t id);
void setting_write_position_offset(float offset); // presetPosition のオフセット
void setting_write_current_pid(uint8_t bit_flag float p, float i, float d);
void setting_write_speed_pid(uint8_t bit_flag float p, float i, float d);
void setting_write_position_p(float p);
void setting_write_max_speed(uint16_t number); //
void setting_write_min_speed(uint16_t number); //
void setting_write_max_torque(uint16_t number); //
void setting_write_max_current(uint16_t number); //
*/

#endif // motor_setting_h

