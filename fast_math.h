#ifndef __fast_math_h
#define __fast_math_h

#include <stdio.h>
#include <stdint.h>
#include "compiler_abstraction.h"
#include "km_motor_definition.h"

#define PI 3.14159265358979f // π: def. of PI
#define DEG60   512         // 60degを512に定義

#define RPM_TO_RADPERSEC(rpm) ((float)(rpm) * 2 * (PI) / 60)  
#define RPM_TO_RADPERMILLIS(rpm) ((float)(rpm) * 2 * (PI) / 60 / 1000)  
#define RADPERSEC_TO_RPM(rps) ((float)(rps) * 360 / (2 * (PI)))
#define RADPERMILLIS_TO_RPM(rpms) ((float)(rpms) * 1000 * 360 / (2 * (PI)))
#define DEGREES_TO_RADIANS(deg) ((float)(deg) * (PI) / 180)
#define RADIANS_TO_DEGREES(rad) ((float)(rad) * 180 / (PI))

#define TICKS_TO_RADIANS(ticks) ((float)(ticks) * 2 * PI / P_max) // ticks --> radians
#define RADPS_TO_TPMS(rps) ((float)(rps) * P_max / 2 / PI / 1000) // radians per seconds --> ticks per milliseconds
#define TPMS_TO_RADPS(tpms) ((float)(tpms) * 1000 * 2 * PI / P_max ) // ticks per milliseconds --> radians per seconds
#define RPM_TO_TPMS(rpm) ((float)(rpm)  * P_max / 60 / 1000) // rpm --> ticks per milliseconds
#define TPMS_TO_RPM(tpms) ((float)(tpms) * 1000 * 60 / P_max ) // ticks per milliseconds --> rpm

extern unsigned short  sin60[]; // sin table from 0 to 60 deg. (max precision error is 0.003%)
// sin(th) = (float)(_sin(th/(PI/3.)*(float)DEG60+0.5))/65535.;
extern long _sin(unsigned short);    // return( 65535*sin(th) ), th=rad*DEG60/(PI/3)=rad*(512*3)/PI (0<=rad<2*PI)
extern long _cos(unsigned short);    // return( 65535*sin(th) ), th=rad*DEG60/(PI/3)=rad*(512*3)/PI (0<=rad<2*PI)
extern void init_fast_math(void);       // call before using _sin(). sin0-sin60deg; 0deg=0, 60deg=512

// TODO static inline 化
extern float _sinf(float);
extern float _cosf(float);
//extern float  norm(float);          // ２ノルムを計算
extern float  sqrt2(float x);         // √xのx=1まわりのテイラー展開 √x = 1 + 1/2*(x-1) -1/4*(x-1)^2 + ...

/* float を 不正な値でないかチェックする TODO*/
static __INLINE float sqrt2(float x)
{
	// √xのx=1まわりのテイラー展開 √x = 1 + 1/2*(x-1) -1/4*(x-1)^2 + ...
	//  return((1+x)*0.5);      // 一次近似
		return(x+(1.0f-x*x)*0.25f); // 二次近似
}

#endif

