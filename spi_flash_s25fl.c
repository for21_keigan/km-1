#include "spi_flash_s25fl.h"
#include "km_io_definition.h"
#include "nrf_delay.h"
#include <string.h>
#include "nrf_log.h"
#include "nrf_drv_spi.h"
#include "peripherals_definition.h"
#include "km_error.h"

// S25FLx control bytes
// The length of tx_buffer
// The length of rx_buffer

/* Write Enable */
#define S25FL_WREN 0x06
#define S25FL_WREN_TX_LEN 1
#define S25FL_WREN_RX_LEN 0

/* Write Disable */
#define S25FL_WRDI 0x04
#define S25FL_WRDI_TX_LEN 1
#define S25FL_WRDI_RX_LEN 0

/* Read Status Register-1 */
// may be used at any time, even while a Program, Erase, or Write Status Registers cycle is in progress.
#define S25FL_RDSR 0x05
#define S25FL_RDSR_TX_LEN 1
#define S25FL_RDSR_RX_LEN 1

#define S25FL_WRSR 0x01      /* Write Status Register */
#define S25FL_READ 0x03      /* Read Data Bytes at 50MHz */
#define S25FL_FAST_READ 0x0b /* Read Data Bytes at Higher Speed 108MHz // Not used as nrf52 SPI max freq. 8MHz isn't fast enough  */
#define S25FL_PP 0x02        /* Page Program  */
#define S25FL_SE 0x20        /* Sector Erase (4k)  */
#define S25FL_BE 0xd8        /* Block Erase (64k)  */
#define S25FL_CE 0xc7        /* Erase entire chip  0x60h is also ok */
#define S25FL_DP 0xb9        /* Deep Power-down  */
#define S25FL_RES 0xab       /* Release Power-down, return Device ID */

/* Read Manufacture ID, memory type ID, capacity ID */
#define S25FL_RDID 0x9F
#define S25FL_RDID_TX_LEN 1
#define S25FL_RDID_RX_LEN 3

bool spi2_device_flash_s25fl = false; // SPI2の制御先がフラッシュである
static const nrf_drv_spi_t spi2 = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE_FLASH_S25FL_IMU_MPU6500); // SPI instance. 

#define FAST_TRANSFER_RX_RIZE 2

// ノンブロッキング時の受信バイト格納用
uint8_t rx_buffer_1_byte[1]; // 1バイトの場合
uint8_t rx_buffer_2_byte[2]; // 2バイトの場合

nrf_drv_spi_config_t s25fl_spi_config = {

    .sck_pin = SPI2_SCK_PIN,
    .mosi_pin = SPI2_MOSI_PIN,
    .miso_pin = SPI2_MISO_PIN,
    .ss_pin = SPI2_SS_PIN_S25FL,
    .irq_priority = APP_IRQ_PRIORITY_LOW,
    .orc = 0xCC,
    .frequency = NRF_DRV_SPI_FREQ_8M, // up to max 108MHz
    .mode = SPI_MODE_S25FL,
    .bit_order = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
};

void spi2_flash_event_handler(nrf_drv_spi_evt_t const * event, void * p_context){

     //NRF_LOG_DEBUG("GYRO_ZOUT: %d\n", (t_buf_1 << 8) | t_buf_2);
}


void flash_s25fl_init_with_nonblocking()
{
	nrf_drv_spi_uninit(&spi2);
	//  "event_handler" 引数が NULL でない場合は、ノンブロッキングモードとなる。（transferが終わるまで他の操作が可能）
	APP_ERROR_CHECK(nrf_drv_spi_init(&spi2, &s25fl_spi_config, spi2_flash_event_handler, NULL));   
	spi2_device_flash_s25fl = true;
	NRF_LOG_DEBUG("flash S25fl was initialized successfully with nonblocking.\r\n");

}


void flash_s25fl_init_with_blocking()
{
	nrf_drv_spi_uninit(&spi2);
	//  "event_handler" 引数が NULL の場合は、ブロッキングモードとなる。（transferが終わるまで他の操作をしない）
	APP_ERROR_CHECK(nrf_drv_spi_init(&spi2, &s25fl_spi_config, NULL, NULL));   
	spi2_device_flash_s25fl = true;
	NRF_LOG_DEBUG("flash S25fl was initialized successfully with blocking.\r\n");

}


void flash_s25fl_uninit(){
    nrf_drv_spi_uninit(&spi2);
}

/*  */
void printBits(uint8_t b)
{
    for (uint8_t mask = 0x80; mask; mask >>= 1)
    {
        if (mask & b)
        {
            NRF_LOG_DEBUG("1\r\n");
        }
        else
        {
            NRF_LOG_DEBUG("0\r\n");
        }
        if (mask == 1)
            NRF_LOG_DEBUG("\n\r");
    }
}




uint8_t flash_s25fl_read_status()
{
    uint8_t tx[] = {S25FL_RDSR};
    uint8_t rx[2]; // 2?
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), rx, ARRAY_SIZE(rx)));

    NRF_LOG_DEBUG("SR1: %d\r\n", rx[1]);

    //printBits(rx[0]);

    return rx[1];
}


// use between each communication to make sure S25FLxx is ready to go.
void flash_s25fl_wait_ready()
{
	NRF_LOG_DEBUG("Waiting ready...\n\r");
    uint8_t s = flash_s25fl_read_status();
    uint8_t c = 10;
    while ((s & 1) == 1 && c > 0)
    { //check if WIP bit is 1
        nrf_delay_ms(10);
        s = flash_s25fl_read_status();
        c--;
    }
    if (c == 0)
    {
        NRF_LOG_DEBUG("Flash S25FL down...\n\r");
    }
}


void flash_s25fl_read_id(uint8_t *manu_id, uint8_t *mem_type, uint8_t *cap)
{
	flash_s25fl_wait_ready();
    uint8_t tx[] = {S25FL_RDID};
    uint8_t rx[4];
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), rx, ARRAY_SIZE(rx)));

    // ※ rx[0] は 0が返ってくる （仕様と異なる）
    NRF_LOG_DEBUG("Manufacturer ID: %d\r\n", rx[1]);
    NRF_LOG_DEBUG("Memory type: %d\r\n", rx[2]);
    NRF_LOG_DEBUG("Capacity: %d\r\n", rx[3]);

    *manu_id = rx[1];
    *mem_type = rx[2];
    *cap = rx[3];
}

void flash_s25fl_read_id2()
{
	flash_s25fl_wait_ready();
    uint8_t tx[] = {0x90};
    uint8_t rx[6];
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), rx, ARRAY_SIZE(rx)));

    // rx[0] は 0が返ってくる （仕様と異なる）
	NRF_LOG_DEBUG("1: %d\r\n", rx[0]);
    NRF_LOG_DEBUG("2: %d\r\n", rx[1]);
    NRF_LOG_DEBUG("3: %d\r\n", rx[2]);
    NRF_LOG_DEBUG("3: %d\r\n", rx[3]);
    NRF_LOG_DEBUG("3: %d\r\n", rx[4]);
    NRF_LOG_DEBUG("3: %d\r\n", rx[5]);
}



/** @brief Must be done to allow erasing or writing
 */
void flash_s25fl_write_enable()
{
    uint8_t tx[] = {0x06}; //{S25FL_WREN};
	uint8_t rx[1];
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), rx, ARRAY_SIZE(rx)));
	//flash_s25fl_wait_ready();
	NRF_LOG_DEBUG("enable write: %d\n\r", flash_s25fl_read_status());
}

/** @brief Erase an entire 4k sector the location is in.
 *  For example "erase_4k(300);" will erase everything from 0-4095. 
 *  All erase commands take time. No other actions can be preformed
 *  while the chip is errasing except for reading the register
 */
void flash_s25fl_erase_4k(uint32_t loc)
{
	NRF_LOG_DEBUG("erase sector loc: %d\r\n", loc);
    flash_s25fl_wait_ready();
    flash_s25fl_write_enable();
	
    uint8_t tx[] = {S25FL_SE, (loc >> 16) & 0xFF, (loc >> 8) & 0xFF, loc & 0xFF};	
	uint8_t rx[4];
	NRF_LOG_DEBUG("loc_byte: %d, %d, %d\r\n", tx[1], tx[2], tx[3]);

    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), rx, 4));
    NRF_LOG_DEBUG("erase 4k transfer\r\n");
	
	flash_s25fl_wait_ready(); // 必要？

}

/**@brief Erase an entire 64_k sector the location is in.
 * For example erase4k(530000) will erase everything from 524543 to 589823. 
 */
void flash_s25fl_erase_64k(uint32_t loc)
{
    flash_s25fl_wait_ready();
    flash_s25fl_write_enable();

    uint8_t tx[] = {S25FL_BE, loc >> 16, loc >> 8, (loc & 0xFF)};
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), NULL, 0));

    flash_s25fl_wait_ready(); // 必要？
}

/** @brief erases all the memory. Can take several seconds.
  *
  */
void flash_s25fl_erase_all()
{
    flash_s25fl_wait_ready();
    flash_s25fl_write_enable();

    uint8_t tx[] = {S25FL_CE};
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), NULL, 0));

    flash_s25fl_wait_ready(); // 必要？
}

/*
 *
 １度のコマンドで無制限に読むことができる。長さ（サイズ）に制限はない。
 */
// Read data from the flash chip. There is no limit "length". The entire memory can be read with one command.
//read_S25(starting location, array, number of bytes);
void flash_s25fl_read(uint32_t loc, uint8_t *array, uint32_t length)
{
	NRF_LOG_DEBUG("read \n\r");
    flash_s25fl_wait_ready();

    uint8_t tx[] = {S25FL_READ, loc >> 16, loc >> 8, (loc & 0xFF)};
    uint8_t rx[length+4];	/*
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), rx, length));
	for (uint16_t i = 0; i < length+4; i++ ) {
		NRF_LOG_DEBUG("%d,",rx[i]); 
		if(i == length+4 - 1) NRF_LOG_DEBUG("\r\n");
	}
	*/
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), rx, length+4));
	
	/* メモリ領域内の内容をコピー */
    memcpy(array, &rx[4], length);
	
	for (uint16_t i = 0; i < length; i++ ) {
		NRF_LOG_DEBUG("%d,",array[i]); 
		if(i == length - 1) NRF_LOG_DEBUG("\r\n");
	}	
    flash_s25fl_wait_ready(); // 必要？
}


/*
 *
 １度のコマンドで無制限に読むことができる。長さ（サイズ）に制限はない。
 */
// Read data from the flash chip. There is no limit "length". The entire memory can be read with one command.
//read_S25(starting location, array, number of bytes);
uint32_t flash_s25fl_fast_read(uint32_t loc, uint8_t *array, uint32_t length)
{
	NRF_LOG_DEBUG("read \n\r");
    uint8_t s = flash_s25fl_read_status();
    if((s & 1) == 1){
        return KM_ERROR_BUSY;
    }

    uint8_t tx[] = {S25FL_READ, loc >> 16, loc >> 8, (loc & 0xFF)};
    uint8_t rx[length+4];	/*
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), rx, length));
	for (uint16_t i = 0; i < length+4; i++ ) {
		NRF_LOG_DEBUG("%d,",rx[i]); 
		if(i == length+4 - 1) NRF_LOG_DEBUG("\r\n");
	}
	*/
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), rx, length+4));
	
	/* メモリ領域内の内容をコピー */
    memcpy(array, &rx[4], length);
	
	for (uint16_t i = 0; i < length; i++ ) {
		NRF_LOG_DEBUG("%d,",array[i]); 
		if(i == length - 1) NRF_LOG_DEBUG("\r\n");
	}	

}



/**@brief 

一度に書き込めるのは 256 bytes (1 page)まで。最初に erase しておく必要があり、上書きは不可。

Programs up to 256 bytes of data to flash chip. Data must be erased first. You cannot overwrite.
  * Only one continuous page (256 Bytes) can be programmed at once so there's some
  * sorcery going on here to make it not wrap around.
  * It's most efficent to only program one page so if you're going for speed make sure your
  * location %=0 (for example location=256, length=255.) or your length is less that the bytes remain
  * in the page (location =120 , length= 135)
*/

//write_S25(starting location, array, number of bytes);
void flash_s25fl_write(uint32_t loc, uint8_t* array, uint32_t length)
{
	flash_s25fl_wait_ready(); // Must be done before writing can commence. Erase clears it.
	flash_s25fl_write_enable();

	uint8_t pp_tx[4+length];
    pp_tx[0] = S25FL_PP;
    pp_tx[1] = loc >> 16;
    pp_tx[2] = loc >> 8;
    pp_tx[3] = loc & 0xff;

	/* メモリ領域内の内容をコピー */
    memcpy(&pp_tx[4], array, length);
	
	APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, pp_tx, ARRAY_SIZE(pp_tx), NULL, 0)); // コマンド、アドレス
	//APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, array, length, NULL, 0)); // データ送信
	NRF_LOG_DEBUG("Writing now.\r\n");
	flash_s25fl_wait_ready(); 
	/*
    if (length > 255)
    {
		NRF_LOG_INFO("flash write length > 255 \n\r");
		
        uint32_t reps = length >> 8;
        uint32_t length1;
        uint32_t array_count;
        uint32_t first_length;
        uint32_t remainer0 = length - (256 * reps);
        uint32_t locb = loc;

		NRF_LOG_INFO("reps: %d, remainer0: %d\n\r", reps, remainer0);

        for (int i = 0; i < (reps + 2); i++)
        {
            if (i == 0)
            {
                length1 = 256 - (locb & 0xff);
                first_length = length1;
                if (length1 == 0)
                {
                    i++;
                }
                array_count = 0;
            }

            if (i > 0 && i < (reps + 1))
            {
                locb = first_length + loc + (256 * (i - 1));
                array_count = first_length + (256 * (i - 1));
                length1 = 255;
            }
            if (i == (reps + 1))
            {
                locb += (256);
                array_count += 256;
                length1 = remainer0;
                if (remainer0 == 0)
                {
                    break;
                }
            }
            //NRF_LOG_INFO("i: %d, locb: %d, length1: %d, array_count: %d\n\r",i, locb, length1, array_count);

            flash_s25fl_wait_ready();
            flash_s25fl_write_enable();
			
			uint8_t pp_tx[] = {S25FL_PP, locb >> 16, locb >> 8, locb & 0xff};
			
			APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, pp_tx, ARRAY_SIZE(pp_tx), NULL, 0)); // コマンド、アドレス
            APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, array, length, NULL, 0)); // データ送信
			
            flash_s25fl_wait_ready();

        }
    }

    if (length <= 255)
    {
		NRF_LOG_INFO("flash write length <= 255 \n\r");
		
        if (((loc & 0xff) != 0) | ((loc & 0xff) < length))
        {
            uint8_t remainer = loc & 0xff;
            uint8_t length1 = 256 - remainer;
            uint8_t length2 = length - length1;
            uint32_t page1_loc = loc;
            uint32_t page2_loc = loc + length1;

            flash_s25fl_wait_ready();
            flash_s25fl_write_enable();
			
			uint8_t pp_tx_1[] = {S25FL_PP, page1_loc >> 16, page1_loc >> 8, page1_loc & 0xff};

			APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, pp_tx_1, ARRAY_SIZE(pp_tx_1), NULL, 0)); // コマンド、アドレス
            APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, &array[0], length1, NULL, 0)); // データ送信

            flash_s25fl_wait_ready();
            flash_s25fl_write_enable();
			
			uint8_t pp_tx_2[] = {S25FL_PP, page2_loc >> 16, page2_loc >> 8, page2_loc & 0xff};

			APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, pp_tx_2, ARRAY_SIZE(pp_tx_2), NULL, 0)); // コマンド、アドレス
            APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, &array[length1], length2, NULL, 0)); // データ送信

            //Serial.println("//////////");
            //Serial.print("remainer ");Serial.println(remainer);

			
            //Serial.print("length1 ");Serial.println(length1);
            //Serial.print("length2 ");Serial.println(length2);
            //Serial.print("page1_loc ");Serial.println(page1_loc);
            //Serial.print("page2_loc ");Serial.println(page2_loc);
            //Serial.println("//////////");
        }

        else
        {
			NRF_LOG_INFO("loc & 0xff =  %d\n\r", loc & 0xff);

			flash_s25fl_wait_ready(); // Must be done before writing can commence. Erase clears it.
            flash_s25fl_write_enable();

			uint8_t pp_tx[] = {S25FL_PP, loc >> 16, loc >> 8, loc & 0xff};
			
			APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, pp_tx, ARRAY_SIZE(pp_tx), NULL, 0)); // コマンド、アドレス
            APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, array, length, NULL, 0)); // データ送信
			
			flash_s25fl_wait_ready(); 
        }
    }
*/
}

//Used in conjuture with the write protect pin to protect blocks.
//For example on the S25FL216K sending "write_reg(B00001000);" will protect 2 blocks, 30 and 31.
//See the datasheet for more. http://www.mouser.com/ds/2/380/S25FL216K_00-6756.pdf
void flash_s25fl_write_reg(uint8_t w)
{
	uint8_t tx[] = {S25FL_WRSR};
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, tx, ARRAY_SIZE(tx), NULL, 0));
}




