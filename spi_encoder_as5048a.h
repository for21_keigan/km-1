/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

/**@file 
 * @brief 磁気式アブソリュートエンコーダー AS5048A、SPI通信を取り扱う
 */

#ifndef SPI_ENCODER_AS5048A_H
#define SPI_ENCODER_AS5048A_H

#include "app_util_platform.h"

/**
 * 初期化
 */
void encoder_as5048a_init(void);

/**
 * SPI転送を行う
 */
void encoder_as5048a_angle_spi_xfer(void);

/**
 * 角度を返す
 */
uint16_t encoder_as5048a_get_angle(void);

/**
 * AGC DIAG の値を返す
 */
uint16_t encoder_as5048a_get_agc(void);
/*
 * エラー値を返す
 */
uint16_t encoder_as5048a_get_errors(void);



#endif // SPI_ENCODER_AS5048A_H

