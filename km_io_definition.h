/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef KM_IO_DEFINITION_H
#define KM_IO_DEFINITION_H

#include "km_motor_definition.h"

#define KM1_BETA /* 201702以前の基板(SPI改造なし) */
//#define KM1_SPI2_PROTO /* 201702 SPI2改造基板(#キ20専用) */
//#define KM1_BETA /* 201704 SPI2改造基板（量産対応） */

#ifdef KM1_LEGACY 

/* 3 Phase ENABLE PIN TO MOTOR DRIVER  */
#define L6230_EN_1_PIN 25 
#define L6230_EN_2_PIN 26 
#define L6230_EN_3_PIN 27 

/* SPI */
#define SPI_SS_PIN_AS5048A 17
//#define SPI_SS_PIN_S25FL 18
//#define SPI_SS_PIN_MPU6500 19
#define SPI_MOSI_PIN 16
#define SPI_MISO_PIN 15
#define SPI_SCK_PIN 14

#define SPI2_SS_PIN_S25FL 40
#define SPI2_SS_PIN_MPU6500 40
#define SPI2_MOSI_PIN 40
#define SPI2_MISO_PIN 40
#define SPI2_SCK_PIN 40

#elif defined KM1_SPI2_PROTO

#define L6230_EN_1_PIN 25

/* SPI */
#define SPI_SS_PIN_AS5048A 17
#define SPI_MOSI_PIN 27
#define SPI_MISO_PIN 26
#define SPI_SCK_PIN 20

#define SPI2_SS_PIN_S25FL 18
#define SPI2_SS_PIN_MPU6500 19
#define SPI2_MOSI_PIN 16
#define SPI2_MISO_PIN 15
#define SPI2_SCK_PIN 14

#elif defined KM1_BETA

#define L6230_EN_1_PIN 25

/* SPI */
#define SPI_SS_PIN_AS5048A 17
#define SPI_MOSI_PIN 16
#define SPI_MISO_PIN 15
#define SPI_SCK_PIN 14

#define SPI2_SS_PIN_S25FL 18
#define SPI2_SS_PIN_MPU6500 19
#define SPI2_MOSI_PIN 27
#define SPI2_MISO_PIN 26
#define SPI2_SCK_PIN 20

#endif 


/* DCDC Enable PIN */
#define DCDC_ENABLE_PIN 30 // 通常の基板
#define DCDC_ENABLE_PIN2 10 //キB2 基板のみ 本来は I2Cピン 

/* INPUT VOLTAGE ? TODO */
#define VS_PIN 28

/* BUTTONS */
#define KM_SW_1 21
#define KM_SW_2 22
#define KM_SW_3 23

/* 結線方法によりピンとUVWの組み合わせが異なる  */
/* 14極12スロット --> 2, 3, 4 
   16極15スロット --> 4, 3, 2 
   その他 --> 結線方法による 
*/
#ifdef MOTOR_TYPE_12N_14P
#define SENSE1_PIN 2
#define SENSE2_PIN 3
#define SENSE3_PIN 4
#elif defined MOTOR_TYPE_15N_16P
#define SENSE1_PIN 4
#define SENSE2_PIN 3
#define SENSE3_PIN 2
#else // その他の設定
#define SENSE1_PIN 2
#define SENSE2_PIN 3
#define SENSE3_PIN 4
#endif

/* 3 Phase PWM PIN TO MOTOR DRIVER  */
#define L6230_IN_1_PIN (SENSE1_PIN + 9)
#define L6230_IN_2_PIN (SENSE2_PIN + 9)
#define L6230_IN_3_PIN (SENSE3_PIN + 9)


/* DIAGNOSTIC PIN FROM MOTOR DRIVER */
#define L6230_DIAG_EN_PIN 29

/* Full Color Serial LED WS2812B Output */
#define LED_WS2812B_PIN 24

/* SAADC */
#define VOLTAGE_REF_PIN 31 // +VR

/* UART */
#define UART_RXD_PIN 8
#define UART_TXD_PIN 6
#define UART_RTS_PIN 7
#define UART_CTS_PIN 5

// TODO

/* I2C */
// TODO
#define I2C_SDA1_PIN 10
#define I2C_SCL1_PIN 9

#endif // KM_IO_DEFINITION
