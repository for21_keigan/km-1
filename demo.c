#include "demo.h"
#include "app_timer.h"
#include "motor_control.h"
#include "motion_planner.h"
#include "km_motor_definition.h"
#include "nrf_log.h"

#define APP_TIMER_PRESCALER             0    
APP_TIMER_DEF(app_demo_timer_id); 
uint32_t demo_count;
#define DEMO_INT_MS 100
#define DEMO_NUM 0
#define DEMO_SPEED 50.0f // ticks/ms
#define DEMO_ACC 10.0f
#define DEMO_DEC 10.0f
#define TRAVEL_LENGTH_1 25000
#define TRAVEL_LENGTH_2 50000
#define TRAVEL_LENGTH_3 81500

float demo_start_position;

void demo_timer_handler(void * p_context)
{
    if(DEMO_NUM == 0){
		if(demo_count == 10){
		   //motion_set_target_position(demo_start_position + TRAVEL_LENGTH_1, DEMO_SPEED, DEMO_ACC, DEMO_DEC);				
		} else if (demo_count == 20){		
		   //motion_set_position(demo_start_position, DEMO_SPEED, DEMO_ACC, DEMO_DEC);		
		} else if (demo_count == 30){
		   //motion_set_position(demo_start_position + TRAVEL_LENGTH_2, DEMO_SPEED, DEMO_ACC, DEMO_DEC);
		} else if (demo_count == 45){
		   //motion_set_position(demo_start_position, DEMO_SPEED, DEMO_ACC, DEMO_DEC);
		} else if (demo_count == 60){
		   //motion_set_position(demo_start_position + TRAVEL_LENGTH_3, DEMO_SPEED, DEMO_ACC, DEMO_DEC);
		} else if (demo_count == 80){
		   //motion_set_position(demo_start_position, DEMO_SPEED, DEMO_ACC, DEMO_DEC);
		} else if (demo_count == 90){
		   demo_count = 0;
		}
	}	
    demo_count++;
    //NRF_LOG_PRINTF("demo_count: %d\r\n",demo_count);	
}

void demo_timer_init()
{
	ret_code_t err_code;
	err_code = app_timer_create(&app_demo_timer_id, APP_TIMER_MODE_REPEATED, demo_timer_handler);
	APP_ERROR_CHECK(err_code);
}

void demo_timer_start(void)
{
    uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_start(app_demo_timer_id, APP_TIMER_TICKS(DEMO_INT_MS), NULL); 
	APP_ERROR_CHECK(err_code);
	demo_start_position = motor_get_position();
	NRF_LOG_INFO("demo_start_position: %d", demo_start_position);
}

void demo_timer_stop(void)
{
    uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(app_demo_timer_id);
    APP_ERROR_CHECK(err_code);
}

void demo_start()
{
	demo_timer_init();
	demo_timer_start();
}

