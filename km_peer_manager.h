#ifndef km_peer_manager_h
#define km_peer_manager_h

#include <stdbool.h>

// ピアマネージャの初期化
// 注意: このメソッドを呼び出す前にpstorageの初期化が完了していること。
void peer_manager_init(bool erase_bonds);

#endif /* km_peer_manager_h */
