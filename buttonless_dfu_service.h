#ifndef buttonless_dfu_service_h
#define buttonless_dfu_service_h

#include <stdint.h>
#include "ble_srv_common.h"

void buttonless_dfu_service_init(void);
void buttonless_dfu_on_ble_event(ble_evt_t * p_ble_evt);

#endif // buttonless_dfu_service.h

