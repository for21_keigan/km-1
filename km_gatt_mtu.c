#include <nrf_log.h>
#include <app_error.h>

#include "km_gatt_mtu.h"
#include "nrf_ble_gatt.h"

static nrf_ble_gatt_t m_gatt;       /**< GATT module instance. */

void gatt_mtu_init(void){

    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);

}

void gatt_mtu_on_ble_evt(ble_evt_t * p_ble_evt){

    nrf_ble_gatt_on_ble_evt(&m_gatt, p_ble_evt);

}

