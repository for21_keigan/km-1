#include "km_error.h"
#include "legacy_command_control.h"
#include "motor_control.h"
#include "type_utility.h"
#include "led_ws2812b.h"
#include "motor_setting.h"
#include "nrf_log.h"
#include "app_util.h"

#define FACTORY_MODE_PASSWORD 0xFACD0520 //4バイト

uint32_t ble_legacy_control_handler(uint8_t *data, uint8_t len)
{

    NRF_LOG_DEBUG("///// motor write handler /////\n\r");
    NRF_LOG_DEBUG("Value length: %d\n\r", len);

    for (uint8_t i = 0; i < len; i++)
    {
        NRF_LOG_DEBUG("Value Byte[%d]: %d\n", i, data[i]);
    }

    switch (data[0])
    {
    case LEGACY_CMD_SetSpeed: // 0x10
    {
        NRF_LOG_DEBUG("LEGACY_CMD_SetSpeed\r\n");
        if (len == LEGACY_VLEN_SET_SPD)
        {
            if (motor_get_mode() == MOTOR_CONTROL_MODE_VELOCITY)
            {
                float vel;
                if (get_float_from_args(data, len, 1, &vel))
                {
                    motor_run_at_velocity(vel);
                    NRF_LOG_DEBUG("KM_SUCCESS\r\n");
                }
                else
                {
                    NRF_LOG_DEBUG("KM_ERROR_INVALID_DATA\r\n");
                    return KM_ERROR_INVALID_DATA;
                }
            }
            else
            {
                NRF_LOG_DEBUG("Error: control_mode is not matched.\r\n");
                return KM_ERROR_INVALID_PARAM;
            }
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;
    case LEGACY_CMD_SetPositionDt: // 0x20
    {
        NRF_LOG_DEBUG("LEGACY_CMD_SetPositionDt\n");
        if (len == LEGACY_VLEN_SET_POS_DT)
        {
            if (motor_get_mode() == MOTOR_CONTROL_MODE_POSITION)
            {
                float pos = 0;
                float vel = 0;
                if (get_float_from_args(data, len, 1, &pos) && get_float_from_args(data, len, 5, &pos))
                {
                    motor_move_to(pos, vel);
                    NRF_LOG_DEBUG("KM_SUCCESS\r\n");
                    //log_output_result(id, KM_SUCCESS);
                }
                else
                {
                    NRF_LOG_DEBUG("KM_ERROR_INVALID_DATA\r\n");
                    return KM_ERROR_INVALID_DATA;
                }
            }
            else
            {
                NRF_LOG_DEBUG("Error: control_mode is not matched.\r\n");
                return KM_ERROR_INVALID_PARAM;
            }
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;
    case LEGACY_CMD_SetPosition: // 0x21
    {
        NRF_LOG_DEBUG("LEGACY_CMD_SetPosition\n");
        if (len == LEGACY_VLEN_SET_POS)
        {
            if (motor_get_mode() == MOTOR_CONTROL_MODE_POSITION)
            {
                float pos = 0;
                float spd = 0;
                if (get_float_from_args(data, len, 1, &pos) && get_float_from_args(data, len, 5, &spd))
                {
                    motor_move_to(pos, spd);
                    NRF_LOG_DEBUG("KM_SUCCESS\r\n");
                }
                else
                {
                    NRF_LOG_DEBUG("KM_ERROR_INVALID_DATA\r\n");
                    return KM_ERROR_INVALID_DATA;
                }
            }
            else
            {
                NRF_LOG_DEBUG("Error: control_mode is not matched.\r\n");
                return KM_ERROR_INVALID_PARAM;
            }
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;
    case LEGACY_CMD_SetRelPositionDt: // 0x23
    {
        NRF_LOG_DEBUG("LEGACY_CMD_SetRelPositionDt");
        if (len == LEGACY_VLEN_SET_REL_POS_DT)
        {
            if (motor_get_mode() == MOTOR_CONTROL_MODE_POSITION)
            {
                float dist = 0;
                float spd = 0;
                if (get_float_from_args(data, len, 1, &dist) && get_float_from_args(data, len, 5, &spd))
                {
                    motor_move_by(dist, spd);
                    NRF_LOG_DEBUG("KM_SUCCESS\r\n");
                }
                else
                {
                    NRF_LOG_DEBUG("KM_ERROR_INVALID_DATA\r\n");
                    return KM_ERROR_INVALID_DATA;
                }
            }
            else
            {
                NRF_LOG_DEBUG("Error: control_mode is not matched.\r\n");
                return KM_ERROR_INVALID_PARAM;
            }
        } else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;
    case LEGACY_CMD_SetRelPosition: // 0x24
    {
        NRF_LOG_DEBUG("LEGACY_CMD_SetRelPosition\n");
        if (len == LEGACY_VLEN_SET_REL_POS)
        {
            if (motor_get_mode() == MOTOR_CONTROL_MODE_POSITION)
            {
                float dist = 0;
                float spd = 0;
                if (get_float_from_args(data, len, 1, &dist) && get_float_from_args(data, len, 5, &spd))
                {
                    motor_move_by(dist, spd);
                    NRF_LOG_DEBUG("KM_SUCCESS\r\n");
                    //log_output_result(id, KM_SUCCESS);
                }
                else
                {
                    NRF_LOG_DEBUG("KM_ERROR_INVALID_DATA\r\n");
                    return KM_ERROR_INVALID_DATA;
                }
            }
            else
            {
                NRF_LOG_DEBUG("Error: control_mode is not matched.\r\n");
                return KM_ERROR_INVALID_PARAM;
            }
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;
    case LEGACY_CMD_PreSetPositon: // 0x27
    {
        NRF_LOG_DEBUG("LEGACY_CMD_PreSetAbsPositon\n");
        if (len == LEGACY_VLEN_SET_PRE_SET_POS)
        {
            float pos = 0;
            if (get_float_from_args(data, len, 1, &pos))
            {
                motor_preset_position(pos);
                NRF_LOG_DEBUG("KM_SUCCESS\r\n");
            }
            else
            {
                NRF_LOG_DEBUG("KM_ERROR_INVALID_DATA\r\n");
                return KM_ERROR_INVALID_DATA;
            }
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;
    case LEGACY_CMD_SetTorque: // 0x70
    {
        NRF_LOG_DEBUG("LEGACY_CMD_SetTorque\n");
        if (len == LEGACY_VLEN_SET_TRQ)
        {
            if (motor_get_mode() == MOTOR_CONTROL_MODE_TORQUE)
            {
                float trq = 0;
                if (get_float_from_args(data, len, 1, &trq))
                {
                    motor_hold_torque(trq);
                    NRF_LOG_DEBUG("KM_SUCCESS\r\n");
                }
                else
                {
                    NRF_LOG_DEBUG("KM_ERROR_INVALID_DATA\r\n");
                    return KM_ERROR_INVALID_DATA;
                }
            }
            else
            {
                NRF_LOG_DEBUG("Error: control_mode is not matched.\r\n");
                return KM_ERROR_INVALID_PARAM;
            }
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;

    case LEGACY_CMD_MaxTorque: //0x71
    {
        NRF_LOG_DEBUG("LEGACY_CMD_MaxTorque\n");
        if (len == LEGACY_VLEN_MAX_TRQ)
        {
            float trq = 0;
            if (get_float_from_args(data, len, 1, &trq))
            {
                motor_set_max_torque(trq);
                NRF_LOG_DEBUG("KM_SUCCESS\r\n");
            }
            else
            {
                NRF_LOG_DEBUG("KM_ERROR_INVALID_DATA\r\n");
                return KM_ERROR_INVALID_DATA;
            }
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;
    /* SoftStop廃止
    case LEGACY_CMD_SoftStop: // 0x80
    {
        NRF_LOG_DEBUG("LEGACY_CMD_SoftStop\n");
        if (len == LEGACY_VLEN_SOFT_STOP)
        {
            float v[1];
            getParamsFloat(data, len, 1, v);
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
        }
        W_ref = 0;
        active = false; // TODO

        motorStop();
    } 
    break;
    */

    case LEGACY_CMD_HardStop: // 0x81
    {
        NRF_LOG_DEBUG("LEGACY_CMD_HardStop\n");
        if (len == LEGACY_VLEN_HARD_STOP)
        {
            // 速度ゼロにした後、フリーにする
            motor_run_at_velocity(0);
            motor_free();
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;
    case LEGACY_CMD_HardBrake: // 0x82
    {
        NRF_LOG_DEBUG("LEGACY_CMD_HardBrake\n");
        if (len == LEGACY_VLEN_HARD_BRAKE)
        {
            // モーターにブレーキをかける（L6230ドライバ）
            motor_brake();
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;

    case LEGACY_CMD_SetSpeedPID: // 0xA2
    {
        NRF_LOG_DEBUG("LEGACY_CMD_SetSpeedPID\n");
        if (len == LEGACY_VLEN_SET_SPD_PID)
        {
            uint8_t flag = data[1];
            if (flag & 0x01)
            {
                float p = 0;
                if (get_float_from_args(data, len, 2, &p))
                {
                    motor_set_speed_pid_p(p);
                    NRF_LOG_DEBUG("KM_SUCCESS\r\n");
                }
                else
                {
                    NRF_LOG_DEBUG("P:KM_ERROR_INVALID_DATA\r\n");
                    return KM_ERROR_INVALID_DATA;
                }
            }
            if (flag & 0x02)
            {
                float i = 0;
                if (get_float_from_args(data, len, 6, &i))
                {
                    motor_set_speed_pid_i(i);
                    NRF_LOG_DEBUG("KM_SUCCESS\r\n");
                }
                else
                {
                    NRF_LOG_DEBUG("I:KM_ERROR_INVALID_DATA\r\n");
                    return KM_ERROR_INVALID_DATA;
                }
            }
            if (flag & 0x04)
            {
                float d = 0;
                if (get_float_from_args(data, len, 10, &d))
                {
                    motor_set_speed_pid_d(d);
                    NRF_LOG_DEBUG("KM_SUCCESS\r\n");
                    return KM_ERROR_INVALID_DATA;
                }
                else
                {
                    NRF_LOG_DEBUG("D:KM_ERROR_INVALID_DATA\r\n");
                    return KM_ERROR_INVALID_DATA;
                }
            }
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;

    case LEGACY_CMD_SetPositionP: // 0xA4
    {
        NRF_LOG_DEBUG("LEGACY_CMD_SetPositionPID\n");
        if (len == LEGACY_VLEN_SET_POS_PID)
        {
            float p = 0;
            if (get_float_from_args(data, len, 1, &p))
            {
                motor_set_position_pid_p(p);
                NRF_LOG_DEBUG("KM_SUCCESS\r\n");
            }
            else
            {
                NRF_LOG_DEBUG("D:KM_ERROR_INVALID_DATA\r\n");
                return KM_ERROR_INVALID_DATA;
            }
        }
        else
        {
            NRF_LOG_DEBUG("Error: Invalid arguments length.");
            return KM_ERROR_INVALID_LENGTH;
        }
    }
    break;
    case LEGACY_CMD_FactoryMode: // 0xFA
    {
        NRF_LOG_DEBUG("LEGACY_CMD_FactoryMode\n");
        switch (data[1]){
        case LEGACY_CMD_FactoryMode_On:
            if (len == LEGACY_VLEN_FA_MODE_ON)
            {
				uint32_t password = uint32_big_decode(data);
				if(password == FACTORY_MODE_PASSWORD){
					setting_factory_mode_on(true);
				}
            }
            else
            {
                NRF_LOG_DEBUG("Error: Invalid arguments length.");
                return KM_ERROR_INVALID_LENGTH;
            }
        break;
        case LEGACY_CMD_FactoryMode_WriteSerialNumber:
            if (len == LEGACY_VLEN_FA_MODE_WRITE_SN)
            {
				if(read_factory_mode_on()){
					
					setting_write_serial_number(data, len);
				} else {
					NRF_LOG_DEBUG("It is not a factory mode.\r\n");
				}
            }
            else
            {
                NRF_LOG_DEBUG("Error: Invalid arguments length.");
                return KM_ERROR_INVALID_LENGTH;
            }
        break;
        case LEGACY_CMD_FactoryMode_WritePhaseDiff:
            if (len == LEGACY_VLEN_FA_MODE_WRITE_PHASE_DIFF)
            {
				setting_write_phase_diff(&data[2]);
				if(read_factory_mode_on()){
					
					
				} else {
					NRF_LOG_DEBUG("It is not a factory mode.\r\n");
				}              
            }
            else
            {
                NRF_LOG_DEBUG("Error: Invalid arguments length.");
                return KM_ERROR_INVALID_LENGTH;
            }
        break;

        case LEGACY_CMD_FactoryMode_ReadPhaseDiff:
            if (len == LEGACY_VLEN_FA_MODE_READ_PHASE_DIFF)
            {
              
            }
            else
            {
                NRF_LOG_DEBUG("Error: Invalid arguments length.");
                return KM_ERROR_INVALID_LENGTH;
            }
        break;
		default:
		break;		
	}		
    }
    break;
    default:
    {
        NRF_LOG_DEBUG(" UNKNOWN COMMAND \n");
      
    }
	break;
    }

    return KM_SUCCESS;
}

uint32_t ble_legacy_control_mode_handler(uint8_t *data, uint8_t len)
{

    NRF_LOG_DEBUG("***** Motor Control Mode write handler *****\n\r");

    if (len != 1)
    {
        NRF_LOG_DEBUG("Error: Invalid arguments length.(not 1)\n\r");
        return KM_ERROR_INVALID_LENGTH;
    }
    NRF_LOG_DEBUG("Value Byte[0]: %d\n", data[0]);

    switch (data[0])
    {
    case KM_MOTOR_CONTROL_MODE_NONE:
    {
        NRF_LOG_DEBUG("KM_MOTOR_CONTROL_MODE_NONE\n");
        motor_disable(); // 内部で motor_set_mode(MOTOR_CONTROL_MODE_NONE);
    }
    break;
    case KM_MOTOR_CONTROL_MODE_SPEED:
    {
        NRF_LOG_DEBUG("KM_MOTOR_CONTROL_MODE_SPEED\n");
        motor_set_mode(MOTOR_CONTROL_MODE_VELOCITY);
		motor_enable();
    }
    break;
    case KM_MOTOR_CONTROL_MODE_POSITION:
    {
        NRF_LOG_DEBUG("KM_MOTOR_CONTROL_MODE_POSITION\n");
        motor_set_mode(MOTOR_CONTROL_MODE_POSITION);
		motor_enable();
    }
    break;
    case KM_MOTOR_CONTROL_MODE_TORQUE:
    {
        NRF_LOG_DEBUG("KM_MOTOR_CONTROL_MODE_TORQUE\n");
        motor_set_mode(MOTOR_CONTROL_MODE_TORQUE);
		motor_enable();
    }
    break;
    default:
    {
    }
    break;
    }

    return KM_SUCCESS;
}

uint32_t ble_legacy_led_handler(uint8_t *data, uint8_t len)
{

    NRF_LOG_DEBUG("***** led write handler *****\n");
    NRF_LOG_DEBUG("Value length: %d\n", len);

    for (uint8_t i = 0; i < len; i++)
    {
        NRF_LOG_DEBUG("Value Byte[%d]: %d\n", i, data[i]); 
    }

    switch (data[0])
    {
    case MOTOR_LED_STATE_OFF:
    {
        NRF_LOG_DEBUG("MOTOR_LED_STATE_OFF\n");
        led_ws2812b_drive_clear();
    }
    break;
    case MOTOR_LED_STATE_ON_SOLID:
    {
        NRF_LOG_DEBUG("MOTOR_LED_STATE_ON_SOLID\n");
        led_ws2812b_drive_set_color(data[1], data[2], data[3]);
        //ws2812b_drive_set_color(126, 126, 0);
    }
    break;
    case MOTOR_LED_STATE_ON_FLASH:
    {
        NRF_LOG_DEBUG("MOTOR_LED_STATE_ON_FLASH\n");
        // TODO
    }
    break;
    default:
    {
        NRF_LOG_DEBUG("LED_COMMAND_OTHERS\n");
    }
    break;
    }
	
	return NRF_SUCCESS;
}



