#include "uart_control.h"
#include "board_km1.h"
#include "app_uart.h" 
#include "motor_command.h"
#include "unit_convert.h"

void uart_error_handle(app_uart_evt_t * p_event)
{
    //printf("UART_ERROR\n");
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        printf("APP_UART_COMMUNICATION_ERROR\n");
        APP_ERROR_HANDLER(p_event->data.error_communication);
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        printf("APP_UART_FIFO_ERROR\n");
        APP_ERROR_HANDLER(p_event->data.error_code);
    } 
}

void uart_control_init(){
	
	ret_code_t err_code;
	const app_uart_comm_params_t comm_params =
		{
			UART_RXD_PIN,
			UART_TXD_PIN,
			UART_RTS_PIN,
			UART_CTS_PIN,
			APP_UART_FLOW_CONTROL_ENABLED,
			false,
			UART_BAUDRATE_BAUDRATE_Baud115200
		};

	APP_UART_FIFO_INIT(&comm_params,
						 UART_RX_BUF_SIZE,
						 UART_TX_BUF_SIZE,
						 uart_error_handle,
						 APP_IRQ_PRIORITY_LOW,
						 err_code);

	APP_ERROR_CHECK(err_code);

}




uint8_t command = 0x00; // コマンド
uint8_t values[256] = {0}; // 引数
uint8_t num = 0;
uint8_t byte_num = 0;
uint32_t exec_num = 0;

typedef enum {
    
   UART_CMD_STATE_WAIT,
   UART_CMD_STATE_RECEIVING,
   UART_CMD_STATE_EXECUTE

} uart_command_state_t;

uart_command_state_t c_state = UART_CMD_STATE_WAIT;


void resetCommand(){
    c_state =  UART_CMD_STATE_WAIT;
    command = 0x00; // コマンド
    num = 0;
    byte_num = 0;
}


void handleCommand(uint8_t cmd) {
    command = cmd;          
    switch (command) {
        case UART_CMD_SETCONTROLMODE:
            printf("UART_CMD_SETCONTROLMODE\n");    
            byte_num = UART_VLEN_SETCONTROLMODE;
            c_state = UART_CMD_STATE_RECEIVING;      
            break;
        case UART_CMD_SETSPEED:
            printf("UART_CMD_SETSPEED\n");    
            byte_num = UART_VLEN_SETSPEED;
            c_state = UART_CMD_STATE_RECEIVING;      
            break;
        case UART_CMD_SETSTOP:
            printf("UART_CMD_SETSTOP\n");    
            byte_num = UART_VLEN_SETSTOP;
            c_state = UART_CMD_STATE_EXECUTE;          
            break;
        case UART_CMD_SETPOSITION:
            printf("UART_CMD_SETPOSITION\n");    
            byte_num = UART_VLEN_SETPOSITION;
            c_state = UART_CMD_STATE_RECEIVING;      
            break;
        case UART_CMD_SETRELPOSITION:
            printf("UART_CMD_SETRELPOSITION\n");    
            byte_num = UART_VLEN_SETRELPOSITION;
            c_state = UART_CMD_STATE_RECEIVING;          
            break;
        case UART_CMD_PRESETPOSITION:
            printf("UART_CMD_PRESETPOSITION\n");    
            byte_num = UART_VLEN_PRESETPOSITION;
            c_state = UART_CMD_STATE_RECEIVING;          
            break;
        default:
            printf("DEFAULT --> RESET \n");
            resetCommand();        
            break;
    }
}

void handleReceivedValue(uint8_t val){
    values[num] = val;
    if(num == byte_num - 1) {
        c_state = UART_CMD_STATE_EXECUTE;
        printf("--> READY TO EXECUTE\n");
        return;} else {
        printf("--> WAITING FOR MORE VALUE \n");  
        num++;
        }
}

void executeCommand(uint8_t val){
    
    if(val != 0xa0) {
        resetCommand();
        printf("EXECUTE CODE INVALID \n");  
        return;
    } 
    
    printf("EXECUTE NOW \n");          

    switch (command) {
        case UART_CMD_SETCONTROLMODE:
            printf("SET_CONTROLMODE\n");    
            //motor_set_control_mode(values[0]);        
            break;
        case UART_CMD_SETSPEED:
        {
            printf("SET_SPEED\n");  
            float v[1];
            float_array_decode(values, byte_num, v);
            //motor_set_speed(v[0]);
            break;
        }
        case UART_CMD_SETSTOP:
            printf("SET_STOP\n");    
            //motor_set_stop();
            break;
        case UART_CMD_SETPOSITION:
        {
            printf("SET_POSITION\n");  
            float v[2];
            float_array_decode(values, byte_num, v);
            //motor_set_position(v[0], v[1], 0, 0); 
            break;
        }
        case UART_CMD_SETRELPOSITION:
        {  
            printf("SET_RELATIVE_POSITION\n");  
            float v[2];
            float_array_decode(values, byte_num, v);
            //motor_set_relative_position(v[0], v[1], 0, 0); 
            break;
        }
        case UART_CMD_PRESETPOSITION:
        {
            printf("PRESET_POSITION\n");  
            float v[1];
            float_array_decode(values, byte_num, v);
            //motor_preset_position(v[0]);
            break;
        }
        default:
            break;
    }
    exec_num++;
    printf("exec_num: %d\n", exec_num); 
    resetCommand();
    
}