/**
 *
 * @brief モーターのコントロールキューを管理する
 * todo::
 */

#ifndef CONTROL_QUE_MANAGER_H
#define CONTROL_QUE_MANAGER_H

#include "app_error.h"
#include "app_util_platform.h"
#include "app_fifo.h"

#include <stdbool.h>
#include <stdint.h>

#define KM_CONTROL_QUE_MAX_TASKS 16384 // 16kByte
#define KM_TASKSET_QUE_MAX_TASKS KM_CONTROL_QUE_MAX_TASKS // 16kByte

/**

* FIFOシステムベースのキュー管理

*/

/* KM_MOTOR_SERVICE */
// Forward declaration
typedef struct km_queue_s km_queue_t;

typedef struct km_task_s km_task_t;


/** @brief KM Control Que structure. This structure contains all options and data needed for
 *        initialization of the service.*/
struct km_queue_s
{
    app_fifo_t q_fifo;
	uint8_t *q_buffer;
	uint16_t q_buffer_size;
};

typedef struct 
{
	km_queue_t *control_q;
	km_queue_t *taskset_q;
	bool enable_control_pause; // コントロール一時停止・デフォルトオフ
    bool enable_record_taskset; // タスクセット記録モード・デフォルトオフ
	
} km_queue_manager_t;

// 不要？
/** @brief KM Control Que Task structure. This structure contains all options and data needed for
 *        initialization of the service.*/
struct km_task_s
{
    // int64_t unixtime64;
    uint16_t task_id;
    uint8_t * data;
    uint8_t data_len;
};


/**@brief Function
for initializing KM Control Que Service.
 *
 * @param[out] p_que KM Control Que structure. This structure must be supplied by
 *                        the application. It is initialized by this function and will later
 *                        be used to identify this particular service instance.
 * @param[in] p_que_init  Information needed to initialize the service.
 *
 * @retval NRF_SUCCESS If the service was initialized successfully. Otherwise, an error code is returned.
 */
uint32_t km_queue_init();

uint32_t km_control_queue_manager_init();

uint32_t km_control_queue_flush();

uint32_t km_control_queue_put(uint8_t data); // 1バイト put 
uint32_t km_control_queue_get(uint8_t *data); // 1バイト get

uint32_t km_control_queue_write(uint8_t *data, uint32_t *len); // 複数バイト put
uint32_t km_control_queue_read(uint8_t *data, uint32_t *len); // 複数バイト get

uint32_t km_taskset_queue_init();

uint32_t km_taskset_queue_flush();

uint32_t km_taskset_queue_put(uint8_t data); // 1バイト put 
uint32_t km_taskset_queue_get(uint8_t *data); // 1バイト get

uint32_t km_taskset_queue_write(uint8_t *data, uint32_t *len); // 複数バイト put
uint32_t km_taskset_queue_read(uint8_t *data, uint32_t *len); // 複数バイト get

uint32_t km_taskset_move_to_control_queue(); // タスクセットをコントロールキューに移動して実行待ちとする

/* 保存時 */
uint32_t km_taskset_add_header(); // 記録に必要なバイト長を算出し、ヘッダーに情報として追加する
uint32_t km_taskset_erase(uint16_t index);
uint32_t km_taskset_flash_write(uint16_t index, uint8_t *data, uint32_t *len); // 
uint32_t km_taskset_flash_read();


#endif // CONTROL_QUE_MANAGER_H