#include "km_buttons.h"
#include "app_button.h"
#include "km_io_definition.h"
#include "app_timer.h"
#include "nrf_log.h"
#include "test_mode_definition.h"
#include "teaching_playback.h"// 暫定TODO
#include "led_ws2812b.h"


#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50)                     /**< Delay from a GPIOTE event until a button is reported as pushed (in number of timer ticks). */

/**@brief Function for handling events from the button handler module.
 *
 * @param[in] pin_no        The pin that the event applies to.
 * @param[in] button_action The button action (press/release).
 */
static void button_event_handler(uint8_t pin_no, uint8_t button_action)
{
    uint32_t err_code;
    NRF_LOG_INFO("BUTTON_EVENT_HANDLER\n");

    if (button_action == APP_BUTTON_PUSH)
    {
        switch (pin_no)
        {
        case KM_SW_1:
            NRF_LOG_INFO("KM_SW_1\n");
            break;
        case KM_SW_2:
            NRF_LOG_INFO("KM_SW_2\n");

#ifdef PHASEDIFF_MANUAL		
            phase_diff -= 20;
            NRF_LOG_PRINTF("phase_diff: %d\r\n", phase_diff);
#endif
            teaching_start(1);

            break;
        case KM_SW_3:
			NRF_LOG_INFO("KM_SW_3\n");
		
#ifdef PHASEDIFF_MANUAL		
            phase_diff += 20;
            NRF_LOG_PRINTF("phase_diff: %d\r\n", phase_diff);
#endif
		    playback_start(1);
            break;
        default:
            APP_ERROR_HANDLER(pin_no);
            break;
        }
    }
}


/**@brief Function for initializing the button handler module.
 */
void buttons_init(void)
{
    uint32_t err_code;

    //The array must be static because a pointer to it will be saved in the button handler module.
    /* Active Low with PULLUP */
    static app_button_cfg_t buttons[] =
        {
			// プルアップを入れないと遅延や認識しない問題が発生する
            {KM_SW_1, APP_BUTTON_ACTIVE_LOW, GPIO_PIN_CNF_PULL_Pullup, button_event_handler}, 
            {KM_SW_2, APP_BUTTON_ACTIVE_LOW, GPIO_PIN_CNF_PULL_Pullup, button_event_handler},
            {KM_SW_3, APP_BUTTON_ACTIVE_LOW, GPIO_PIN_CNF_PULL_Pullup, button_event_handler}, 
		};

    err_code = app_button_init(buttons, sizeof(buttons) / sizeof(buttons[0]),
                               BUTTON_DETECTION_DELAY);
    APP_ERROR_CHECK(err_code);

    // Enable Buttons
    err_code = app_button_enable();
    APP_ERROR_CHECK(err_code);
}
