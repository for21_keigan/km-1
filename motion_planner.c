#include "motion_planner.h"
#include "fast_math.h"
#include "nrf_log.h"

void calc_motion_plan_velocity(mp_v_t *mpv){

	if(mpv->initial_velocity < mpv->target_velocity){
		
	}
    

}

float calc_target_velocity(const mp_v_t *mpv);


void calc_motion_plan_position(mp_p_t *mpp){

	mpp->distance = mpp->target_position - mpp->initial_position;	
	
	if(mpp->distance >= 0){
		
		mpp->direction = DIRECTION_FORWARD;
		
	} else { 
		// マイナス方向に進む
		mpp->direction = DIRECTION_REVERSE;
		mpp->distance = - mpp->distance;
	}	
	// TODO 初速度ありで、マイナスほうこうに進む、プラス方向に進む
	
	mpp->acc_duration = mpp->cruise_velocity / mpp->acc;
	mpp->dec_duration = mpp->cruise_velocity / mpp->dec;
	
	mpp->cruise_duration = mpp->distance/mpp->cruise_velocity - 0.5*(mpp->acc_duration + mpp->dec_duration); // TOＤＯ　マイナスになるかも
	
	if(mpp->cruise_duration < 0){
		
		mpp->shape = SHAPE_HEAD_TAIL;
		mpp->cruise_duration = 0;
		mpp->acc_duration = sqrt2( 2 * mpp->acc * mpp->distance / mpp->dec / ( mpp -> acc + mpp -> dec ));
		mpp->dec_duration = mpp->acc * mpp->acc_duration / mpp->dec;
		
	} else {
		
		mpp->shape = SHAPE_HEAD_BODY_TAIL;
	}
	
    mpp->total_duration = mpp->acc_duration + mpp->cruise_duration + mpp->dec_duration;
	
	NRF_LOG_DEBUG("shape: %d\r\n", mpp->shape);
	NRF_LOG_DEBUG("distance: %d\r\n", (int)(mpp->distance));
	NRF_LOG_DEBUG("acc_duration: %d\r\n", (int)(mpp->acc_duration * 1000));
	NRF_LOG_DEBUG("dec_duration: %d\r\n", (int)(mpp->dec_duration * 1000));
	NRF_LOG_DEBUG("cruise_duration: %d\r\n", (int)(mpp->cruise_duration * 1000));	
	NRF_LOG_DEBUG("total_duration: %d\r\n", (int)(mpp->total_duration * 1000));
	
}


float calc_target_position(const mp_p_t *mpp){
	
	float x;
	float time = mpp->time;
	
	if(time <= mpp->acc_duration){
		// 加速
		//NRF_LOG_DEBUG("ACC\r\n");
		x = 0.5 * mpp->acc * time * time;
		
	} else if (mpp->acc_duration < time && time <= mpp->acc_duration + mpp->cruise_duration){
		// 定速
		//NRF_LOG_DEBUG("CONST\r\n");

		x = mpp->cruise_velocity * (0.5 * mpp->acc_duration + time - mpp->acc_duration);
	
	} else if (mpp->acc_duration + mpp->cruise_duration < time && time < mpp->total_duration){
		// 減速
		//NRF_LOG_DEBUG("DEC\r\n");

		x = mpp->cruise_velocity * (0.5 * mpp->acc_duration + mpp->cruise_duration + 
			0.5 * (1-(time - mpp->total_duration) / mpp->dec_duration) * (time - mpp->acc_duration - mpp->cruise_duration));
	} else {
		// 制御時間後
		//NRF_LOG_DEBUG("AFTER\r\n");
		x = mpp->distance;
	}
	
	if(mpp->direction == DIRECTION_REVERSE){
		x = -x; // 符号マイナス
	}
	//NRF_LOG_DEBUG("x: %d\r\n", (int)x);
	
	return mpp->initial_position + x;
	
}

void calc_motion_plan_position_s(mp_p_t *mpp){

	mpp->distance = mpp->target_position - mpp->initial_position;	
	
	if(mpp->distance >= 0){
		mpp->direction = DIRECTION_FORWARD;
	} else { // マイナス方向に進む
		mpp->direction = DIRECTION_REVERSE;
		mpp->distance = - mpp->distance;
	}	
	// TODO 初速度ありで、マイナスほうこうに進む、プラス方向に進む
	
	mpp->acc_duration = mpp->cruise_velocity / mpp->acc;
	mpp->dec_duration = mpp->cruise_velocity / mpp->dec;
	mpp->cruise_duration = mpp->distance/mpp->cruise_velocity - 0.5*(mpp->acc_duration + mpp->dec_duration);
	mpp->total_duration = mpp->acc_duration + mpp->cruise_duration + mpp->dec_duration;
	
	NRF_LOG_DEBUG("distance: %d\r\n", (int)(mpp->distance));
	NRF_LOG_DEBUG("acc_duration: %d\r\n", (int)(mpp->acc_duration * 1000));
	NRF_LOG_DEBUG("dec_duration: %d\r\n", (int)(mpp->dec_duration * 1000));
	NRF_LOG_DEBUG("cruise_duration: %d\r\n", (int)(mpp->cruise_duration * 1000));	
	NRF_LOG_DEBUG("total_duration: %d\r\n", (int)(mpp->total_duration * 1000));

	mpp->sec_1_duration = mpp->acc_duration * mpp->acc_front_jerk_ratio * 2; 
  	mpp->sec_2_duration = mpp->acc_duration * (1 - (mpp->acc_front_jerk_ratio + mpp->acc_back_jerk_ratio)); 
  	mpp->sec_3_duration = mpp->acc_duration * mpp->acc_back_jerk_ratio * 2; 
  	mpp->sec_4_duration = mpp->cruise_duration - mpp->acc_back_jerk_ratio * mpp->acc_duration
	 - mpp-> dec_front_jerk_ratio * mpp-> dec_duration; 
	mpp->sec_5_duration = mpp->dec_duration * mpp-> dec_front_jerk_ratio * mpp-> dec_duration * 2;
  	mpp->sec_6_duration = mpp->dec_duration * (1 - (mpp->dec_front_jerk_ratio + mpp->dec_back_jerk_ratio)); 
	mpp->sec_7_duration = mpp->dec_duration * mpp->dec_back_jerk_ratio * 2; 	
}

float calc_target_position_s(const mp_p_t *mpp){
	
	float x;
	float t = mpp->time;
	float t_1 = mpp->sec_1_duration;
	float t_2 = t_1 + mpp->sec_2_duration;
	float t_3 = t_2 + mpp->sec_3_duration;
	float t_4 = t_3 + mpp->sec_4_duration;
	float t_5 = t_4 + mpp->sec_5_duration;
	float t_6 = t_5 + mpp->sec_6_duration;
	float t_7 = t_6 + mpp->sec_7_duration;
	
	if(t < t_1){
	
	} else if (t < t_2){
	
	} else if (t < t_3){
	
	} else if (t < t_4){
		
	} else if (t < t_5){
		
	} else if (t < t_6){
		
	} else if (t < t_7){
		
	} else {
		// 制御後
	}

}

