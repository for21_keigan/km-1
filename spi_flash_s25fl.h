/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef SPI_FLASH_S25FL_H
#define SPI_FLASH_S25FL_H

#include "app_util_platform.h"

/** Counts number of elements inside the array
 */
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
	
void flash_s25fl_init_with_nonblocking(void);
void flash_s25fl_init_with_blocking(void);
void flash_s25fl_uninit(void);

void flash_s25fl_read_id(uint8_t *manu_id, uint8_t *mem_type, uint8_t *cap);
uint8_t flash_s25fl_read_status(void);
	
void flash_s25fl_wait_ready(void);
void flash_s25fl_write_enable(void);
void flash_s25fl_erase_4k(uint32_t loc);
void flash_s25fl_erase_64k(uint32_t loc);
void flash_s25fl_erase_all(void);
void flash_s25fl_read(uint32_t loc, uint8_t* array, uint32_t length);
void flash_s25fl_write(uint32_t loc, uint8_t* array, uint32_t length);
void flash_s25fl_write_reg(uint8_t w);

/**
 * S25FL116K Main Memory Address Map
 *
 * Sector Size :    4 kbyte (4096 byte)
 * Sector Count:  512
 * Sector range     Address Range(Byte Address)
 *    SA0            000000h-000FFFh        0-   4095 
 *    SA1            001000h-001FFFh     4096-   8191
 *                          |
 *    SA511          1FF000h-1FFFFFh  2093056-2097151
 * 
 */
 
#endif // SPI_FLASH_S25FL_H

