#ifndef VECTOR_CONTROL_MATH
#define VECTOR_CONTROL_MATH

#include <stdio.h>
#include <stdint.h>
#include "fast_math.h"
#include "compiler_abstraction.h"

// ●使用 電流UVW->αβ座標変換
static __INLINE void I_uvw2abo(float *inI_uvw, float *outI_abo)
{
    outI_abo[0] = 0.81649658f * (inI_uvw[0] - inI_uvw[1] / 2 - inI_uvw[2] / 2); //0.81649658=√(2/3)
    outI_abo[1] = 0.70710678f * (inI_uvw[1] - inI_uvw[2]);                      //0.70710678=1/√2
    outI_abo[2] = 0.57735027f * (inI_uvw[0] - inI_uvw[1] / 2 - inI_uvw[2] / 2); //0.57735027=1/√3
}


// ●使用 電流αβ->dq座標変換
static __INLINE void I_abo2dq(float sin_th, float cos_th, float *inI_abo, float *outI_dq)
{
    outI_dq[0] =   cos_th * inI_abo[0] + sin_th * inI_abo[1];
    outI_dq[1] = - sin_th * inI_abo[0] + cos_th * inI_abo[1];
}

// ●使用 電圧dq->αβ座標変換 
static __INLINE void V_dq2abo(float sin_th, float cos_th, float *inV_dq, float *outV_abo)
{
    outV_abo[0] = cos_th * inV_dq[0] - sin_th * inV_dq[1];
    outV_abo[1] = sin_th * inV_dq[0] + cos_th * inV_dq[1];
    //outV_abo[2] = 0;
}

// 電流dq->αβ座標変換
static __INLINE void I_dq2abo(float sin_th, float cos_th, float *inI_dq, float *outI_abo)
{
    outI_abo[0] = cos_th * inI_dq[0] - sin_th * inI_dq[1];
    outI_abo[1] = sin_th * inI_dq[0] + cos_th * inI_dq[1];
    //outI_abo[2] = 0;
}


// ●使用 電圧αβ->UVW座標変換
static __INLINE void V_abo2uvw(float *inV_abo, float *outV_uvw)
{
    outV_uvw[0] =  0.81649658f * inV_abo[0];                         //0.81649658=√(2/3)
    outV_uvw[1] = -0.40824829f * inV_abo[0] + 0.70710678f * inV_abo[1]; //0.70710678=1/√2, 0.40824829 =1/√6
    outV_uvw[2] = -0.40824829f * inV_abo[0] - 0.70710678f * inV_abo[1]; //0.57735027=1/√3
}

//電流αβ->UVW座標変換
static __INLINE void I_abo2uvw(float *inI_abo, float *outI_uvw)
{
    outI_uvw[0] =  0.81649658f * inI_abo[0] ; // + 0.57735027f * inI_abo[2] / 2;                         //0.81649658=√(2/3)
    outI_uvw[1] = -0.40824829f * inI_abo[0] + 0.70710678f * inI_abo[1]; // + 0.57735027f * inI_abo[2]; //0.70710678=1/√2, 0.40824829 =1/√6
    outI_uvw[2] = -0.40824829f * inI_abo[0] - 0.70710678f * inI_abo[1]; // + 0.57735027f * inI_abo[2]; //0.57735027=1/√3
}



/* 3次調波加算 */
static __INLINE void Harmo3(float *outV_uvw)
{
    //static uint8_t VP_MODE;
    float Vmax; /* 最大電圧（を選ぶ） */
    float Vmin; /* 最小電圧（を選ぶ） */

    if (outV_uvw[0] > outV_uvw[1])
    {
        if (outV_uvw[0] > outV_uvw[2]) /* Max=Vu */
        {
            Vmax = outV_uvw[0];
            if (outV_uvw[1] > outV_uvw[2])
            {
                Vmin = outV_uvw[2]; /* Min=Vw */
                //VP_MODE = 2;        /* U > V > W */
            }
            else
            {
                Vmin = outV_uvw[1]; /* Min=Vv */
                //VP_MODE = 1;        /* U > W > V */
            }
        }
        else
        {
            Vmax = outV_uvw[2]; /* Max=Vw */
            Vmin = outV_uvw[1]; /* Min=Vv */
            //VP_MODE = 6;        /* W > U > V */
        }
    }
    else
    {
        if (outV_uvw[1] > outV_uvw[2]) /* Max=Vv */
        {
            Vmax = outV_uvw[1];
            if (outV_uvw[0] > outV_uvw[2])
            {
                Vmin = outV_uvw[2]; /* Min=Vw */
                //VP_MODE = 3;        /* V > U > W */
            }
            else
            {
                Vmin = outV_uvw[0]; /* Min=Vu */
                //VP_MODE = 4;        /* V > W > U */
            }
        }
        else
        {
            Vmax = outV_uvw[2]; /* Max=Vw */
            Vmin = outV_uvw[0]; /* Min=Vu */
            //VP_MODE = 5;        /* W > V > U */
        }
    }

    float Vzero = (Vmax + Vmin) * 0.5; /* MaxとMinの平均値 */
    outV_uvw[0] = outV_uvw[0] - Vzero;
    outV_uvw[1] = outV_uvw[1] - Vzero;
    outV_uvw[2] = outV_uvw[2] - Vzero;
}




#endif // VECTOR_CONTROL_MATH
