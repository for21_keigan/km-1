#include "motor_setting.h"
#include "spi_flash_s25fl.h"
#include "km_error.h"
#include "type_utility.h"
#include "app_util.h"
#include "motor_control.h"

#define PHASEDIFF_SIZE 2

typedef struct {

    bool is_factory_mode_enabled;

} setting_mode_t;

setting_mode_t setting_mode = {

    .is_factory_mode_enabled = false
};


void setting_factory_mode_on(bool is_enabled){

    setting_mode.is_factory_mode_enabled = is_enabled;
	flash_s25fl_init_with_blocking(); // ブロッキングモードで初期化しておく
}

void setting_factory_mode_off(){

    setting_mode.is_factory_mode_enabled = false;
}

bool read_factory_mode_on()
{
    return setting_mode.is_factory_mode_enabled;
} 

uint32_t setting_write_serial_number(uint8_t *sn, uint32_t length){

	// サイズチェックはBLE受信時に行っている
    if(read_factory_mode_on() == false){
		NRF_LOG_DEBUG("It is not a factory mode.\r\n");
        return KM_ERROR_FORBIDDEN;
    }

    flash_s25fl_write(SETTING_ADDR_SERIAL_NUMBER, sn, length);
    return KM_SUCCESS;
}



uint32_t setting_write_phase_diff(uint8_t *diff){

	// サイズチェックはBLE受信時に行っている
    if(read_factory_mode_on() == false){
		NRF_LOG_DEBUG("It is not a factory mode.\r\n");
        return KM_ERROR_FORBIDDEN;
    }
	
	uint16_t pd = uint16_big_decode(diff);
	motor_set_phase_diff(pd);
    flash_s25fl_write(SETTING_ADDR_PHASE_DIFF, diff, PHASEDIFF_SIZE);
    
    return KM_SUCCESS;

}

uint32_t setting_read_phase_diff(){
	
	// サイズチェックはBLE受信時に行っている
    if(read_factory_mode_on() == false){
		NRF_LOG_DEBUG("It is not a factory mode.\r\n");
        return KM_ERROR_FORBIDDEN;
    }
	
	uint8_t data[PHASEDIFF_SIZE] = {0};
    flash_s25fl_read(SETTING_ADDR_PHASE_DIFF, data, PHASEDIFF_SIZE);
	uint16_t diff = uint16_big_decode(data);
	NRF_LOG_DEBUG("phase_diff: %d (RAM), %d(external).\r\n", motor_get_phase_diff(), diff);
	// ブロッキングモードでログを出すべき？
    return KM_SUCCESS;
	
}