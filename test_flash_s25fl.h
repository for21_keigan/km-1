/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef TEST_FLASH_S25FL_H
#define TEST_FLASH_S25FL_H

#include "app_util_platform.h"

#include "flash_address_definition.h"

void test_flash_s25fl(uint8_t *read_buf);

#endif // TEST_FLASH_S25FL_H

