#include <math.h>
#include "command_control.h"
#include "motor_control.h"
#include "km_error.h"
#include "type_utility.h"
#include "app_timer.h"

#define APP_TIMER_PRESCALER 0
#define TIMEOUT_CHECK_INT_MS 1

// 成功判定の誤差
#define VELOCITY_TOLERANCE_PERCENT 5 // 速度はパーセンテージ TODO
#define POSITION_TOLERANCE 0.005     // [rad] 0.2864 deg.
#define TORQUE_TOLERANCE 0.02        // [Nm] 約 200g・cm TODO

/* App Timer （RTC1によるタイマー） */
APP_TIMER_DEF(command_timeout_timer_id);

// モーターの状態を表す構造体
typedef struct motor_status
{

    bool is_taskset_queue_enabled;
    bool is_direct_teaching_enabled;

} motor_status_t;

motor_status_t status = {

    .is_taskset_queue_enabled = false,
    .is_direct_teaching_enabled = false

};

typedef enum {

    MOTOR_CMD_RESULT_UNKNOWN,
    MOTOR_CMD_RESULT_SUCCESS,
    MOTOR_CMD_RESULT_FAILED

} motor_command_result_t;

typedef enum {

    MOTOR_CMD_TYPE_UNKNOWN,
    MOTOR_CMD_TYPE_VELOCITY,
    MOTOR_CMD_TYPE_POSITION,
    MOTOR_CMD_TYPE_TORQUE,
    MOTOR_CMD_TYPE_WAIT,
    MOTOR_CMD_TYPE_TASKSET,
    MOTOR_CMD_TYPE_MOTION // ダイレクトティーチング

} motor_command_type_t;

// 監視必要なコマンドの構造体
typedef struct observed_command
{

    bool active;
    uint8_t cmd;
    motor_command_type_t type;
    uint16_t id;
    uint32_t timeout;
    bool completed;
    motor_command_result_t result;
    float target_value;

} observed_command_t;

observed_command_t observed_cmd = {

    .active = false,
    .cmd = 0x00,
    .type = MOTOR_CMD_TYPE_UNKNOWN,
    .id = 0,
    .timeout = 0,
    .completed = false,
    .result = MOTOR_CMD_RESULT_UNKNOWN,
    .target_value = 0

};

typedef enum {

    DATA_RECEIVING_STATE_WAIT_CMD,  // 最初またはコマンド実行直後だけ。コマンド待ち状態。
    DATA_RECEIVING_STATE_WAIT_ARGS, // １つ目のバイト＝コマンド受付完了。データ待ち状態
    DATA_RECEIVING_STATE_EXECUTE    // コマンド実行

} data_receiving_state_type_t;

// バイトデータ処理状態を管理する構造体
typedef struct
{

    data_receiving_state_type_t state;
    uint16_t data_len;
    uint16_t data_cnt;
    uint8_t data[128];
    void (*execute_command)(uint8_t *data, uint16_t len); // 関数ポインタ

} data_receiving_t;

data_receiving_t data_recv = {

    .state = DATA_RECEIVING_STATE_WAIT_CMD,
    .data_len = 0,
    .data_cnt = 0,
    {0} // data[128] の中身全部ゼロ。こう書くしかない？
};

static void reset_command()
{

    data_recv.state = DATA_RECEIVING_STATE_WAIT_CMD;
    data_recv.data_len = 0;
    data_recv.data_cnt = 0;

    // 全てゼロで初期化。memsetは処理系依存らしいが、uint8_tの配列なので問題ないだろう。float, double とかは注意。
    memset(data_recv.data, 0, sizeof(data_recv.data));
}

void act_disable(uint8_t *data, uint16_t len)
{

    motor_disable();
}

static void act_enable(uint8_t *data, uint16_t len)
{

    motor_enable();
}

void act_stop(uint8_t *data, uint16_t len)
{

    motor_stop();
}

static void act_brake(uint8_t *data, uint16_t len)
{

    motor_brake();
}

void act_free(uint8_t *data, uint16_t len)
{

    motor_free();
}

void act_speed(uint8_t *data, uint16_t len)
{
    uint16_t id = get_uint16t_from_args(data, len, 1);
    float v;
    if (get_float_from_args(data, len, 3, &v))
    {
        motor_preset_speed(v);
        //log_output_result(id, KM_SUCCESS);
    }
    else
    {
        NRF_LOG_DEBUG("KM_ERROR_INVALID_DATA\r\n");
        //log_output_result(id, KM_ERROR_INVALID_DATA);
    }
}

void act_run_forward(uint8_t *data, uint16_t len)
{
    uint16_t id = get_uint16t_from_args(data, len, 2);
    float v;
    if (get_float_from_args(data, len, 3, &v))
    {
        motor_preset_speed(v);
        //log_output_result(id, KM_SUCCESS);
    }
    else
    {
        //log_output_result(id, KM_ERROR_INVALID_DATA);
    }
    motor_run_forward();
}

void act_run_reverse(uint8_t *data, uint16_t len)
{
    motor_run_reverse();
}

void act_run_at_velocity(uint8_t *data, uint16_t len)
{
	float vel;
	if (get_float_from_args(data, len, 3, &vel))
	{
		motor_run_at_velocity(vel);
		NRF_LOG_DEBUG("KM_SUCCESS\r\n");
	}
	else
	{
		NRF_LOG_DEBUG("KM_ERROR_INVALID_DATA\r\n");
	}
}

// コマンドに応じて引数を含む全要素数をセットする
static void set_data_len(uint8_t cmd)
{
    switch (cmd)
    {
    // 動作に関するコマンド・優先高い順から
    case CMD_ACT_DISABLE:
        data_recv.data_len = CMD_ACT_DISABLE_LEN;
        data_recv.execute_command = act_disable;
        break;
    case CMD_ACT_ENABLE:
        data_recv.data_len = CMD_ACT_ENABLE_LEN;
        data_recv.execute_command = act_enable;
        break;
    case CMD_ACT_STOP:
        data_recv.data_len = CMD_ACT_STOP_LEN;
        data_recv.execute_command = act_stop;
        break;
    case CMD_ACT_BRAKE:
        data_recv.data_len = CMD_ACT_BRAKE_LEN;
        break;
    case CMD_ACT_FREE:
        data_recv.data_len = CMD_ACT_FREE_LEN;
        break;
    case CMD_ACT_SPEED:
        data_recv.data_len = CMD_ACT_SPEED_LEN;
        break;
    case CMD_ACT_PRESET_POSITION:
        data_recv.data_len = CMD_ACT_PRESET_POSITION;
        break;
    case CMD_ACT_RUN_FORWARD:
        data_recv.data_len = CMD_ACT_RUN_FORWARD_LEN;
        break;
    case CMD_ACT_RUN_REVERSE:
        data_recv.data_len = CMD_ACT_RUN_REVERSE_LEN;
        break;
    case CMD_ACT_RUN_AT_VELOCITY:
        data_recv.data_len = CMD_ACT_RUN_AT_VELOCITY_LEN;
        break;
    case CMD_ACT_MOVE_TO_POS_AT_SPEED:
        data_recv.data_len = CMD_ACT_MOVE_TO_POS_AT_SPEED_LEN;
        break;
    case CMD_ACT_MOVE_TO_POS:
        data_recv.data_len = CMD_ACT_MOVE_TO_POS_LEN;
        break;
    case CMD_ACT_MOVE_BY_DIST_AT_SPEED:
        data_recv.data_len = CMD_ACT_MOVE_BY_DIST_AT_SPEED_LEN;
        break;
    case CMD_ACT_MOVE_BY_DIST:
        data_recv.data_len = CMD_ACT_MOVE_BY_DIST_LEN;
        break;
    case CMD_ACT_HOLD_TORQUE:
        data_recv.data_len = CMD_ACT_HOLD_TORQUE_LEN;
        break;
    case CMD_ACT_DO_TASKSET:
        data_recv.data_len = CMD_ACT_DO_TASKSET;
        break;
    case CMD_ACT_READY_FOR_MOTION:
        data_recv.data_len = CMD_ACT_READY_FOR_MOTION_LEN;
        break;
    case CMD_ACT_PLAY_MOTION:
        data_recv.data_len = CMD_ACT_PLAY_MOTION_LEN;
        break;
    case CMD_QUE_PAUSE:
        data_recv.data_len = CMD_QUE_PAUSE_LEN;
        break;
    case CMD_QUE_RESUME:
        data_recv.data_len = CMD_QUE_RESUME_LEN;
        break;
    case CMD_QUE_WAIT:
        data_recv.data_len = CMD_QUE_WAIT_LEN;
        break;
    case CMD_QUE_ERASE_TASK:
        data_recv.data_len = CMD_QUE_ERASE_TASK_LEN;
        break;
    case CMD_QUE_ERASE_ALL_TASKS:
        data_recv.data_len = CMD_QUE_ERASE_ALL_TASKS_LEN;
        break;
    // レジスタ設定
    case CMD_REG_MAX_SPEED:
        data_recv.data_len = CMD_REG_MAX_SPEED_LEN;
        break;
    case CMD_REG_MIN_SPEED:
        data_recv.data_len = CMD_REG_MIN_SPEED_LEN;
        break;
    case CMD_REG_ACC:
        data_recv.data_len = CMD_REG_ACC_LEN;
        break;
    case CMD_REG_DEC:
        data_recv.data_len = CMD_REG_DEC_LEN;
        break;
    case CMD_REG_MAX_TORQUE:
        data_recv.data_len = CMD_REG_MAX_TORQUE_LEN;
        break;
    case CMD_REG_SPEED_PID:
        data_recv.data_len = CMD_REG_SPEED_PID_LEN;
        break;
    case CMD_REG_POSITION_P:
        data_recv.data_len = CMD_REG_POSITION_P_LEN;
        break;
    case CMD_REG_RESET_PID:
        data_recv.data_len = CMD_REG_RESET_PID_LEN;
        break;
    case CMD_REG_LIMIT_CURRENT:
        data_recv.data_len = CMD_REG_LIMIT_CURRENT_LEN;
        break;
    case CMD_REG_MOTOR_MEASUREMENT:
        data_recv.data_len = CMD_REG_MOTOR_MEASUREMENT_LEN;
        break;
    case CMD_REG_INTERFACE:
        data_recv.data_len = CMD_REG_INTERFACE_LEN;
        break;
    case CMD_REG_LOG_OUTPUT:
        data_recv.data_len = CMD_REG_LOG_OUTPUT;
        break;
    case CMD_REG_OWN_COLOR:
        data_recv.data_len = CMD_REG_OWN_COLOR_LEN;
        break;
    case CMD_REG_IMU_MEASUREMENT:
        data_recv.data_len = CMD_REG_IMU_MEASUREMENT_LEN;
        break;
    case CMD_REG_READ_REGISTER:
        data_recv.data_len = CMD_REG_READ_REGISTER_LEN;
        break;
    case CMD_REG_RESET_REGISTER:
        data_recv.data_len = CMD_REG_RESET_REGISTER_LEN;
        break;
    case CMD_REG_RESET_ALL_REGISTERS:
        data_recv.data_len = CMD_REG_RESET_ALL_REGISTERS_LEN;
        break;
    // タスクセット（コマンド記憶）
    case CMD_T_START_RECORD_TASKSET:
        data_recv.data_len = CMD_T_START_RECORD_TASKSET_LEN;
        break;
    case CMD_T_SAVE_TASKSET:
        data_recv.data_len = CMD_T_SAVE_TASKSET_LEN;
        break;
    case CMD_T_ERASE_TASKSET:
        data_recv.data_len = CMD_T_ERASE_TASKSET_LEN;
        break;
    case CMD_T_ERASE_ALL_TASKSETS:
        data_recv.data_len = CMD_T_ERASE_ALL_TASKSETS_LEN;
        break;
    // ダイレクトティーチングによるモーション記憶
    case CMD_DT_START_RECORD_MOTION:
        data_recv.data_len = CMD_DT_START_RECORD_MOTION_LEN;
        break;
    case CMD_DT_STOP_RECORD_MOTION:
        data_recv.data_len = CMD_DT_STOP_RECORD_MOTION_LEN;
        break;
    case CMD_DT_ERASE_MOTION:
        data_recv.data_len = CMD_DT_ERASE_MOTION_LEN;
        break;
    case CMD_DT_ERASE_ALL_MOTION:
        data_recv.data_len = CMD_DT_ERASE_ALL_MOTION_LEN;
        break;
    // LED
    case CMD_LED_SET_LED:
        data_recv.data_len = CMD_LED_SET_LED_LEN;
        break;
    case CMD_LED_GET_LED_STATE:
        data_recv.data_len = CMD_LED_GET_LED_STATE_LEN;
        break;
    // その他
    case CMD_OTHERS_REBOOT:
        data_recv.data_len = CMD_OTHERS_REBOOT_LEN;
        break;
    default:
        break;
    }
}

//static uint32_t handle_byte_data(uint8_t data)
//{

//    static int i = 0;
//    if (data_recv.state == DATA_RECEIVING_STATE_WAIT_CMD)
//    {

//        data_recv.data[0] = data;
//        data_recv.state = DATA_RECEIVING_STATE_WAIT_ARGS;
//        i++;
//        set_data_len(data);
//    }
//    else if (data_recv.state == DATA_RECEIVING_STATE_WAIT_ARGS)
//    {

//        if (data_recv.data_len == i)
//        {
//            data_recv.execute_command(&data, data_recv.data_len); // コマンドを実行
//            i = 0;
//            reset_command(); // コマンドをゼロに
//        }
//        else
//        {
//            data_recv.data[i] = data;
//            i++;
//        }
//    }
//}


//// 前方宣言
//static void process_judgement();

//// モーター動作の成功判定
//uint32_t command_result_judgement()
//{

//    observed_cmd.completed = true;

//    float d = 0;
//    if (observed_cmd.type == MOTOR_CMD_TYPE_VELOCITY)
//    {

//        d = (observed_cmd.target_value - motor_get_smoothed_velocity()) / (observed_cmd.target_value);
//        if (fabs(d) < VELOCITY_TOLERANCE_PERCENT)
//            return MOTOR_CMD_RESULT_SUCCESS;
//        else
//            MOTOR_CMD_RESULT_FAILED;
//    }
//    else if (observed_cmd.type == MOTOR_CMD_TYPE_POSITION)
//    {

//        d = observed_cmd.target_value - motor_get_position();
//        if (fabs(d) < POSITION_TOLERANCE)
//            return MOTOR_CMD_RESULT_SUCCESS;
//        else
//            MOTOR_CMD_RESULT_FAILED;
//    }
//    else if (observed_cmd.type == MOTOR_CMD_TYPE_TORQUE)
//    {

//        d = observed_cmd.target_value - motor_get_torque();
//        if (fabs(d) < TORQUE_TOLERANCE)
//            return MOTOR_CMD_RESULT_SUCCESS;
//        else
//            MOTOR_CMD_RESULT_FAILED;
//    }
//    else if (observed_cmd.type == MOTOR_CMD_TYPE_WAIT)
//    {

//        start_queue();
//        return MOTOR_CMD_RESULT_SUCCESS;
//    }
//    else
//    {
//        return MOTOR_CMD_RESULT_FAILED; // TODO
//    }
//}


//static void command_obs_timeout_handler(void *p_context)
//{
//    NRF_LOG_DEBUG("command timeout \r\n");
//    process_judgement();
//}

//static void command_obs_timer_init()
//{

//    ret_code_t err_code;
//    // リピートなしタイマー。
//    err_code = app_timer_create(&command_obs_timer_id, APP_TIMER_MODE_SINGLE_SHOT, command_obs_timeout_handler);
//    APP_ERROR_CHECK(err_code);
//}

//static void command_obs_timer_start(uint32_t timeout)
//{
//    uint32_t err_code = NRF_SUCCESS;
//    err_code = app_timer_start(command_obs_timer_id, APP_TIMER_TICKS(timeout, APP_TIMER_PRESCALER), NULL);
//    APP_ERROR_CHECK(err_code);
//}

//static void command_obs_timer_stop(void)
//{
//    uint32_t err_code = NRF_SUCCESS;
//    err_code = app_timer_stop(command_obs_timer_id);
//    APP_ERROR_CHECK(err_code);
//}

//// Set Command Observation
//static void set_observed_cmd(uint8_t cmd, uint8_t type, uint16_t id, uint32_t timeout, float target_value)
//{

//    observed_cmd.active = true;
//    observed_cmd.cmd = cmd;
//    observed_cmd.type = type;
//    observed_cmd.id = id;
//    observed_cmd.timeout = timeout;
//    observed_cmd.target_value = target_value;

//    command_obs_timer_start(timeout);
//}

//static void process_judgement()
//{

//    observed_cmd.result = command_result_judgement();
//    log_output_result(observed_cmd.id, observed_cmd.result);
//}

//static void reset_observed_cmd()
//{

//    if (observed_cmd.active)
//    {
//        process_judgement();
//    }

//    observed_cmd.active = false;
//    observed_cmd.cmd = 0x00;
//    observed_cmd.type = MOTOR_CMD_TYPE_UNKNOWN;
//    observed_cmd.id = 0;
//    observed_cmd.timeout = 0;
//    observed_cmd.completed = false;
//    observed_cmd.result = MOTOR_CMD_RESULT_UNKNOWN;
//    observed_cmd.target_value = 0;
//}

//static void command_enable()
//{
//    motor_enable();
//    log_output_result();
//}

//static void command_disable()
//{
//    motor_disable();
//}

//static void command_get_state(uint16_t *state)
//{
//    // TODO
//}

//static void command_preset_position(float position)
//{
//    // TODO
//    motor_preset_position(position);
//}

///*
// * 速度制御：定常速度は velocity
// * | CMD | V[0] | V[1] | V[2] | V[3] | I[1] | I[2] | T[0] | T[2] | T[3] | T[4] | 
//*/
//static void command_run(uint16_t id, float velocity, uint32_t timeout)
//{
//    reset_observed_cmd();
//    set_observed_cmd(CMD_CONTROL_RUN_AT_VELOCITY, MOTOR_CMD_TYPE_VELOCITY, id, timeout, velocity);
//    motor_set_velocity(velocity);
//}

///*
// * 位置制御：絶対位置を distance で指定。定常速度は velocity
// * | CMD | P[0] | P[1] | P[2] | P[3] | V[0] | V[1] | V[2] | V[3] | I[1] | I[2] | T[0] | T[2] | T[3] | T[4] | 
//*/
//static void command_move_to(uint16_t id, float position, float velocity, uint32_t timeout)
//{
//    reset_observed_cmd();
//    set_observed_cmd(CMD_CONTROL_MOVE_TO_POSITION, MOTOR_CMD_TYPE_POSITION, id, timeout, position);
//    motor_set_position(position, velocity);
//}

///*
// * 位置制御：相対位置を distance で指定。定常速度は velocity
// * | CMD | P[0] | P[1] | P[2] | P[3] | V[0] | V[1] | V[2] | V[3] | I[1] | I[2] | T[0] | T[2] | T[3] | T[4] | 
//*/
//static void command_move_by(uint16_t id, float distance, float velocity, uint32_t timeout)
//{
//    reset_observed_cmd();
//    position = motor_get_position() + distance;
//    set_observed_cmd(CMD_CONTROL_MOVE_BY_DISTANCE, MOTOR_CMD_TYPE_POSITION, id, timeout, position);
//    motor_set_position(position, velocity);
//}

//static void command_stop()
//{
//    reset_observed_cmd();
//    set_observed_cmd(CMD_CONTROL_STOP, MOTOR_CMD_TYPE_VELOCITY, id, timeout, 0);
//    motor_set_stop();
//}

//static void command_brake()
//{
//    motor_set_brake();
//}

//static void command_hold(uint16_t id, float torque, uint32_t timeout)
//{
//    reset_observed_cmd();
//    set_observed_cmd(CMD_CONTROL_HOLD_TORQUE, MOTOR_CMD_TYPE_TORQUE, id, timeout, torque);
//    motor_set_torque(torque);
//}

//static void command_wait(uint16_t id, uint32_t time)
//{
//    reset_observed_cmd();
//    set_observed_cmd(CMD_CONTROL_WAIT, MOTOR_CMD_TYPE_WAIT, id, time, 0);
//    pause_queue(); // wait中はその他の「動作」コマンドを受け付けない
//}

//int motor_record_start(uint16_t id)
//{
//    NRF_LOG_DEBUG("motor_record_start\r\n");
//    control_mode = C_TEST_RECORD;
//    ws2812b_drive_set_color(128, 0, 0);
//}

//int motor_record_stop()
//{
//    control_mode = C_TEST_POSITION;
//    motor_set_position(rec_start_rotation, t_speed, 0, 0); // 暫定
//    rec_end_flag = true;
//}

//int motor_record_play(uint16_t id)
//{
//    control_mode = C_TEST_PLAY;
//    motorStart();
//    t_speed = RPM_TO_TPMS(RECORD_PLAY_RPM); // TODO
//    ws2812b_drive_set_color(0, 128, 0);
//}
