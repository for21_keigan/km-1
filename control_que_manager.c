#include "control_que_manager.h"

km_queue_manager_t *q_manager;
uint8_t control_queue_buffer[KM_CONTROL_QUE_MAX_TASKS];
uint8_t taskset_queue_buffer[KM_TASKSET_QUE_MAX_TASKS];


uint32_t km_control_queue_manager_init(){
	
	q_manager->control_q->q_buffer = control_queue_buffer;
	q_manager->control_q->q_buffer_size = KM_CONTROL_QUE_MAX_TASKS;
	app_fifo_init(&q_manager->control_q->q_fifo, control_queue_buffer, KM_CONTROL_QUE_MAX_TASKS);	

	q_manager->taskset_q->q_buffer = taskset_queue_buffer;
	q_manager->taskset_q->q_buffer_size	= KM_TASKSET_QUE_MAX_TASKS;	
	app_fifo_init(&q_manager->taskset_q->q_fifo, taskset_queue_buffer, KM_TASKSET_QUE_MAX_TASKS);		
}

// キュー流し
uint32_t km_control_queue_flush(){
	app_fifo_flush(&q_manager->control_q->q_fifo);
}

// 1バイト put 
uint32_t km_control_queue_put(uint8_t data){
	
	app_fifo_put(&q_manager->control_q->q_fifo, data);
}

// 1バイト get
uint32_t km_control_queue_get(uint8_t *data){
	
	app_fifo_get(&q_manager->control_q->q_fifo, data);	
}

// 複数バイト put
uint32_t km_control_queue_write(uint8_t *data, uint32_t *len){
	
	app_fifo_write(&q_manager->control_q->q_fifo, data, len);
}

// 複数バイト get
uint32_t km_control_queue_read(uint8_t *data, uint32_t *len){
	
	app_fifo_read(&q_manager->control_q->q_fifo, data, len);
}

// キュー流し
uint32_t km_taskset_queue_flush(){
	
	app_fifo_flush(&q_manager->taskset_q->q_fifo);	
}

// 1バイト put
uint32_t km_taskset_queue_put(uint8_t data){
	
	app_fifo_put(&q_manager->taskset_q->q_fifo, data);
}	 

// 1バイト get
uint32_t km_taskset_queue_get(uint8_t *data){
	
	app_fifo_get(&q_manager->taskset_q->q_fifo, data);	
}

// 複数バイト put
uint32_t km_taskset_queue_write(uint8_t *data, uint32_t *len){
	
	app_fifo_write(&q_manager->taskset_q->q_fifo, data, len);	
}

// 複数バイト get
uint32_t km_taskset_queue_read(uint8_t *data, uint32_t *len){
	
	app_fifo_read(&q_manager->taskset_q->q_fifo, data, len);	
}

// タスクセットをコントロールキューに移動して実行待ちとする
uint32_t km_taskset_move_to_control_queue(){
	
	
	
}

/* 保存時 */
uint32_t km_taskset_add_header(); // 記録に必要なバイト長を算出し、ヘッダーに情報として追加する
uint32_t km_taskset_erase(uint16_t index);
uint32_t km_taskset_flash_write(uint16_t index, uint8_t *data, uint32_t *len); // 
uint32_t km_taskset_flash_read();

