#ifndef km_gap_params_h
#define km_gap_params_h

#include <ble.h>

void gap_params_init(void);
void gap_params_on_ble_event(ble_evt_t * p_ble_evt);

// GAPのデバイス名を設定します。p_device_nameは0終端指定なくともよいUTF-8の文字列。lengthは文字列の長さ。
void gap_params_set_device_name(uint8_t *p_device_name, uint16_t length);
// GAPのデバイス名を取得します。
uint16_t gap_params_get_device_name(uint8_t *p_device_name, uint16_t length);
#endif /* km_gap_params */
