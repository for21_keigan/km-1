/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef TEST_MODE_DEFINITION_H
#define TEST_MODE_DEFINITION_H

/* モーターテスト */
//#define MOTOR_TEST

/* 位置制御テスト */
//#define POSITION_TEST
#define POSITION_TEST_REL (1400)
#define POSITION_TEST_RPM 20

/* 入力保存再生テスト */
//#define RECORD_TEST
#define RECORD_MAX_COUNTS 3000 // 記憶時間 CONTROL_INT_MS * 10 = 2.2 ms ならば、3000 * 2.2 = 6600ms
#define RECORD_FREQ_TIMES 10 /* TODO n回に1回記録を行う。再生時も使用。*/
#define RECORD_PLAY_RPM 50

/* 再生のみテスト */
//#define REPLAY_TEST

/* 速度制御 */
//#define SPEED_TEST
#define SPEED_TEST_RPM (100)

/* 速度位置制御 */
//#define POS_TMP_TEST 
#define POS_TMP_INT_MS 0.2
#define POS_TMP_SPD_RPM 10
#define POS_TMP_RPM 2 

/* トルク制御 */
//#define TORQUE_TEST
#define TORQUE_TEST_VAL 0

/* 位相調整 */
//#define VARIABLE_phase_diff 
#define VARIABLE_PHASEDIFF_DEFAULT 800
#define PHASEDIFF_ADJ_RPM (260)
#define VARIABLE_PHASEDIFF_INC 10

/* 位相調整スイッチ */
// 真ん中ボタンで -10 右ボタンで +10
//#define PHASEDIFF_MANUAL

/* デモ */
//#define DEMO_EXECUTE

/* UART 通信 */
// #define UART_CONTROL

/* 外部FLASH S25FL テスト */
//#define S25FL_TEST

/* LED TEST */
//#define LED_TEST

/* モーター制御系　移行 */
//#define MOTOR_CONTROL_TEMP



#endif // TEST_MODE_DEFINITION_H
