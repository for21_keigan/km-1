#ifndef TYPE_UTILITY_H
#define TYPE_UTILITY_H

#include <stdio.h>
#include <stdint.h>
#include "compiler_abstraction.h"
#include "nrf_log.h"

union float_byte_ui32 {
    float fl;
    uint8_t bt[sizeof(float)];
    uint32_t ui32;
};

union byte_uint16 {
	uint8_t bt[sizeof(uint16_t)];
	uint16_t ui16;
};



/**@brief Function for validation of float value.
 *
 * @param[in]   value            Value to be encoded.
 * @param[out]  p_encoded_data   Buffer where the encoded data is to be written.
 * @return      Number of bytes written.
 *     0x7F800000: 正の無限大
 *     0xFF800000: 負の無限大 
 *     0x7F800001: シグナル NaN
 *     0x7FC00000: クワイエット型 NaN
 */

/* float を 不正な値でないかチェックする TODO*/
static __INLINE bool float_validation(float f)
{
    if(f != 0x7F800000 | f != 0xFF800000 | f != 0x7F800001 | f != 0x7FC00000	){return true;}
    else { return false;}    
}



/* float を バイト列に変換する ※ビッグエンディアンにしている。順配列になる */
static __INLINE uint8_t float_big_encode(float f, uint8_t * p_encoded_data)
{
    union float_byte_ui32 u;
    u.fl = f;
    uint32_t value = u.ui32;
    //NRF_LOG_DEBUG("uint32_val: %d\n", value);
    p_encoded_data[0] = (uint8_t) ((value & 0xFF000000) >> 24);
    p_encoded_data[1] = (uint8_t) ((value & 0x00FF0000) >> 16);
    p_encoded_data[2] = (uint8_t) ((value & 0x0000FF00) >> 8);
    p_encoded_data[3] = (uint8_t) ((value & 0x000000FF) >> 0);
    return sizeof(uint32_t);
}


/**@brief Function for decoding float value in big-endian format.
 *
 * @param[in]   p_encoded_data   Buffer where the encoded data is stored.
 *
 * @return      Decoded value.
 */
/* バイト列 を float に変換する ※バイト列がリトルエンディアンで格納されている場合、逆にする*/
static __INLINE float float_big_decode(const uint8_t * p_encoded_data)
{
    union float_byte_ui32 u;
    // uint32_t に変換し、共用体を使って float 化する
    u.ui32 = ( (((uint32_t)((uint8_t *)p_encoded_data)[0]) << 24) |
             (((uint32_t)((uint8_t *)p_encoded_data)[1]) << 16) |
             (((uint32_t)((uint8_t *)p_encoded_data)[2]) << 8)  |
             (((uint32_t)((uint8_t *)p_encoded_data)[3]) << 0) );
    return u.fl;
}




/* 値のバイト列（コマンド（ヘッダー）を含まない）から float を生成しログ出力 */
static __INLINE bool float_array_decode(uint8_t * p_encoded_data, uint8_t vlen, float *f_array){
    
	// 4の倍数 = 2進数での下位2ビットが0
    if (!(vlen & 3)){
        printf("size of array is not multiples of 4\n");
        return 0
	;} else {    
		printf("size of array is multiples of 4: OK\n");
        uint8_t k = vlen/4; // TODO コマンドに応じてべた書きのが良いか
			for (uint8_t i = 0; i < k; i++){
				uint8_t bt[4]; 
				bt[0] = p_encoded_data[4*i];
				//NRF_LOG_DEBUG("bt[0]: %d\n", bt[0]);
				bt[1] = p_encoded_data[4*i+1];
				//NRF_LOG_DEBUG("bt[1]: %d\n", bt[1]);
				bt[2] = p_encoded_data[4*i+2];
				//NRF_LOG_DEBUG("bt[2]: %d\n", bt[2]);
				bt[3] = p_encoded_data[4*i+3];
				//NRF_LOG_DEBUG("bt[3]: %d\n", bt[3]);
				float f  = float_big_decode(bt);
				//NRF_LOG_DEBUG("(%d) ", i+1);
				char str[200];
				sprintf(str,"Float :%f\n", f);
				printf("%s,\n", str);
				f_array[i] = f;
			}        
        // NRF_LOG_DEBUG("num:%d", num);
			return 1;
        }
}


/* データ（バイト列）の指定の場所から float を1つ生成する */
static __INLINE bool get_float_from_args(uint8_t *data, uint8_t len, uint8_t num, float *f)
{

    if (len - num < sizeof(float)){
		NRF_LOG_DEBUG("args is less than float size\r\n");
        return false;
	}
	
	uint8_t arg[4] = {0};
	arg[0] = data[num];
	//NRF_LOG_DEBUG("bt[0]: %d\n", bt[0]);
	arg[1] = data[num + 1];
	//NRF_LOG_DEBUG("bt[1]: %d\n", bt[1]);
	arg[2] = data[num + 2];
	//NRF_LOG_DEBUG("bt[2]: %d\n", bt[2]);
	arg[3] = data[num + 3];
	//NRF_LOG_DEBUG("bt[3]: %d\n", bt[3]);
	
	*f = float_big_decode(arg);
	
	if(float_validation(*f)){
		return true;
	} else {
		NRF_LOG_DEBUG("content of float is invalid...\r\n");
		return false;
	}

}

/* データ（バイト列）の指定の場所から uint32_t を1つ生成する */
static __INLINE uint32_t get_uint32t_from_args(uint8_t *data, uint8_t len, uint8_t num)
{

    if (len - num < sizeof(uint32_t)){
		NRF_LOG_DEBUG("args is less than uint32_t size\r\n");
	}
	
	uint8_t arg[4] = {0};
	arg[0] = data[num];
	//NRF_LOG_DEBUG("bt[0]: %d\n", bt[0]);
	arg[1] = data[num + 1];
	//NRF_LOG_DEBUG("bt[1]: %d\n", bt[1]);
	arg[2] = data[num + 2];
	//NRF_LOG_DEBUG("bt[2]: %d\n", bt[2]);
	arg[3] = data[num + 3];
	//NRF_LOG_DEBUG("bt[3]: %d\n", bt[3]);
	
	return uint32_big_decode(arg);

}

/* データ（バイト列）の指定の場所から uint16_t を1つ生成する */
static __INLINE uint16_t get_uint16t_from_args(uint8_t *data, uint8_t len, uint8_t num)
{

    if (len - num < sizeof(uint16_t)){
		NRF_LOG_DEBUG("args is less than uint16_t size\r\n");
	}
	
	uint8_t arg[2] = {0};
	
	arg[0] = data[num];
	arg[1] = data[num + 1];
	
    return uint16_big_decode(arg);

}




/* リトルエンディアンのバイト列は基本用いないため削除
// float を バイト列に変換する ※順序通りだがArmなのでリトルエンディアン→逆配列になる 
static __INLINE uint8_t float_little_encode(float f, uint8_t * p_encoded_data)
{
    union float_byte_ui32 u;
    u.fl = f;
    uint32_t value = u.ui32;
    //NRF_LOG_DEBUG("uint32_val: %d\n", value);
    p_encoded_data[0] = (uint8_t) ((value & 0x000000FF) >> 0);
    p_encoded_data[1] = (uint8_t) ((value & 0x0000FF00) >> 8);
    p_encoded_data[2] = (uint8_t) ((value & 0x00FF0000) >> 16);
    p_encoded_data[3] = (uint8_t) ((value & 0xFF000000) >> 24);
    return sizeof(uint32_t);
}

// バイト列 を float に変換する ※バイト列が逆：リトルエンディアンである場合 
static __INLINE float float_little_decode(const uint8_t * p_encoded_data)
{
    union float_byte_ui32 u;
    // uint32_t に変換し、共用体を使って float 化する
    u.ui32 = ( (((uint32_t)((uint8_t *)p_encoded_data)[0]) << 0) |
             (((uint32_t)((uint8_t *)p_encoded_data)[1]) << 8) |
             (((uint32_t)((uint8_t *)p_encoded_data)[2]) << 16)  |
             (((uint32_t)((uint8_t *)p_encoded_data)[3]) << 24) );
    return u.fl;
}
*/



#endif // TYPE_UTILITY_H

