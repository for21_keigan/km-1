#ifndef MOTOR_MEASUREMENT_H
#define MOTOR_MEASUREMENT_H

/**@brief モーターの測定値取得と更新
 */
void motor_measurement_init(void);
void motor_measurement_timer_start(void);
void motor_measurement_timer_stop(void);

#endif // MOTOR_MEASUREMENT_H