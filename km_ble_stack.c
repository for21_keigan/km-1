#include <string.h>

#include <ble.h>
#include <app_error.h>
#include <sdk_errors.h>
#include <softdevice_handler_appsh.h>

#include "km_ble_stack.h"

// Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device
#define IS_SRVC_CHANGED_CHARACT_PRESENT  1

// セントラル及びペリフェラルとしての、接続数の定義
#define CENTRAL_LINK_COUNT    0
#define PERIPHERAL_LINK_COUNT 1

// GATT MTUサイズの定義, GATT_MTU_SIZE_DEFAULT は ble_gatt.h で23に定義されている。
#define NRF_BLE_MAX_MTU_SIZE GATT_MTU_SIZE_DEFAULT

void ble_stack_init(sys_evt_handler_t sys_evt_dispatch, ble_evt_handler_t ble_evt_dispatch)
{
    ret_code_t err_code;
    
    // Initialize the SoftDevice handler module.
    // LFCLK crystal oscillator. 32kHz xtal外付け。
     nrf_clock_lf_cfg_t clock_lf_cfg = {
     .source        = NRF_CLOCK_LF_SRC_XTAL,
     .rc_ctiv       = 0,
     .rc_temp_ctiv  = 0,
     .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM};
    // 太陽誘電 EYSHJNZXZ ver1.1 データシートの推奨設定。-> nrf51時代
    /*
    nrf_clock_lf_cfg_t clock_lf_cfg = {
        .source        = NRF_CLOCK_LF_SRC_RC,
        .rc_ctiv       = 16,
        .rc_temp_ctiv  = 2,
        .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_250_PPM};
*/

#ifdef APP_SCHEDULER_ENABLED
	SOFTDEVICE_HANDLER_APPSH_INIT(&clock_lf_cfg, true);      // BLEのスタックの処理にスケジューラを使う場合
#else 
    SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL); // スケジューラを使わない場合
#endif
    
	// Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = softdevice_app_ram_start_get(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Overwrite some of the default configurations for the BLE stack.
    ble_cfg_t ble_cfg;

    memset(&ble_cfg, 0, sizeof(ble_cfg));
    ble_cfg.common_cfg.vs_uuid_cfg.vs_uuid_count = 1;
    err_code = sd_ble_cfg_set(BLE_COMMON_CFG_VS_UUID, &ble_cfg, ram_start);
    APP_ERROR_CHECK(err_code);

    // Configure the maximum number of connections.
    memset(&ble_cfg, 0, sizeof(ble_cfg));
    ble_cfg.gap_cfg.role_count_cfg.periph_role_count  = BLE_GAP_ROLE_COUNT_PERIPH_DEFAULT;
    ble_cfg.gap_cfg.role_count_cfg.central_role_count = 0;
    ble_cfg.gap_cfg.role_count_cfg.central_sec_count  = 0;
    err_code = sd_ble_cfg_set(BLE_GAP_CFG_ROLE_COUNT, &ble_cfg, ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = softdevice_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);

	// メモ：GATT table のサイズを変更する場合は、ble_enable_params を使う
}

