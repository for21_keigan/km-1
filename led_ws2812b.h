
#ifndef LED_WS2812B_H
#define LED_WS2812B_H

#include "app_error.h"
#include "app_util_platform.h"

/*
 * LED WS2812B 制御仕様 
 * period = 1.25us としたとき、
 *
 * 0code -> 0.4us:HIGH 0.85us:LOW
 * 1code -> 0.8us:HIGH 0.45us:LOW
 * 
 * とすれば制御できる 
 *
*/

// 

#define RGB_BUFFER_SIZE 24
#define LED_PWM_TOP 20 // bit PWM priod Counter Top
#define B_H 14 // bit high width
#define B_L 7 // bit low width
#define B_Z 0 // bit low width

// LED 色
typedef struct
{
    uint8_t   green; // Brightness of green (0 to 255)
    uint8_t   red;   // Brightness of red   (0 to 255)
    uint8_t   blue;  // Brightness of blue  (0 to 255)
} rgb_led_t;

void led_ws2812b_init(void);
void led_ws2812b_led_uninit(void);
void led_ws2812b_drive_set_color(uint8_t red, uint8_t green, uint8_t blue);
void led_ws2812b_drive_set_color_default(void);
void led_ws2812b_drive_clear(void);

#endif // KM_AS5048A_DRIVER_H

