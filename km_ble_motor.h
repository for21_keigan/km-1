/* Copyright (c) 2015 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @defgroup ble_sdk_srv_km KM Motor Service Server
 * @{
 * @ingroup ble_sdk_srv
 *
 * @brief KM Motor Service Server module.
 *
 * @details This module implements a custom KM Motor Service with an LED and Button Characteristics.
 *          During initialization, the module adds the KM Motor Service and Characteristics
 *          to the BLE stack database.
 *
 *          The application must supply an event handler for receiving KM Motor Service
 *          events. Using this handler, the service notifies the application when the
 *          LED value changes.
 *
 *          The service also provides a function for letting the application notify
 *          the state of the Button Characteristic to connected peers.
 *
 * @note The application must propagate BLE stack events to the KM Motor Service
 *       module by calling ble_km_on_ble_evt() from the @ref softdevice_handler callback.
*/

#ifndef KM_BLE_MOTOR_H
#define KM_BLE_MOTOR_H

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "ble_parameters_config.h"

// 量産対応に伴うUUIDの変更
#define KM_UUID_MOTOR_BASE {0x8C, 0xAA, 0x5B, 0x79, 0xCD, 0xDF, 0xED, 0xA0, \
                              0x35, 0x4D, 0x36, 0x89, 0x35, 0xEA, 0x40, 0xF1}    
#define KM_UUID_MOTOR_SERVICE     0xEA35
#define KM_UUID_MOTOR_CONTROL_CHAR    0x0001
#define KM_UUID_MOTOR_LOG_CHAR    0x0002
#define KM_UUID_MOTOR_LED_CHAR    0x0003
#define KM_UUID_MOTOR_MEASUREMENT_CHAR    0x0004
#define KM_UUID_MOTOR_IMU_MEASUREMENT_CHAR    0x0005
#define KM_UUID_MOTOR_LEGACY_CONTROL_CHAR    0x0051
#define KM_UUID_MOTOR_LEGACY_CONTROL_MODE_CHAR    0x0052
				
// buttonless_dfu_service 内のキャラクタリスティクス用
#define KM_UUID_MOTOR_DFU_CHAR    0x0030						  

#define KM_MOTOR_CONTROL_CHAR_MAX_VLEN 20
#define KM_MOTOR_LOG_CHAR_MAX_VLEN 20
#define KM_MOTOR_LED_CHAR_MAX_VLEN 4
#define KM_MOTOR_MEASUREMENT_CHAR_MAX_VLEN 20
#define KM_MOTOR_IMU_MEASUREMENT_CHAR_MAX_VLEN 20
#define KM_MOTOR_LEGACY_CONTROL_CHAR_MAX_VLEN 20
#define KM_MOTOR_LEGACY_CONTROL_MODE_CHAR_MAX_VLEN 1


/* 旧バージョン 2017/7/24より前
#define KM_UUID_MOTOR_BASE        {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0x11, 0x11, \
                              0x11, 0x11, 0x11, 0x11, 0x00, 0x00, 0x00, 0x00} // <--  00000000-1111-1111-1111-AAAAAAAAAAAA 逆順なので注意 

#define KM_UUID_MOTOR_SERVICE     0x0000
#define KM_UUID_MOTOR_CONTROL_CHAR    0x0001
#define KM_UUID_MOTOR_CONTROL_MODE_CHAR    0x0002
#define KM_UUID_MOTOR_MEASUREMENT_CHAR    0x0003
#define KM_UUID_MOTOR_LED_CHAR    0x0004
#define KM_UUID_MOTOR_SETTING_CHAR    0x0005
#define KM_UUID_MOTOR_RECORD_CHAR    0x0006
#define KM_UUID_MOTOR_CONTROL_MC_CHAR    0x0011  
*/


/**@brief Function
for initializing KM Motor Service.
 *
 * @param[out] p_km      KM Motor Service structure. This structure must be supplied by
 *                        the application. It is initialized by this function and will later
 *                        be used to identify this particular service instance.
 * @param[in] p_km_init  Information needed to initialize the service.
 *
 * @retval NRF_SUCCESS If the service was initialized successfully. Otherwise, an error code is returned.
 */
uint32_t ble_motor_init(void);

/**@brief Function for handling the application's BLE stack events.
 *
 * @details This function handles all events from the BLE stack that are of interest to the KM Motor Service.
 *
 * @param[in] p_km      KM Motor Service structure.
 * @param[in] p_ble_evt  Event received from the BLE stack.
 */
void ble_motor_on_ble_evt(ble_evt_t * p_ble_evt);

/**@brief Function for notifying KM Motor Measurement Char. 
 *
*/
void ble_motor_measurement_update(float position, float speed, float torque);

 
#define KM_FLAG_MASK_MEASURE_POSITION  (0x01 << 1)    /**< Position Measurement bit. */
#define KM_FLAG_MASK_MEASURE_SPEED  (0x01 << 2)   /**< Speed Measurement bit. */
#define KM_FLAG_MASK_MEASURE_TORQUE  (0x01 << 3)   /**< Torque Measurement bit. */
                              


#endif // KM_BLE_MOTOR_H


