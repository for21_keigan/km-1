#include "fast_math.h"
#include "math.h"

unsigned short  sin60[DEG60+1];  // 0～60度, 振幅65535のsinテーブル(最大誤差0.003%) from 0 to 60 deg. (max precision error is 0.003%)

float _sinf(float x){
	unsigned short th;
	th = (unsigned short)(x*DEG60/(PI/3));
	return (float)_sin(th)/65535;
}


float _cosf(float x){
	unsigned short th;
	th = (unsigned short)(x*DEG60/(PI/3));
	return (float)_cos(th)/65535;
}
long _sin(unsigned short th){    // return( 65535*sin(th) ), th=rad*DEG60/(PI/3)=rad*(512*3)/PI (0<=rad<2*PI)
// 入力 : th = rad*DEG60/(PI/3)=rad*(512*3)/PI, (0<=rad<2*PI)
// 出力 : 65535*sin(th)
//    init_fast_math();
//    if( th>2.*PI ){ th -= 2*PI*(float)((int)(th/(2.*PI)));}
//    th_int = (unsigned short)(th/(PI/3.)*(float)DEG60+0.5);  // rad to deg
//    sin = (float)_sin(th)/65535.;
    unsigned short f_minus;
    long    x;

    // sinがマイナスのとき、thから180度引いて、f_minus=1にする
    if ( th>=DEG60*3){   f_minus = 1;    th -= DEG60*3;} // if th>=180deg, th = th - 180deg;
    else {               f_minus = 0;}                   // else         , f_minus = on. 

    if( th<DEG60 ){         // th<60度のとき
        x = sin60[th];                              // sin(th)
    } else if ( th<DEG60*2 ){ // 60≦th<120度のとき
        x = sin60[DEG60*2-th] + sin60[th-DEG60];    // sin(th)=sin(th+60)+sin(th-60)=sin(180-(th+60))+sin(th-60) because sin(th+60)=s/2+c*root(3)/2, sin(th-60)=s/2-c*root(3)/2.
    } else {                  // 120≦th<180度のとき
        x = sin60[DEG60*3-th];                      // sin(60-(th-120))=sin(180-th)
    }
    if ( f_minus==1 ){   x = -x;}  // sinがマイナスのときマイナスにする
    return(x);
}

long _cos(unsigned short th){    // return( 65535*sin(th) ), th=rad*DEG60/(PI/3)=rad*(512*3)/PI (0<=rad<2*PI)
// 入力 : th = rad*DEG60/(PI/3)=rad*(512*3)/PI, (0<=rad<2*PI)
// 出力 : 65535*cos(th)
    th += DEG60*3/2;
    if( th>=DEG60*6 ){  th -= DEG60*6;}
    return( _sin(th) );
}

void init_fast_math(){  // sin0-sin60deg; 0deg=0, 60deg=512
    int i;
    
    for( i=0;i<=DEG60;i++ ){    // 0～60度までのsinテーブルをつくるset sin table from 0 to 60 deg..
//        sin60[i] = (unsigned short)(sin((float)i/512.*PI/3.));
        sin60[i] = (unsigned short)(65535.*sinf((float)i/(float)DEG60*PI/3.));
    }
}

//#if 0
//float  norm(float x[0], float x[1]){  // ２ノルムを計算
//    return(sqrt(x[0]*x[0]+x[1]*x[1]));
//}
