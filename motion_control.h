 /*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef MOTION_CONTROL_H
#define MOTION_CONTROL_H

#include "app_util_platform.h"

/** Counts number of elements inside the array
 */
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

void motion_timer_init(void);
void motion_set_position(float position, float speed);
void motion_set_velocity(float velocity);
void motion_set_stop(void); // 減速してゼロにしてから motor_stop();

#endif