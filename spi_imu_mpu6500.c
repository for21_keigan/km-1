#include "spi_imu_mpu6500.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_spi.h"
#include "nrf_log.h"
#include "peripherals_definition.h"

/* MPU6500 */
/*
    Note:
    When using SPI interface, user should use PWR_MGMT_1 (register 107) as well as SIGNAL_PATH_RESET (register 104) to ensure the reset is performed properly. The sequence used should be:
    1. Set H_RESET = 1 (register PWR_MGMT_1)
    2. Wait 100ms
    3. Set GYRO_RST = ACCEL_RST = TEMP_RST = 1 (register SIGNAL_PATH_RESET)
    4. Wait 100ms
*/


#ifdef KM1_BETA

#define MPU6500_BUF_SIZE 2

#define ADDRESS_PWR_MGMT_1 (0x6BU) // device_reset 
#define ADDRESS_WHO_AM_I  (0x75U) // !< WHO_AM_I register identifies the device. Expected value is 0x70.
#define ADDRESS_SIGNAL_PATH_RESET (0x68U) // !<
#define ADDRESS_I2C_IF_DIS (0x6A) // USER_CONTROL
#define ADDRESS_GYRO_ZOUT_H  (0x47U) 
#define ADDRESS_GYRO_ZOUT_L  (0x48U) 


static const uint8_t expected_who_am_i = 0x70U; // !< Expected value to get from WHO_AM_I register.

static uint8_t	 mpu6500_write_tx_buf[MPU6500_BUF_SIZE] = {0xff, 0xff};
static uint8_t  mpu6500_write_rx_buf[MPU6500_BUF_SIZE];    //< RX buffer

static uint8_t	 mpu6500_read_tx_buf[MPU6500_BUF_SIZE] = {0,0};
static uint8_t	 mpu6500_read_rx_buf[MPU6500_BUF_SIZE];
static uint8_t mpu6500_read_tx_length = sizeof(mpu6500_read_tx_buf);
static uint8_t	 mpu6500_read_rx_buf[MPU6500_BUF_SIZE];


uint8_t t_buf_1;
uint8_t t_buf_2;

static const uint8_t mpu6500_length = sizeof(mpu6500_write_tx_buf);        /**< Transfer length. */

nrf_drv_spi_config_t spi2_config = {
	.sck_pin  = SPI2_SCK_PIN,
	.mosi_pin = SPI2_MOSI_PIN,
	.miso_pin = SPI2_MISO_PIN,
	.ss_pin   =  SPI2_SS_PIN_MPU6500, //NRF_DRV_SPI_PIN_NOT_USED,
	.irq_priority = APP_IRQ_PRIORITY_LOW,
	.orc          = 0xCC,
	.frequency    = NRF_DRV_SPI_FREQ_1M,
	.mode         = SPI_MODE_MPU6500,
	.bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
};


bool spi2_xfer_done = false;

#define RX_WHO_AM_I_LENGTH 2
uint8_t rx_who_am_i[RX_WHO_AM_I_LENGTH] = {0,0};
uint8_t rx_gyro_z_h[2] = {0,0};
uint8_t rx_gyro_z_l[2] = {0,0};

static const nrf_drv_spi_t spi2 = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE_FLASH_S25FL_IMU_MPU6500); // SPI instance. 


void spi2_event_handler(nrf_drv_spi_evt_t const * event, void * p_context){

     //NRF_LOG_DEBUG("GYRO_ZOUT: %d\n", (t_buf_1 << 8) | t_buf_2);
     //NRF_LOG_DEBUG("read: %d\n", spi_who_am_i);
}

static void imu_spi2_init(void)
{
	nrf_drv_spi_uninit(&spi2);
	//  "event_handler" 引数が NULL の場合は、ブロッキングモードとなる。（transferが終わるまで他の操作をしない）
	APP_ERROR_CHECK(nrf_drv_spi_init(&spi2, &spi2_config, spi2_event_handler, NULL));   
	NRF_LOG_DEBUG("IMU MPU6500 was initialized successfully.\r\n");

}

void imu_mpu6500_write(uint8_t register_address, uint8_t value){
        
    memset(mpu6500_write_rx_buf, 0, mpu6500_length);
    
    mpu6500_write_tx_buf[0] = register_address;
    mpu6500_write_tx_buf[1] = value;
    
	spi2_xfer_done = false;
    
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2, mpu6500_write_tx_buf, mpu6500_length, mpu6500_write_rx_buf, mpu6500_length)); 
    //NRF_LOG_DEBUG("mpu6500_write_rx_buf %d, %d", );    
}



void imu_mpu6500_read(uint8_t register_address, uint8_t * destination, uint8_t read_length){
      
    bool transfer_succeeded;    
    
    mpu6500_read_tx_buf[0] = register_address | 0x80;  // register_address | 0x80 to denote read
    
	spi2_xfer_done = false;
    
    APP_ERROR_CHECK(nrf_drv_spi_transfer(&spi2,  mpu6500_read_tx_buf, mpu6500_read_tx_length, destination, read_length)); 
}

void imu_mpu6500_read_who_am_i(){
    
    imu_mpu6500_read(ADDRESS_WHO_AM_I,  rx_who_am_i, RX_WHO_AM_I_LENGTH);     

}

uint8_t imu_mpu6500_get_who_am_i(){
    
    return rx_who_am_i[1];

}

void imu_mpu6500_read_gyro_z_h(){
    
    imu_mpu6500_read(ADDRESS_GYRO_ZOUT_H,  rx_gyro_z_h, 2);     

}


uint8_t imu_mpu6500_get_gyro_z_h(){
    
    return rx_gyro_z_h[1];

}

void imu_mpu6500_read_gyro_z_l(){
    
    imu_mpu6500_read(ADDRESS_GYRO_ZOUT_L,  rx_gyro_z_l, 2);     

}


uint8_t imu_mpu6500_get_gyro_z_l(){
    
    return rx_gyro_z_l[1];

}


bool imu_mpu6500_verify_product_id(void)
{

    imu_mpu6500_read_who_am_i();
    nrf_delay_ms(100);
    uint8_t who_am_i = imu_mpu6500_get_who_am_i();

    if (who_am_i)
    {
        if (who_am_i != expected_who_am_i)
        {
            return false;
        }
        else
        {
            NRF_LOG_DEBUG("Product ID verified.");
            return true;
        }
    }
    else
    {
        return false;
    }
}


bool imu_mpu6500_init(){
    
	imu_spi2_init();
	
    bool transfer_succeeded = true;
    uint8_t reset = 0x80U; // Reset Device
    imu_mpu6500_write(ADDRESS_PWR_MGMT_1, reset);

    nrf_delay_ms(100);
    
    uint8_t reset_value= 0x04U | 0x02U | 0x01U; // Resets gyro, accelerometer and temperature sensor signal paths.
    imu_mpu6500_write(ADDRESS_SIGNAL_PATH_RESET, reset_value);

    nrf_delay_ms(100);
    
    uint8_t i2c_disable = 0x10U; // I2C Disable
    imu_mpu6500_write(ADDRESS_I2C_IF_DIS, i2c_disable);

    nrf_delay_ms(100);
    
    // Read and verify product ID
    transfer_succeeded &= imu_mpu6500_verify_product_id(); 
    nrf_delay_ms(100);
    
/*
    while(1){
        km_spi2_read_gyro_z_h();
        nrf_delay_ms(50);
        NRF_LOG_DEBUG("gyro_z_h: %d, ", km_spi2_get_gyro_z_h());
        km_spi2_read_gyro_z_l();
        nrf_delay_ms(50);
        NRF_LOG_DEBUG("gyro_z_l: %d\n", km_spi2_get_gyro_z_l());        
     
        uint16_t gyro_z = km_spi2_get_gyro_z_h() << 8 | km_spi2_get_gyro_z_l() ;
        NRF_LOG_DEBUG("gyro_z: %d\n", gyro_z);  
        int z;        
        if(gyro_z & (1 << 15)){
            z = - (~gyro_z + 1);
            NRF_LOG_DEBUG("minus\n");
        } else {
            z = gyro_z;
        }
        NRF_LOG_DEBUG("z: %d\n", z);  
    }
*/   
    
/*
   uint8_t c = 0;
    while(1){
            mpu6500_read_tx_buf[0] = 0x75U | 0x80U;
    uint8_t destination[2];
    nrf_drv_spi_transfer(&spi2,  mpu6500_read_tx_buf, 2, destination, 2);

        NRF_LOG_DEBUG("who_am_I?: %d, %d\n",destination[0], destination[1]);
                    //nrf_delay_ms(100);


            uint8_t d;
                    mpu6500_read_tx_buf[0] = 0x00U;
    nrf_drv_spi_transfer(&spi2,  mpu6500_read_tx_buf, 1, &d, 1);
         NRF_LOG_DEBUG("who_am_I!: %d\n",d);       
      
                    nrf_delay_ms(100);
        c++;
 
        if(c == 1000){
                        nrf_delay_ms(5000);
        }

        
    }

        */

/*
    nrf_delay_ms(100);    

    while(1){
        mpu6500_read_tx_buf[0] = 0x47U | 0x80U;
        uint8_t leftByte;
        nrf_drv_spi_transfer(&spi2,  mpu6500_read_tx_buf, 1, &t_buf_1, 1);
         nrf_delay_ms(50);
        mpu6500_read_tx_buf[0] = 0x48U | 0x80U;
        uint8_t rightByte;
        nrf_drv_spi_transfer(&spi2,  mpu6500_read_tx_buf, 1, &t_buf_1, 1);
        nrf_delay_ms(50);
    }
    */


    return transfer_succeeded;
    

}

bool imu_mpu6500_get_sample(){
    
        imu_mpu6500_read_gyro_z_h();
        //nrf_delay_ms(50);
        NRF_LOG_DEBUG("gyro_z_h: %d, ", imu_mpu6500_get_gyro_z_h());
        //km_spi2_read_gyro_z_l();
        //nrf_delay_ms(50);
        //NRF_LOG_DEBUG("gyro_z_l: %d\n", km_spi2_get_gyro_z_l());        
     
    /*
        uint16_t gyro_z = km_spi2_get_gyro_z_h() << 8 | km_spi2_get_gyro_z_l() ;
        NRF_LOG_DEBUG("gyro_z: %d\n", gyro_z);  
        int z;        
        if(gyro_z & (1 << 15)){
            z = - (~gyro_z + 1);
            NRF_LOG_DEBUG("minus\n");
        } else {
            z = gyro_z;
        }
        NRF_LOG_DEBUG("z: %d\n", z);  
    */
}

#endif


