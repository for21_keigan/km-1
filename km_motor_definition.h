/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef KM_MOTOR_DEFINITION_H
#define KM_MOTOR_DEFINITION_H

/* 極数 / phase_diff_DEFAULT
03-01: 14p / 1300 --> 03-16 に変更
03-02: 14p / 500
03-03: 14p / 1300 --> 1600
03-04: 14p / 2050 --> 1200 --> 1400(R8)
03-05: 14p / 1750 --> 1450 --> 1450(R9)
03-06: 14p / 600

03-07: 16p / 500
03-08: 16p / 250
03-09: 16p / 450
03-10: 16p / 1800

03-11: 16p / 1750 ※INピン配 13,12,11
03-12: 16p / 1400 ※INピン配 13,12,11
03-13: 16p / 1600 ※INピン配 13,12,11
03-14: 16p / 1850 ※INピン配 13,12,11
03-15: 16p / 0 ※INピン配 13,12,11

03-16: 14p / 1700
03-17: 14p / 1600

03-18: 16p / 400

03-20: 16p / 300 （仮）
03-21: 16p / 1900 0以下で最適ありそう
03-22: 16p / 500

03-23(R9):14p / 1450
03-24(R8):14p / 1400

03-25:14p / 1470
03-26:14p / 1500
03-27:14p / 1400
03-28:14p / 1400
03-29:14p / 1750

03-31:14p / 1800 量14
03-32:14p / 1800 量13 

03-34:14p / 1700
03-33:14p / 1700


金02:14p / 1390
金04:14p / 960
金05:14p / 1220
金06:14p / 1400 --> ipresence
金07:14p / 1310 --> ipresence
金08:14p / 1900
金09:14p / 1310
金10:14p  /  2100 ※ ピンアサインが、IN1/2/3 = 11/13/12 -> 修正済
金11:14p  /  1620

Keigan 長浜量試 8/5 
1:1120
2:1660
3:1300
4:1080

*/

/* DEBUG LOG */
//#define ROTATION_DEBUG
//#define SAADC_DEBUG // Set to log SAADC buffer
//#define CONTROL_CURRENT_DEBUG // Set to log Iq
//#define VELOCITY_DEBUG
//#define OUTPUT_PWM_DEBUG
//#define DCC

#define MOTOR_TYPE_12N_14P // 12スロット14極
//#define MOTOR_TYPE_15N_16P // 15スロット16極

#ifdef MOTOR_TYPE_12N_14P
#define POLE_NUM 14 // 極数
#elif MOTOR_TYPE_15N_16P
#define POLE_NUM 16 // 極数
#endif


/* 位相調整 */
#define phase_diff_DEFAULT 1120 // 1040
/* MOTOR ROTARY ENCODER RESOLUTION FOR 360 degrees  */
#define P_max 16384 // AS5048A の分解能

/* SAADC AD Converter */
#define SAADC_MAX_VAL 16384
#define SAADC_INPUT_RANGE 3.6f

/* INPUT VOLTAGE */
#define INPUT_AMP 16
#define R_SHUNT (0.1)

#define MEAS_INT_US 200000

/* LED Color */
#define LED_DEF_R 30
#define LED_DEF_G 30
#define LED_DEF_B 30


#endif // KM_MOTOR_DEFINITION_H
