/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#include "test_flash_s25fl.h"
#include "spi_flash_s25fl.h"

#include "nrf_log.h"


void test_flash_s25fl(uint8_t *read_buf){
    
	NRF_LOG_DEBUG("test_flash_s25fl\r\n");
    flash_s25fl_erase_4k(MOTION_START_ADDRESS(0));
	NRF_LOG_DEBUG("after erase\r\n");
	
	static uint8_t buf[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xBA, 0xAD, 0xF0, 0x0D, 0xDE, 0xAD, 0xBE, 0xEF, 0xBA, 0xAD, 0xF0, 0x0D};
	NRF_LOG_DEBUG("write\r\n");
	flash_s25fl_write(MOTION_START_ADDRESS(0), buf, ARRAY_SIZE(buf));
	NRF_LOG_DEBUG("read\r\n");
	//uint8_t rx[16];
	flash_s25fl_read(MOTION_START_ADDRESS(0), read_buf, 16);
	for (uint16_t i = 0; i < 16; i++ ) {
		NRF_LOG_DEBUG("%d,",read_buf[i]); 
	}
	//read_buf[0] = rx[4];
	//read_buf[1] = rx[5];	
}

