/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef PERIPHERALS_DEFINITION_H
#define PERIPHERALS_DEFINITION_H

#include "app_timer.h"

/* タイマーなど、nrf52832 のペリフェラルの重複使用を避けるための定義リスト */

/* PWM */
#define PWM_INSTANCE_DRIVER_L6230 0
#define PWM_INSTANCE_LED_WS2812B 1

// PWM_INSTANCE_DRIVER_L6230 のカウンタートップ
#define COUNTER_TOP 500
/* タイマー・ピリオド（周期）時間設定 */
/* PWM 
    up and down
    20kHz -> counterTop:800  100us
    25kHz -> counterTop:640  80us   
    32kHz -> counterTop:500  62.5us
    40kHz -> counterTop:400  50us
    50kHz -> counterTop:320  40us
*/


/* TIMER */
#define TIMER_INSTANCE_CURRENT 1 // 速度制御系タイマー
#define TIMER_INSTANCE_POSITION 2 // 回転位置取得SPIタイマー
#define TIMER_INSTANCE_CONTROL 3 // 電流取得タイマー

// 速度制御系タイマー nrf_drv_timer (timer2) 
#define CONTROL_INT_US 500 //300//220
#define CONTROL_INT_MS 0.5 //
#define CONTROL_INT_SEC 0.0005 //

// 回転位置取得SPIタイマー
#define GET_POSITION_INT_US 100 //300
#define GET_POSITION_INT_MS 0.1
#define GET_POSITION_INT_SEC 0.0001 //

// 電流取得タイマー
#define GET_CURRENT_INT_US 125
#define GET_CURRENT_INT_MS 0.125
#define GET_CURRENT_INT_SEC 0.000125
#define GET_CURRENT_INT_TICKS (GET_CURRENT_INT_US * 16) // 16MHz * 62.5 us = 1000 ticks

/* SPI */
#define SPI_INSTANCE_ENCODER_AS5048A 0
#define SPI_INSTANCE_FLASH_S25FL_IMU_MPU6500 1 // TODO MPU6500 と共通でいける？

#define SPI_MODE_AS5048A NRF_DRV_SPI_MODE_1
#define SPI_MODE_S25FL NRF_DRV_SPI_MODE_0
#define SPI_MODE_MPU6500 NRF_DRV_SPI_MODE_0


/* TWIS (I2Cスレーブ) */
#define TWIS_INSTANCE_MOTOR 0 // TODO Master は？ 

/* TWIM (I2Cマスター) */
#define TWIM_INSTANCE_OTHER 0 // TODO 

/* ソフトウェアタイマー app_timer.h */
// ペリフェラルではないがここに記載する?? TODO
/* Notification Test TODO */
#define MOTOR_NOTIFICATION_INTERVAL APP_TIMER_TICKS(200)
/* APP_TIMER */
APP_TIMER_DEF(motor_meas_timer_id);



//#define DUTY_LIMIT 360






#endif // PERIPHERALS_DEFINITION_H

