/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef LEGACY_COMMAND_CONTROL_H
#define LEGACY_COMMAND_CONTROL_H

#include "app_util_platform.h"

/* KM_MOTOR_LEGACY_CONTROL Characteristics 
   byte[0] command list */

#define LEGACY_CMD_SetSpeed 0x10 // write_wo_resp, write // モーターの速度をセットする
#define LEGACY_CMD_SetPositionDt 0x20 // write_wo_resp, write // モーターの絶対位置をセットする （ACC DEC Duration別々）
#define LEGACY_CMD_SetPosition 0x21 // write_wo_resp, write // モーターの絶対位置をセットする （DEC と ACC で Duration と同じ）
#define LEGACY_CMD_SetRelPositionDt 0x23 // write_wo_resp, write // モーターの相対位置をセットする （ACC DEC Duration別々）
#define LEGACY_CMD_SetRelPosition 0x24 // write_wo_resp, write // モーターの相対位置をセットする （DEC と ACC で Duration と同じ）
#define LEGACY_CMD_PreSetPositon 0x27 // write_wo_resp, write // 現在の状態でモーターの絶対位置をセットする
#define LEGACY_CMD_SetTorque 0x70 // write_wo_resp, write // 現在のトルクを設定する
#define LEGACY_CMD_MaxTorque 0x71 // write_wo_resp, write // 最大トルク値を設定する
// #define LEGACY_CMD_SoftStop 0x80 // write_wo_resp, write  // モーターをソフトに減速し停止
#define LEGACY_CMD_HardStop 0x81 // write_wo_resp, write // モーターをハードに減速し停止
#define LEGACY_CMD_HardBrake 0x82 // write_wo_resp, write // モーターを最大トルクでハードに停止

//#define LEGACY_CMD_SetCurrentPID 0xA0 // write_wo_resp, write // モーターの速度制御PIDパラメタをセット
//#define LEGACY_CMD_GetCurrentPID 0xA1 // read // モーターの速度制御PIDパラメタを取得
#define LEGACY_CMD_SetSpeedPID 0xA2 // write_wo_resp, write // モーターの速度制御PIDパラメタをセット
//#define LEGACY_CMD_GetSpeedPID 0xA3 //read // モーターの速度制御PIDパラメタを取得
#define LEGACY_CMD_SetPositionP 0xA4 // write_wo_resp, write // モーターの速度制御PIDパラメタをセット
//#define LEGACY_CMD_GetPositionPID 0xA5 // read // モーターの速度制御PIDパラメタを取得

#define LEGACY_CMD_FactoryMode 0xFA // ファクトリーモードヘッダー data[0]
//以下は byte[1]
#define LEGACY_CMD_FactoryMode_On 0xCD // ファクトリーモードをオンにする
#define LEGACY_CMD_FactoryMode_WriteSerialNumber 0x00 // シリアル番号を書き込む
#define LEGACY_CMD_FactoryMode_WritePhaseDiff 0x01 // 位相調整値をセットし保存する
#define LEGACY_CMD_FactoryMode_ReadPhaseDiff 0x02 //　現在の位相調整値を読みだす


/* value length of argument list  */
#define LEGACY_VLEN_SET_SPD 5
#define LEGACY_VLEN_SET_POS_DT 17
#define LEGACY_VLEN_SET_POS 13
#define LEGACY_VLEN_SET_REL_POS_DT 17
#define LEGACY_VLEN_SET_REL_POS 13
#define LEGACY_VLEN_SET_PRE_SET_POS 5
#define LEGACY_VLEN_SET_TRQ 5
#define LEGACY_VLEN_MAX_TRQ 5
#define LEGACY_VLEN_SOFT_STOP 5
#define LEGACY_VLEN_HARD_STOP 1
#define LEGACY_VLEN_HARD_BRAKE 1
#define LEGACY_VLEN_TIME_SET 5
#define LEGACY_VLEN_SET_CUR_PID 13
#define LEGACY_VLEN_GET_CUR_PID 13
#define LEGACY_VLEN_SET_SPD_PID 13
#define LEGACY_VLEN_GET_SPD_PID 13
#define LEGACY_VLEN_SET_POS_PID 13
#define LEGACY_VLEN_GET_POS_PID 13
#define LEGACY_VLEN_SET_MEAS 2


#define LEGACY_VLEN_FA_MODE_ON 4
#define LEGACY_VLEN_FA_MODE_WRITE_SN 14
#define LEGACY_VLEN_FA_MODE_WRITE_PHASE_DIFF 4
#define LEGACY_VLEN_FA_MODE_READ_PHASE_DIFF 2


/* KM_MOTOR_LEGACY_CONTROL_MODE */
typedef enum
{
    KM_MOTOR_CONTROL_MODE_NONE,
    KM_MOTOR_CONTROL_MODE_SPEED,
    KM_MOTOR_CONTROL_MODE_POSITION,
    KM_MOTOR_CONTROL_MODE_TORQUE
    
} km_control_mode_t;


/* KM_MOTOR_LED */
typedef enum
{
    MOTOR_LED_STATE_OFF,       // モーターのLEDを停止する（点滅・点灯の停止）
    MOTOR_LED_STATE_ON_SOLID,  //  モーターのLEDカラーをセットする（点灯）
    MOTOR_LED_STATE_ON_FLASH   //  モーターのLEDカラーをセットする（点滅）
    
} km_led_state_t;

uint32_t ble_legacy_control_handler(uint8_t *data, uint8_t len);
uint32_t ble_legacy_control_mode_handler(uint8_t *data, uint8_t len);
uint32_t ble_legacy_led_handler(uint8_t *data, uint8_t len);

#endif // LEGACY_COMMAND_CONTROL_H
