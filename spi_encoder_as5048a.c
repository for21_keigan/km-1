#include <stdio.h>
#include "spi_encoder_as5048a.h"
#include "nrf_drv_spi.h"
#include "km_io_definition.h"
#include "peripherals_definition.h"
#include "nrf_log.h"

//#define AS5048A_DEBUG
//#define AS5048A_ERROR_DETECTION

/* AS5048A レジスタアドレス(2バイト長) */
#define AS5048A_CLEAR_ERROR_FLAG            0x0001 // read
#define AS5048A_PROGRAMMING_CONTROL         0x0003 // read/write
#define AS5048A_OTP_REGISTER_ZERO_POS_HIGH  0x0016 // read/write
#define AS5048A_OTP_REGISTER_ZERO_POS_LOW   0x0017 // read/write
#define AS5048A_DIAG_AGC                    0x3FFD // read
#define AS5048A_MAGNITUDE                   0x3FFE // read
#define AS5048A_ANGLE                       0x3FFF // read

#define SPI_MODE_AS5048A NRF_DRV_SPI_MODE_1

static const nrf_drv_spi_t as5048a_spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE_ENCODER_AS5048A); /**< SPI instance. */

#define AS5048A_BUFFER_SIZE 2
static uint8_t	tx_buf[AS5048A_BUFFER_SIZE] = {0xFF,0xFF}; // 
static uint8_t  rx_buf[AS5048A_BUFFER_SIZE];    /**< RX buffer. */
static const uint8_t m_length = sizeof(tx_buf);        /**< Transfer length. */

typedef enum {
	
	AS5048A_MODE_ANGLE, // 通常時、角度検出
	AS5048A_MODE_ERROR, // エラー時
	AS5048A_MODE_AGC, // Diagnostic and Automatic Gain Control
	AS5048A_MODE_OTHERS
	
} as5048a_mode_t;

typedef struct encoder_as5048a {
	
	uint8_t mode;
	uint16_t angle; // 回転角 0-(2^14-1)
	uint16_t errors; // 読みだしたデータ
	uint16_t agc;
	
} as5048a_t;


as5048a_t as5048a = {
	
    .mode = AS5048A_MODE_ANGLE,
	.angle = 0x00,
	.errors = 0x00,
	.agc = 0x00
};


/**
 * Utility function used to calculate even parity of uint16_t
 * XOR で計算早くする方法もあり
 */
static uint8_t calc_even_parity(uint16_t value){
	uint8_t cnt = 0;
	uint8_t  i;

	for (uint16_t i = 0; i < 16; i++)
	{
		if (value & 0x1)
		{
			cnt++;
		}
		value >>= 1;
	}
	return cnt & 0x1;
}


static void update_tx_buffer_read(uint8_t mode){
	
	uint16_t address;
	
	switch (mode){
		case AS5048A_MODE_ANGLE:
			address = AS5048A_ANGLE;
		    break;
		case AS5048A_MODE_ERROR:
			address = AS5048A_CLEAR_ERROR_FLAG;
		    break;
		case AS5048A_MODE_AGC:
			address = AS5048A_DIAG_AGC;
		    break;
		default:
			break;
	}
	
	uint16_t command = 0x4000; // パリティフラグ = 0, R/W flag = read
	command = command | address;

	//Add a parity bit on the the MSB
	command |= ((uint16_t)calc_even_parity(command)<<15);

	//Split the command into two uint8_ts
	tx_buf[0] = command & 0xFF;
	tx_buf[1] = ( command >> 8 ) & 0xFF;
	
	// NRF_LOG_DEBUG("read tx: %d, %d\r\n", tx_buf[0], tx_buf[1]);
	
}

// TODO
static void update_tx_buffer_write(uint8_t mode){

}

//　受け取った値から、パリティビットとエラービットを除去する
static uint16_t extract_tx_buffer(){
	return (( ( rx_buf[0] & 0xFF ) << 8 ) | ( rx_buf[1] & 0xFF )) & ~0xC000 ;
}

static void as5048a_mode_change(uint8_t mode){
	
	as5048a.mode = mode;
	update_tx_buffer_read(mode);
}

static nrf_drv_spi_evt_handler_t spi_as5048a_event_handler(nrf_drv_spi_evt_t const * event){

	if (as5048a.mode == AS5048A_MODE_ANGLE)
	{
		//NRF_LOG_DEBUG("l: %d, r: %d\r\n", rx_buf[0], rx_buf[1]);
		as5048a.angle = extract_tx_buffer();

        #ifdef AS5048A_ERROR_DETECTION	
			// エラービットがあった場合、診断する
			if (rx_buf[0] & 0x40) {
				NRF_LOG("Setting Error bit\r\n");
				as5048a_mode_change(AS5048A_MODE_ERROR);
			}
		#endif 
		
    } else if(as5048a.mode == AS5048A_MODE_ERROR){
		
		as5048a.errors = extract_tx_buffer();
		NRF_LOG_DEBUG("as5048a ERRORS: %d\r\n", as5048a.errors);
		as5048a_mode_change(AS5048A_MODE_ANGLE); // エラーフラグは全てクリアされる。通常の角度取得モードへ。 
		
	} else if(as5048a.mode == AS5048A_MODE_AGC){
		
		as5048a.agc = extract_tx_buffer();
		NRF_LOG_DEBUG("as5048a DIAG_AGC: %d\r\n", as5048a.agc);
		as5048a_mode_change(AS5048A_MODE_ANGLE); 
    }

}


void encoder_as5048a_init(){
	
	nrf_drv_spi_config_t as5048a_spi_config = {
		.sck_pin  = SPI_SCK_PIN,
		.mosi_pin = SPI_MOSI_PIN,
		.miso_pin = SPI_MISO_PIN,
		.ss_pin   = SPI_SS_PIN_AS5048A,
		.irq_priority = APP_IRQ_PRIORITY_LOW,
		.orc          = 0xCC,
		.frequency    = NRF_DRV_SPI_FREQ_8M,
		.mode         = SPI_MODE_AS5048A,
		.bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
	};

    APP_ERROR_CHECK(nrf_drv_spi_init(&as5048a_spi, &as5048a_spi_config, spi_as5048a_event_handler, NULL));
	update_tx_buffer_read(AS5048A_MODE_ANGLE);
}

void encoder_as5048a_angle_spi_xfer(){
	
	memset(rx_buf, 0, m_length);
	APP_ERROR_CHECK(nrf_drv_spi_transfer(&as5048a_spi, tx_buf, m_length, rx_buf, m_length));
}


/**
 * 角度を返す
 */
uint16_t encoder_as5048a_get_angle(){
	
	// NRF_LOG_DEBUG("ANGLE: %d\r\n", as5048a.angle);
	return as5048a.angle;
}

/**
 * AGC DIAG の値を返す
 */
uint16_t encoder_as5048a_get_agc(){
	return as5048a.agc;
}

/*
 * エラー値を返す
 */
uint16_t encoder_as5048a_get_errors(){
	return as5048a.errors;
}


