#ifndef teaching_playback_h
#define teaching_playback_h

#include "app_util_platform.h"

typedef enum {
	
	TEACHING_PLAYBACK_MODE_NONE,
	TEACHING_PLAYBACK_MODE_TEACHING,
	TEACHING_PLAYBACK_MODE_PLAYBACK
	
	
} teaching_playback_mode_t;


//暫定TODO
void teaching_playback_init(void);
void teaching_start(uint16_t motion);
void playback_start(uint16_t motion);

#endif // teaching_playback_h