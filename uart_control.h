/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef UART_CONTROL_H
#define UART_CONTROL_H

#include "app_util_platform.h"

/* UART */ 

#define UART_TX_BUF_SIZE 256                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 256                        /**< UART RX buffer size. */

void uart_control_init();

#endif // UART_CONTROL_H