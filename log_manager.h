 /*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef LOG_MANAGER_H
#define LOG_MANAGER_H

#include "app_util_platform.h"

#define LOG_OUTPUT_BIT_ECHO          0x01
#define LOG_OUTPUT_BIT_RESULT        0x02
#define LOG_OUTPUT_BIT_REGISTER      0x04
#define LOG_OUTPUT_BIT_MEASUREMENT   0x08


void set_log_output(uint8_t bit_flag);
void output_log(
);

#endif