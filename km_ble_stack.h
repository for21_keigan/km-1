#ifndef km_ble_stack_h
#define km_ble_stack_h

#include <ble.h>
#include <softdevice_handler.h>

void ble_stack_init(sys_evt_handler_t systemHandler, ble_evt_handler_t bleHandler);

#endif /* km_ble_stack_h */
