#include "motor_control.h"
#include "km_io_definition.h"
#include "km_motor_definition.h"
#include "spi_encoder_as5048a.h"
#include "fast_math.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_timer.h"
#include "app_timer.h"

#include "peripherals_definition.h"

#include "nrf_drv_pwm.h"
#include "nrf_drv_ppi.h" 
#include "nrf_drv_saadc.h"

#include "vector_control_math.h"
#include "type_utility.h"

#include "nrf_log.h"

#include "app_timer.h"

#include "km_ble_motor.h"

/* DEBUG */
//#define DEBUG_POSITION
//#define DEBUG_VELOCITY
//#define DEBUG_CURRENT
//#define DEBUG_SAADC_CURRENT
//#define DEBUG_VELOCITY_CONTROL
//#define DEBUG_OUTPUT_PWM
//#define DEBUG_CURRENT_CONTROL

#define BOUNDARY (P_max * 0.5)
#define ELECTRIC_CYCLE_TICKS (P_max / (POLE_NUM / 2)) // 電気角 360度分を エンコーダ AS5048A の単位(0~2^14-1)に 換算したもの

int phase_diff = phase_diff_DEFAULT; // TODO　位相調整値


/* Timer （ハードウェアタイマー） */
const nrf_drv_timer_t timer_current0 = NRF_DRV_TIMER_INSTANCE(TIMER_INSTANCE_CURRENT);
const nrf_drv_timer_t timer_position = NRF_DRV_TIMER_INSTANCE(TIMER_INSTANCE_POSITION);
const nrf_drv_timer_t timer_control0 = NRF_DRV_TIMER_INSTANCE(TIMER_INSTANCE_CONTROL);

/* PWM */
static nrf_drv_pwm_t m_pwm0 = NRF_DRV_PWM_INSTANCE(0);
static nrf_pwm_values_individual_t m_seq_values;
static nrf_pwm_sequence_t const m_seq = {
        .values.p_individual = &m_seq_values,
        .length = NRF_PWM_VALUES_LENGTH(m_seq_values),
        .repeats = 0,
        .end_delay = 0
};

/* SAADC */
#define SAMPLES_IN_BUFFER 1 //3
static uint8_t m_adc_channel_enabled;
static nrf_saadc_channel_config_t channel_0_config;
static nrf_saadc_channel_config_t channel_1_config;
static uint32_t m_adc_evt_counter;
static nrf_saadc_value_t m_buffer_pool[SAMPLES_IN_BUFFER];


typedef struct motor_params {
	
	motor_mode_t mode;
	
	float velocity; // 速度 [rad/s]
    float smoothed_velocity; // 大きくフィルタをかけた速度 [rad/s]
	int raw_position; // 位置 [ticks] 
	float position; // 位置 [radians] 
	float torque; // トルク [N/m]
	float current; // 電流 
	float theta; // 電気角
	
	float outward_position_offset; // preset_position に関係する
	float outward_position; // 同じく
	
	float target_position;
	float target_velocity;
	float target_torque;

    float max_speed;
    float min_speed;
    float max_torque;
    
    float preset_speed;

	bool is_measure_position_enabled;
	bool is_measure_velocity_enabled;
	bool is_measure_torque_enabled;

	
} motor_t;

// モーターの状態を表す構造体
motor_t motor = {
	
	.mode = MOTOR_CONTROL_MODE_NONE,	
	
	.position = 0,
	.velocity = 0,
	.torque = 0,
	.current = 0,
	.theta = 0,
	
	.outward_position_offset = 0,
	.outward_position = 0,
	
	.target_position = 0,
	.target_velocity = 0,
	.target_torque = 0,
    
    .max_speed = 0,
    .min_speed = 0,
    .max_torque = 0

};


/* new */
/* 電流制御 */
// PID係数
float Kc_p[2]={CURRENT_D_AXIS_P, CURRENT_Q_AXIS_P}; 
float Kc_i[2]={CURRENT_D_AXIS_I, CURRENT_Q_AXIS_I}; 
float Kc_d[2]={CURRENT_D_AXIS_D, CURRENT_Q_AXIS_D}; 
float lpf_c = LOW_PASS_FILTER_CURRENT; // ローパスフィルター

/* 速度制御 */
// PID係数
float Kv_p = VELOCITY_P;
float Kv_i = VELOCITY_I;
float Kv_d = VELOCITY_D;
float lpf_v = LOW_PASS_FILTER_VELOCITY; // ローパスフィルター
float lpf_sv = LOW_PASS_FILTER_SMOOTHED_VELOCITY; // ローパスフィルター

/* 位置制御 */	
float Kp_p = POSITION_P; 
float lpf_p = LOW_PASS_FILTER_POSITION; // ローパスフィルター

///ベクトル制御用変数
float TS_v = GET_CURRENT_INT_MS; //0.01;
float LPF_v = LOW_PASS_FILTER_CURRENT; //0.9; main.c で使用。廃止予定
float V_uvw[3];
float V_abo[3];
float V_dq[2];
float I_uvw[3];
float oldI_uvw[3] = {0.0, 0.0, 0.0};

float I_abo[3];
float I_dq[2];
float I_dq_lpf[2] = {0.0, 0.0};
//モーターへの入力電圧・電流
float refV_uvw[3];
float refV_abo[3];
float refV_dq[2];
float refI_uvw[3];
float refI_abo[3];
float refI_dq[2];

float Iq_max = 32.0; //8.0f;   //32.0f;// 4.0f;

float c_old[2] = {0.0, 0.0};
float cI[2] = {0.0, 0.0};
float Vd_max = INPUT_AMP;
float Vq_max = INPUT_AMP;
float V_max = INPUT_AMP;

float theta = 0; // 廃止予定
float sin_theta = 0;
float cos_theta = 0;

void motor_set_mode(motor_mode_t mode);

static void get_position(void);
static void get_position_moving_average(void);

static void velocity_control(void); 
static void position_control(void); 

static void motor_control_start(void);
static void motor_control_stop(void);


// L6230ドライバへの出力ピン
void motor_gpio_config(void){

    NRF_LOG_INFO("Config L6230 ENABLE PIN. \r\n");

    // ENABLE ピン（#3試作はプルダウン抵抗不要）
    nrf_gpio_cfg_output(L6230_EN_1_PIN);

#ifdef KM1_LEGACY
    nrf_gpio_cfg_output(L6230_EN_2_PIN);
    nrf_gpio_cfg_output(L6230_EN_3_PIN);
#endif

    // DIAG_EN ピンのみプルアップ抵抗を入れる
    nrf_gpio_cfg(L6230_DIAG_EN_PIN, NRF_GPIO_PIN_DIR_OUTPUT, NRF_GPIO_PIN_INPUT_DISCONNECT, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_S0S1, NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_pin_write(L6230_DIAG_EN_PIN, 1);

}


// モーターを動作可能にする
void motor_enable(){
    
    nrf_gpio_pin_write(L6230_EN_1_PIN, 1);
#ifdef KM1_LEGACY
    nrf_gpio_pin_write(L6230_EN_2_PIN, 1);
    nrf_gpio_pin_write(L6230_EN_3_PIN, 1);
#endif
	motor_control_start();
}

// モーターを動作不可にする
void motor_disable()
{
    motor_set_mode(MOTOR_CONTROL_MODE_NONE);
    
    nrf_gpio_pin_write(L6230_EN_1_PIN, 0);
#ifdef KM1_LEGACY
    nrf_gpio_pin_write(L6230_EN_2_PIN, 0);
    nrf_gpio_pin_write(L6230_EN_3_PIN, 0);
#endif
    motor_control_stop();
}


/* Setter and Getter */

motor_mode_t motor_get_mode(){
	return motor.mode;
}

float motor_get_velocity(){
    return motor.velocity;
}

float motor_get_smoothed_velocity(){
    return motor.smoothed_velocity;
}

int motor_get_raw_position(void)
{
	return motor.raw_position;
}

float motor_get_position(){
    return motor.position;
}

float motor_get_outward_position(){
    motor.outward_position = motor.position + motor.outward_position_offset;
    return motor.outward_position;
}

float motor_get_torque(){
    return motor.torque;
}

uint16_t motor_get_phase_diff(){
	
	return phase_diff;
}


void motor_set_phase_diff(uint16_t diff){
	
	phase_diff = diff;
}

void motor_set_mode(motor_mode_t mode){
	motor.mode = mode;
}


void motor_set_target_position(float position){
	motor.target_position = position;
}

void motor_preset_position(float position){
	motor.outward_position_offset = position - motor.position;
}

void motor_preset_speed(float speed){
    motor.preset_speed = speed;
}

void motor_set_target_velocity(float velocity){
    motor.target_velocity = velocity;
}


void motor_set_target_torque(float torque){
    motor.target_torque = torque;
}


void motor_set_max_speed(float speed){
    motor.max_speed = speed;
    // 保存 TODO
}

void motor_set_min_speed(float speed){
    motor.min_speed = speed;
    // 保存 TODO
}

void motor_set_max_torque(float torque){  
    motor.max_torque = torque;
    // 保存 TODO
}

// PIDコントローラの係数
void motor_set_speed_pid_p(float p){  
    Kv_p = p;
    // 保存 TODO
}

void motor_set_speed_pid_i(float i){  
    Kv_i = i;
    // 保存 TODO
}

void motor_set_speed_pid_d(float d){  
    Kv_d = d;
    // 保存 TODO
}

void motor_set_position_pid_p(float p){  
    Kp_p = p;
    // 保存 TODO
}


// 以下暫定、加減速実装 = motion planning 必要
void motor_stop(){
    motor_set_mode(MOTOR_CONTROL_MODE_VELOCITY);
    motor_set_target_velocity(0);
	//motor_control_start();
}

void motor_brake(){
    motor_set_mode(MOTOR_CONTROL_MODE_BRAKE); // TODO
}

void motor_free(){
    motor_set_mode(MOTOR_CONTROL_MODE_NONE);
	//motor_control_stop();
}

void motor_run_at_velocity(float velocity){
    motor_set_mode(MOTOR_CONTROL_MODE_VELOCITY);
    motor_set_target_velocity(velocity);
	//motor_control_start();
}

void motor_run_forward(){
    motor_set_mode(MOTOR_CONTROL_MODE_VELOCITY);
    motor_set_target_velocity(motor.preset_speed);
	//motor_control_start();
}

void motor_run_reverse(){
    motor_set_mode(MOTOR_CONTROL_MODE_VELOCITY);
    motor_set_target_velocity(- motor.preset_speed);
	//motor_control_start();
}

void motor_move_to(float position, float speed){
    motor_set_mode(MOTOR_CONTROL_MODE_POSITION);
    motor_preset_speed(speed);
    motor_set_target_position(position - motor.outward_position_offset);
	//motor_control_start();
}

void motor_move_to_pos(float position){
    motor_set_mode(MOTOR_CONTROL_MODE_POSITION);
    motor_set_target_position(position - motor.outward_position_offset);
	//motor_control_start();
}

void motor_move_by(float distance, float speed){
    motor_set_mode(MOTOR_CONTROL_MODE_POSITION);
    motor_preset_speed(speed);
    motor_set_target_position(motor.position + distance);
	//motor_control_start();
}

void motor_move_by_dist(float distance){
    motor_set_mode(MOTOR_CONTROL_MODE_POSITION);
    motor_set_target_position(motor.position + distance);
	//motor_control_start();
}

void motor_hold_torque(float torque){
    motor_set_mode(MOTOR_CONTROL_MODE_TORQUE);
    motor_set_target_torque(torque);
	//motor_control_start();
}




/**@brief Function for encoding Motor Measurement.

 * @param[out]  p_encoded_buffer   Buffer where the encoded data will be written. *
 * @return      Size of encoded data.
 */
static uint8_t motor_meas_encode(uint8_t * p_encoded_buffer)
{
    uint8_t len   = 0;
    int     i;

    if (motor.is_measure_position_enabled)
    {
        len   += float_big_encode(motor_get_outward_position(), &p_encoded_buffer[len]);
    }
    if (motor.is_measure_velocity_enabled)
    {
        len   += float_big_encode(motor.velocity, &p_encoded_buffer[len]);
    }
    if (motor.is_measure_torque_enabled)
    {
        len   += float_big_encode(motor.torque, &p_encoded_buffer[len]);
    }

    return len;
}

void motor_get_measurement(uint8_t * data, uint16_t * len){
	
	uint8_t   encoded_meas[20];
	
	*len = motor_meas_encode(data);
}


static void measurement_update(void)
{
    uint32_t err_code;
    float pos = motor_get_outward_position(); 
    float vel = motor.velocity;                
    float trq = motor.torque;                     

    ble_motor_measurement_update(pos, vel, trq);
}

static void motor_meas_timeout_handler(void *p_context)
{
    UNUSED_PARAMETER(p_context);
    measurement_update();
    //km_mpu6500_get_sample();
}



/**@brief Function for starting application timers.
 */

void motor_measurement_timer_start(void)
{

    uint32_t err_code;
    err_code = app_timer_start(motor_meas_timer_id, MOTOR_NOTIFICATION_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
}

void motor_measurement_timer_stop(void)
{
    uint32_t err_code;
    err_code = app_timer_stop(motor_meas_timer_id);
    APP_ERROR_CHECK(err_code);
}


/* 
 * モーターの回転位置を取得するタイマー
 *
 */
void motor_start_position_timer()
{
    nrf_drv_timer_enable(&timer_position);
}

void motor_stop_position_timer()
{
    nrf_drv_timer_disable(&timer_position);
}

static void motor_control_start(){
	
	NRF_LOG_DEBUG("motor_start\r\n");
    nrf_drv_pwm_simple_playback(&m_pwm0, &m_seq, 1, NRF_DRV_PWM_FLAG_LOOP);
    nrf_drv_timer_enable(&timer_current0);
    nrf_drv_timer_enable(&timer_control0);
}

static void motor_control_stop(){
	
	NRF_LOG_DEBUG("motor_stop\r\n");
    nrf_drv_pwm_stop(&m_pwm0, true);
    nrf_drv_timer_disable(&timer_current0);
    nrf_drv_timer_disable(&timer_control0);
}


static void calc_velocity(){

	static float old_position = 0;
	
    float diff = motor.position - old_position;
	old_position = motor.position;
		
	// ローパスフィルター有
	motor.velocity = (1.0f - lpf_v) * motor.velocity + lpf_v * diff / CALC_VEROCITY_INT_SEC;
    motor.smoothed_velocity =  (1.0f - lpf_sv) * motor.smoothed_velocity + lpf_sv * diff / CALC_VEROCITY_INT_SEC;
	
#ifdef DEBUG_VELOCITY
	
	NRF_LOG_DEBUG("velocity: %d\r\n", (int)(motor.velocity * 10000));

#endif
	
}


static void current_control()
{
	// 取得した電流を座標変換
	I_uvw2abo(I_uvw, I_abo);
    I_abo2dq(sin_theta, cos_theta, I_abo, I_dq);
	
    I_dq_lpf[0] = I_dq_lpf[0] * (1 - lpf_c) + lpf_c * I_dq[0];
    I_dq_lpf[1] = I_dq_lpf[1] * (1 - lpf_c) + lpf_c * I_dq[1];

	motor.torque = TORQUE_COEFF * I_dq_lpf[1]; // モータートルクの更新
	
    float c[2], cd[2];
    c[0] = refI_dq[0] - I_dq_lpf[0];
    c[1] = refI_dq[1] - I_dq_lpf[1];

    cI[0] = cI[0] + c[0] * TS_v;
    cI[1] = cI[1] + c[1] * TS_v;

    cd[0] = (c_old[0] - c[0]) / GET_CURRENT_INT_SEC;
    cd[1] = (c_old[1] - c[1]) / GET_CURRENT_INT_SEC;

    c_old[0] = c[0];
    c_old[1] = c[1];

    float ref_Vd = Kc_p[0] * c[0] + Kc_i[0] * cI[0] + Kc_d[0] * cd[0]; // 電流PID制御、V_d電圧出力
    ref_Vd = 0.0f;
	// TODO Vd に関する計算の削除
	
    float ref_Vq = Kc_p[1] * c[1] + Kc_i[1] * cI[1] + Kc_d[1] * cd[1]; // 電流PID制御、V_q電圧出力

    //SEGGER_RTT_printf(0, "%d,%d,%d\n",(int)(I_dq_lpf[0]*1000),(int)(I_dq_lpf[1]*1000),(int)(theta_pwm*1000));

    //SEGGER_RTT_printf(0, "%d,%d,%d,%d\n",(int)(I_dq[1]*1000),(int)(c[1]*1000),(int)(cI[1]*1000),(int)(cd[1]*1000));
    //SEGGER_RTT_printf(0, "%d,%d,%d,%d\n",(int)(I_uvw[0]*1000),(int)(I_uvw[1]*1000),(int)(I_uvw[2]*1000),(int)(cd[1]*1000));

    // アンチワインドアップ
    if (ref_Vd > Vd_max)
    {
        cI[0] = (Vd_max - Kc_p[0] * c[0] - Kc_d[0] * cd[0]) / Kc_i[0]; // 電圧振幅指令が最大値と等しくなる積分項を計算
        if (cI[0] < 0)
            cI[0] = 0; // 積分項が負になるとゼロにする
        ref_Vd = Vd_max;
    }
    else if (ref_Vd < -Vd_max)
    {
        // 本番 TODO 速度マイナス用
        // 電圧振幅指令がマイナス側に大きすぎるとき。計算マイナスの回転速度用であり、実際は不要
        cI[0] -= (ref_Vd + Vd_max) / Kc_i[0]; // 電圧振幅指令が最大値のマイナスと等しくなる積分項を計算
        if (cI[0] > 0)
            cI[0] = 0;    // 積分項が正になるとゼロにする
        ref_Vd = -Vd_max; // TODO
    }

    if (ref_Vq > Vq_max)
    {
        cI[1] = (Vq_max - Kc_p[1] * c[1] - Kc_d[1] * cd[1]) / Kc_i[1]; // 電圧振幅指令が最大値と等しくなる積分項を計算
        if (cI[1] < 0)
            cI[1] = 0; // 積分項が負になるとゼロにする
        ref_Vq = Vq_max;
    }
    else if (ref_Vq < 0 - Vq_max)
    {
        // 本番 TODO 速度マイナス用
        // 電圧振幅指令がマイナス側に大きすぎるとき。計算マイナスの回転速度用であり、実際は不要
        cI[1] -= (ref_Vq + Vq_max) / Kc_i[1]; // 電圧振幅指令が最大値のマイナスと等しくなる積分項を計算
        if (cI[1] > 0)
            cI[1] = 0; // 積分項が正になるとゼロにする
        ref_Vq = -Vq_max;
    }

    refV_dq[0] = ref_Vd;
    refV_dq[1] = ref_Vq;

    V_dq2abo(sin_theta, cos_theta, refV_dq, refV_abo);
    V_abo2uvw(refV_abo, refV_uvw);
	//Harmo3(refV_uvw); // 3次調波加算
	
#ifdef DEBUG_CURRENT_CONTROL
	
	//RF_LOG_PRINTF("%d, %d, %d\r\n", (int)(sin_theta*1000), (int)(cos_theta*1000), (int)(ref_Vq*1000));
	//NRF_LOG_DEBUG("%d, %d\r\n", (int)(refV_dq[0]*1000), (int)(refV_dq[1]*1000));
	//NRF_LOG_DEBUG("%d, %d, %d\r\n", (int)(refV_uvw[0]*1000), (int)(refV_uvw[1]*1000), (int)(refV_uvw[2]*1000));
	NRF_LOG_DEBUG("%d, %d\r\n", (int)(refV_abo[0]*1000), (int)(refV_abo[1]*1000));

#endif
	
}



float torque2Iq(float torque)
{
	return torque/TORQUE_COEFF;
}

float Iq2torque(float iq)
{
	return iq*TORQUE_COEFF;
}

/**@brief Function for setting a maximal torque
 *
 * @param[in] torque maximal value of torque (N.m)
 */
static void maxTorque(float torque)
{
  Iq_max	= torque2Iq(torque);
}


// トルクを制御する
static void torque_control(){
	
	refI_dq[0] = 0;
    refI_dq[1] = torque2Iq(motor.target_torque);
	
}


static void motor_output_pwm(){
	
	m_seq_values.channel_0 = (int)((0.5 - refV_uvw[0] / (INPUT_AMP * 2)) * COUNTER_TOP);
    m_seq_values.channel_1 = (int)((0.5 - refV_uvw[1] / (INPUT_AMP * 2)) * COUNTER_TOP);
    m_seq_values.channel_2 = (int)((0.5 - refV_uvw[2] / (INPUT_AMP * 2)) * COUNTER_TOP);

    nrf_drv_pwm_step(&m_pwm0);

#ifdef DEBUG_OUTPUT_PWM
    static int t = 0;

    if (t == 100)
    {
        NRF_LOG_DEBUG("%d, %d, %d, %d\r\n", (int)(motor.position), m_seq_values.channel_0, m_seq_values.channel_1, m_seq_values.channel_2);
        t = 0;
    }
    t++;
#endif

}
	

/**@brief SAADC A to D Converter
 */

void saadc_get_current_callback(nrf_drv_saadc_evt_t const *p_event)
{
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        float I_xyz[2] = {0, 0};
        static ret_code_t err_code;
        uint8_t current_type;

        if (m_adc_channel_enabled == 0)
        {
            current_type = 0;
            err_code = nrf_drv_saadc_channel_uninit(0);
            APP_ERROR_CHECK(err_code);
            err_code = nrf_drv_saadc_channel_init(1, &channel_1_config);
            APP_ERROR_CHECK(err_code);
            m_adc_channel_enabled = 1;
        }
        else if (m_adc_channel_enabled == 1)
        {
            current_type = 1;
            err_code = nrf_drv_saadc_channel_uninit(1);
            APP_ERROR_CHECK(err_code);
            err_code = nrf_drv_saadc_channel_init(0, &channel_0_config);
            APP_ERROR_CHECK(err_code);
            m_adc_channel_enabled = 0;
        }

        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
        // TODO err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool, SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);

        if (current_type == 0)
        {
            // I_uvw[0] = (float)p_event->data.done.p_buffer[0]*SAADC_INPUT_RANGE/SAADC_MAX_VAL;
            I_uvw[0] = -(float)m_buffer_pool[0] * SAADC_INPUT_RANGE / SAADC_MAX_VAL / R_SHUNT;
        }
        else if (current_type == 1)
        {
            // I_uvw[1]  = (float)p_event->data.done.p_buffer[0]*SAADC_INPUT_RANGE/SAADC_MAX_VAL;
            I_uvw[1] = -(float)m_buffer_pool[0] * SAADC_INPUT_RANGE / SAADC_MAX_VAL / R_SHUNT;
        }

        I_uvw[2] = -I_uvw[0] - I_uvw[1];

        m_adc_evt_counter++;

#ifdef DEBUG_SAADC_CURRENT
        static int saadc_count = 0;
        if (saadc_count == 10)
        {
            //SEGGER_RTT_printf(0, "flag/Ixyz: %d, %d,%d,%d\n", flag, (int)(I_xyz[0]*1000),(int)(I_xyz[1]*1000),(int)(I_xyz[2]*1000));
            SEGGER_RTT_printf(0, "Iuvw: %d,%d,%d\n", (int)(I_uvw[0] * 1000), (int)(I_uvw[1] * 1000), (int)(I_uvw[2] * 1000));
            saadc_count = 0;
        }
        saadc_count += 1;
#endif

		oldI_uvw[0] = I_uvw[0];
        oldI_uvw[1] = I_uvw[1];
        oldI_uvw[2] = I_uvw[2];

        current_control(); 
        motor_output_pwm();  
    }
}

// Timeout handler for the repeated timer
static void position_timer_event_handler(nrf_timer_event_t event_type, void * p_context)
{
	if(event_type == NRF_TIMER_EVENT_COMPARE0){
		
		encoder_as5048a_angle_spi_xfer();
		get_position();
		//get_position_moving_average(); //位置更新、移動平均版
	}
}


// 使用しないが、定義しないとクラッシュ
static void current_timer_event_handler(nrf_timer_event_t event_type, void * p_context){
	
	//NRF_LOG_DEBUG("current_timer_event_handler\r\n");	

}

// 定義しないとクラッシュ
static void control_timer_event_handler(nrf_timer_event_t event_type, void * p_context){

	if (event_type == NRF_TIMER_EVENT_COMPARE0)
    {
        calc_velocity();
	
		if(motor.mode == MOTOR_CONTROL_MODE_VELOCITY){
			
			velocity_control();
			
		} else if (motor.mode == MOTOR_CONTROL_MODE_POSITION){
		
			velocity_control();
			position_control();
		
		} else if (motor.mode == MOTOR_CONTROL_MODE_TORQUE){
			
			torque_control();

			
        } else if (motor.mode == MOTOR_CONTROL_MODE_BRAKE){
            
            
        } else if (motor.mode == MOTOR_CONTROL_MODE_PHASEDIFF_ORIGIN){
			
				
		} else if (motor.mode == MOTOR_CONTROL_MODE_PHASEDIFF_VARIABLE){
			
			
		}
    }
	


}


static void measurement_timer_handler(void * p_context){
	
	
	
}

/* Initializer */

void motor_timer_init()
{
	NRF_LOG_DEBUG("motor_timer_init\r\n");

    uint32_t err_code = NRF_SUCCESS;
    
	NRF_LOG_DEBUG("get position timer init\r\n");
	
	nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
	timer_cfg.interrupt_priority = 6; // 通常は7であり、優先度を高くしてみる

    // 角度取得タイマー Timer
    err_code = nrf_drv_timer_init(&timer_position, &timer_cfg, position_timer_event_handler);
    APP_ERROR_CHECK(err_code);   
    
    uint32_t time_ticks =  nrf_drv_timer_us_to_ticks(&timer_position, GET_POSITION_INT_US);
    
    nrf_drv_timer_extended_compare(
    &timer_position, NRF_TIMER_CC_CHANNEL0, time_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);
   
	// 電流取得タイマー Timer
	NRF_LOG_DEBUG("get current timer init\r\n");
	     
    err_code = nrf_drv_timer_init(&timer_current0, &timer_cfg, current_timer_event_handler);
    APP_ERROR_CHECK(err_code);   
    
	// 62.5 us のような小数は丸められてしまう	
    //uint32_t current_ticks =  nrf_drv_timer_us_to_ticks(&timer_current0, GET_CURRENT_INT_US);
	//NRF_LOG_DEBUG("current_ticks: %d\n\r", current_ticks);
    
    nrf_drv_timer_extended_compare(
    &timer_current0, NRF_TIMER_CC_CHANNEL0, GET_CURRENT_INT_TICKS, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);
	
	// コントロールタイマー
    NRF_LOG_DEBUG("control timer init\r\n");

    err_code = nrf_drv_timer_init(&timer_control0, &timer_cfg, control_timer_event_handler);
    APP_ERROR_CHECK(err_code);

    // 62.5 us のような小数は丸められてしまう	
    uint32_t control_ticks = nrf_drv_timer_us_to_ticks(&timer_control0, CONTROL_INT_US);

    nrf_drv_timer_extended_compare(
        &timer_control0, NRF_TIMER_CC_CHANNEL0, control_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);

    // Create timers.
    err_code = app_timer_create(&motor_meas_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                motor_meas_timeout_handler);
    APP_ERROR_CHECK(err_code);

}


void motor_pwm_init()
{
    uint32_t err_code;
    nrf_drv_pwm_config_t const config0 =
        {
            .output_pins =
                {
                    L6230_IN_1_PIN, // | NRF_DRV_PWM_PIN_INVERTED, // channel 0
                    L6230_IN_2_PIN, // | NRF_DRV_PWM_PIN_INVERTED, // channel 1
                    L6230_IN_3_PIN, // | NRF_DRV_PWM_PIN_INVERTED, // channel 2
                    NRF_DRV_PWM_PIN_NOT_USED
				},
            .irq_priority = APP_IRQ_PRIORITY_LOW,
            .base_clock = NRF_PWM_CLK_16MHz,
            .count_mode = NRF_PWM_MODE_UP_AND_DOWN,
            .top_value = COUNTER_TOP, // Must be more than each sequence value (TODO),
            .load_mode = NRF_PWM_LOAD_INDIVIDUAL,
            .step_mode = NRF_PWM_STEP_TRIGGERED, // When the NRF_PWM_TASK_NEXTSTEP task is triggered.
        };

    err_code = nrf_drv_pwm_init(&m_pwm0, &config0, NULL);
    //err_code = nrf_drv_pwm_init(&m_pwm0, &config0, pwm_event_handler);

    APP_ERROR_CHECK(err_code);

    m_seq_values.channel_0 = 0;
    m_seq_values.channel_1 = 0;
    m_seq_values.channel_2 = 0;
}


void motor_saadc_init(void)
{
    ret_code_t err_code;

    nrf_drv_saadc_config_t config =
        {
            .resolution = NRF_SAADC_RESOLUTION_14BIT,
            .oversample = NRF_SAADC_OVERSAMPLE_DISABLED,
            .interrupt_priority = APP_IRQ_PRIORITY_LOW};

    //set configuration for saadc channel 0
    channel_0_config.resistor_p = NRF_SAADC_RESISTOR_DISABLED;
    channel_0_config.resistor_n = NRF_SAADC_RESISTOR_DISABLED;
    channel_0_config.gain = NRF_SAADC_GAIN1_6;
    channel_0_config.reference = NRF_SAADC_REFERENCE_INTERNAL;
    channel_0_config.acq_time = NRF_SAADC_ACQTIME_3US;
    channel_0_config.mode = NRF_SAADC_MODE_DIFFERENTIAL;
    channel_0_config.pin_p = (nrf_saadc_input_t)(NRF_SAADC_INPUT_AIN0);
    channel_0_config.pin_n = (nrf_saadc_input_t)(NRF_SAADC_INPUT_AIN7);

    channel_1_config.resistor_p = NRF_SAADC_RESISTOR_DISABLED;
    channel_1_config.resistor_n = NRF_SAADC_RESISTOR_DISABLED;
    channel_1_config.gain = NRF_SAADC_GAIN1_6;
    channel_1_config.reference = NRF_SAADC_REFERENCE_INTERNAL;
    channel_1_config.acq_time = NRF_SAADC_ACQTIME_3US;
    channel_1_config.mode = NRF_SAADC_MODE_DIFFERENTIAL;
    channel_1_config.pin_p = (nrf_saadc_input_t)(NRF_SAADC_INPUT_AIN1);
    channel_1_config.pin_n = (nrf_saadc_input_t)(NRF_SAADC_INPUT_AIN7);

    err_code = nrf_drv_saadc_init(&config, saadc_get_current_callback);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(0, &channel_0_config);
    APP_ERROR_CHECK(err_code);

    // Non-blocking mode
    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool, SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);

    m_adc_channel_enabled = 0;
}


void motor_ppi_init()
{
    uint32_t err_code = NRF_SUCCESS;

    err_code = nrf_drv_ppi_init();
    APP_ERROR_CHECK(err_code);

    nrf_ppi_channel_t ppi_channel1;

    err_code = nrf_drv_ppi_channel_alloc(&ppi_channel1);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_ppi_channel_assign(ppi_channel1, nrf_drv_timer_event_address_get(&timer_current0, NRF_TIMER_EVENT_COMPARE0), nrf_drv_saadc_sample_task_get());
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_ppi_channel_enable(ppi_channel1);
    APP_ERROR_CHECK(err_code);
}

static void velocity_control(){
	
	float W_ref = motor.target_velocity;
	float W_lpf = motor.velocity;
		
	static float eI = 0; // 速度制御用偏差の積分値
    static float e_old = 0; // 速度成語用偏差の１サンプル過去の値
	
    float e, ed;
	
	e = W_ref - W_lpf; // 速度偏差
	
    eI = eI  + CONTROL_INT_SEC * e; // 速度偏差の積分値
    ed = (e - e_old) / CONTROL_INT_SEC; // 速度偏差の微分値
    e_old = e;
    float ref_Iq = Kv_p * e + Kv_i * eI + Kv_d * ed; // 速度PID制御、I_q電圧出力

    // アンチワインドアップ
    if (ref_Iq > Iq_max) {
        eI = (Iq_max-Kv_p*e-Kv_d*ed) / Kv_i; // 電圧振幅指令が最大値と等しくなる積分項を計算
    if (eI < 0) eI = 0; // 積分項が負になるとゼロにする
        ref_Iq = Iq_max;
    } else if (ref_Iq < - Iq_max) {
        // 本番 TODO 速度マイナス用
        // 電圧振幅指令がマイナス側に大きすぎるとき。計算マイナスの回転速度用であり、実際は不要
        eI -= (ref_Iq + Iq_max) / Kv_i; // 電圧振幅指令が最大値のマイナスと等しくなる積分項を計算
        if (eI > 0) eI = 0; // 積分項が正になるとゼロにする
        ref_Iq = - Iq_max;
    }
	
	refI_dq[0] = 0; // 最大トルク制御
	
	// 以下を入れると USB バッテリーで急激な逆トルクをかけてもバッテリーがダウンしない -> 加減速含めて再考必要 TODO
	if(I_dq[1] > CURRENT_Q_LIMIT){
		
		refI_dq[1] = 0; //TODO 暫定
	} else {
        refI_dq[1] = ref_Iq;
	}		
    
#ifdef DEBUG_CURRENT
    static int i = 0;
    if (i == 100)
    {
        //NRF_LOG_DEBUG("%d, %d\n", (int)rotation,(int)(ref_Iq*10000));
        NRF_LOG_DEBUG("%d, %d\n", (int)(I_dq[1] * 10000), (int)(ref_Iq * 10000));
        i = 0;
    }
    i++;
#endif
	
	
#ifdef DEBUG_VELOCITY_CONTROL
    static int vt = 0;
    if (vt == 100)
    {
        NRF_LOG_DEBUG("%d, %d, %d\r\n", (int)motor.position, (int)(W_ref * 10000), (int)(W_lpf * 10000));
        vt = 0;
    }

    vt++;
#endif

	
}


static void position_control(){
	
	float e = motor.target_position - motor.position;
	
	if  (e < POSITION_TOLERANCE && e > - POSITION_TOLERANCE)
	{
		motor.target_velocity = 0;
	} else {
		motor.target_velocity = Kp_p * e;
	}
}


static void get_position(){
	
	static float angle = 0;
	static float old_angle = 0;
	static int b_count = 0;
	static int raw_angle = 0;

	// 角度 AS5048A 2^14　ビット単位系
	angle = encoder_as5048a_get_angle();
	raw_angle = angle;

	int16_t diff = angle - old_angle; // 左辺 uint16_t だと右辺の計算　がおかしくなる
	
	if (diff < -BOUNDARY){
		//NRF_LOG_DEBUG("******************** Border Up *****************\r\n");
		old_angle -= P_max;
		b_count += 1;	
		//NRF_LOG_DEBUG("b_count: %d\r\n", b_count);
	    //NRF_LOG_DEBUG("diff: %d\r\n", (int)diff);
	} else if (diff > BOUNDARY){
		//NRF_LOG_DEBUG("******************** Border Down *****************\r\n");
		old_angle += P_max;
		b_count -= 1;
		//NRF_LOG_DEBUG("b_count: %d\r\n", b_count);
	    //NRF_LOG_DEBUG("diff: %d\r\n", (int)diff);
	}
    
    angle = lpf_p * angle + (1-lpf_p) * old_angle;
    
	motor.raw_position = (int)(angle + b_count * P_max);
	// ラジアンに変換
    motor.position = TICKS_TO_RADIANS(motor.raw_position);
	
	old_angle = angle;	

    int r = (angle + phase_diff) / ELECTRIC_CYCLE_TICKS;
    float d = angle + phase_diff - r * ELECTRIC_CYCLE_TICKS;
    motor.theta = d / ELECTRIC_CYCLE_TICKS * 2 * PI ; // ベクトル制御用電気角 θ の更新
	sin_theta = _sinf(motor.theta); // sinθ をここで計算
	cos_theta = _cosf(motor.theta); // cosθ をここで計算

	#ifdef DEBUG_POSITION
	static int t;
	if(t == 1000)
	{
		//NRF_LOG_DEBUG("angle: %d, %d, %d\r\n", angle, old_angle, b_count);
		//NRF_LOG_DEBUG("angle: %d, abs: %d, d: %d, theta x 1000: %d\r\n", (int)angle, (int)motor.position, (int)d, (int)(motor.theta * 1000));
		//NRF_LOG_DEBUG("theta: %d, sin cos: %d, %d\r\n",(int)(motor.theta * 10000), (int)(sin_theta * 10000), (int)(cos_theta * 10000));
		//NRF_LOG_DEBUG("%d, %d\r\n", raw_angle, (int)angle);		
		NRF_LOG_DEBUG("%d, %d\r\n", motor.raw_position, (int)(motor.position * 10000));	
		t = 0;
	}
	t++;
	#endif
	
}



// 宣言 
#define AVE_NUM 5


static void get_position_moving_average()
{
	static float pos_arr[AVE_NUM + 1];
	static int cnt = 0; // カウント
	static int oldCnt = 1;
	static float sum = 0;
	static float angle = 0;
	static float old_angle = 0;
	static float raw_abs_pos = 0;
	static float old_raw_abs_pos = 0;
	static float ave_pos = 0; 
	
	static float lpf_abs_pos = 0;
	
	angle = encoder_as5048a_get_angle();

	int16_t diff = angle - old_angle; // 左辺 uint16_t だと右辺の計算　がおかしくなる
	old_angle = angle;
	
	if (diff < -BOUNDARY){
		//NRF_LOG_DEBUG("******************** Border Up *****************\r\n");
		diff += P_max;
	    //NRF_LOG_DEBUG("diff: %d\r\n", (int)diff);
	} else if (diff > BOUNDARY){
		//NRF_LOG_DEBUG("******************** Border Down *****************\r\n");
		diff -= P_max;
		//NRF_LOG_DEBUG("diff: %d\r\n", (int)diff);
	}
    
    raw_abs_pos = old_raw_abs_pos + diff;
	old_raw_abs_pos = raw_abs_pos;
	
	lpf_abs_pos = lpf_p * raw_abs_pos + (1-lpf_p) * lpf_abs_pos;
	
    if(cnt == AVE_NUM+1) cnt = 0;
    else if (cnt == AVE_NUM) oldCnt = 0;
    
	pos_arr[cnt] = raw_abs_pos;
    sum = sum + pos_arr[cnt] - pos_arr[oldCnt];
    ave_pos = sum / AVE_NUM;
    
	motor.position = ave_pos;
	
    cnt++;
    oldCnt++;
    
	float d = ((int)lpf_abs_pos + phase_diff) % ELECTRIC_CYCLE_TICKS;
	
	motor.theta = d * 2 * PI / ELECTRIC_CYCLE_TICKS ;

	#ifdef DEBUG_POSITION
	{
		//NRF_LOG_DEBUG("angle: %d, %d, %d\r\n", angle, old_angle, b_count);
		//NRF_LOG_DEBUG("angle: %d, abs: %d, d: %d, theta x 1000: %d\r\n", angle, abs_position, (int)d, (int)(motor.theta * 1000));
		NRF_LOG_DEBUG("%d, %d, %d\r\n", (int)raw_abs_pos, (int)lpf_abs_pos, (int)ave_pos);
	}
	#endif
	
}



	

