/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef MOTOR_CONTROL_H
#define MOTOR_CONTROL_H

#include "app_util_platform.h"

/*　廃止予定グローバル変数 */

// old
extern int phase_diff;

extern float Kp;
extern float Ki;
extern float Kd; 

extern float LPF; // 速度用 ローパスフィルター
extern float LPF_r; //エンコーダ値用ローパスフィルタ

extern float Kp_bias;
extern float Kp3; 

extern float t_speed;
extern float brake_area;
extern float stop_area; 

extern float Kp2[2]; 
extern float Ki2[2]; 
extern float Kd2[2]; 

extern float W_ref;
extern int abs_position;

//new
extern float Kc_p[2]; 
extern float Kc_i[2]; 
extern float Kc_d[2]; 
extern float lpf_c; // ローパスフィルター

/* 速度制御 */
// PID係数
extern float Kv_p;
extern float Kv_i;
extern float Kv_d;
extern float lpf_v; // ローパスフィルター

/* 位置制御 */	
extern float Kp_p; 
extern float lpf_p; // ローパスフィルター

///ベクトル制御用変数
extern float TS_v; //0.01;
extern float LPF_v; //0.9;
extern float V_uvw[3];
extern float V_abo[3];
extern float V_dq[2];
extern float I_uvw[3];
extern float oldI_uvw[3];

extern float I_abo[3];
extern float I_dq[2];
extern float I_dq_lpf[2];
//モーターへの入力電圧・電流
extern float refV_uvw[3];
extern float refV_abo[3];
extern float refV_dq[2];
extern float refI_uvw[3];
extern float refI_abo[3];
extern float refI_dq[2];

extern float Iq_max;   //8.0f; //32.0f;// 4.0f;
extern float theta;     //U軸とd軸とのなす角,単位ラジアン

extern float c_old[2];
extern float cI[2];
extern float Vd_max;
extern float Vq_max;
extern float V_max;

extern float sin_theta;
extern float cos_theta;

#define CURRENT_D_AXIS_P (5.0) // 5.0
#define CURRENT_D_AXIS_I (1.0) // 0.15
#define CURRENT_D_AXIS_D (0.0) // 0
	
#define CURRENT_Q_AXIS_P (0.05) // 0.05
#define CURRENT_Q_AXIS_I (0.01) // 0.01
#define CURRENT_Q_AXIS_D (0.0) // 0.002

#define VELOCITY_P (15.0) // 5.0 <--#define MOTOR_CONTROL_TEMP　のとき 20.0
#define VELOCITY_I (0.0005) // 0.0005
#define VELOCITY_D (0.0) // 1.0

#define CURRENT_Q_LIMIT (6.5)

#define POSITION_P (2.0) // 0.01

#define POSITION_TOLERANCE 1 // ticks ±設定値  の範囲になると 速度制御ゼロとする

// ローパスフィルター
// 新しい計測値の重み係数 LPF, 古いローパス値の重み係数は (1-LPF)
#define	LOW_PASS_FILTER_CURRENT 0.8 //0.95 // 元々 0.1->0.95
#define	LOW_PASS_FILTER_VELOCITY 0.1 //　元々 0.1->0.1
#define	LOW_PASS_FILTER_SMOOTHED_VELOCITY 0.05 //　元々 0.1->0.1
#define	LOW_PASS_FILTER_POSITION 0.8 // 元々 0.05->0.8

#define CALC_VEROCITY_INT_MS CONTROL_INT_MS
#define CALC_VEROCITY_INT_SEC CONTROL_INT_SEC


#define rpm_ref (TPMS_TO_RPM(W_ref)) // ターゲットとする角速度の目標値 rpm 場所 TODO

#define TORQUE_COEFF (0.045f) // Iq-Torque 直線の傾き
#define DEFAULT_MAX_TORQUE (5.0f)


typedef enum 
{
    MOTOR_CONTROL_MODE_NONE,
    MOTOR_CONTROL_MODE_VELOCITY,
    MOTOR_CONTROL_MODE_POSITION,
    MOTOR_CONTROL_MODE_TORQUE,
    MOTOR_CONTROL_MODE_BRAKE,
	
	// 非公開
	MOTOR_CONTROL_MODE_PHASEDIFF_AUTO,
	MOTOR_CONTROL_MODE_PHASEDIFF_ORIGIN,
	MOTOR_CONTROL_MODE_PHASEDIFF_VARIABLE
    
} motor_mode_t;
		

// Config output pins for L6230 Motor driver
void motor_gpio_config(void);

/* Setter and Getter */
motor_mode_t motor_get_mode(void);
float motor_get_velocity(void);
float motor_get_smoothed_velocity(void);
int motor_get_raw_position(void);
float motor_get_position(void);
float motor_get_torque(void);

uint16_t motor_get_phase_diff(void); // 位相調整値を取得
void motor_set_phase_diff(uint16_t diff); // 位相調整値をセット

void motor_set_mode(motor_mode_t mode);
void motor_preset_position(float position);
void motor_preset_speed(float speed);
void motor_set_target_position(float position);
void motor_set_target_velocity(float velocity);
void motor_set_target_torque(float torque);

void motor_enable(void);
void motor_disable(void);

void motor_stop(void);
void motor_brake(void);
void motor_free(void);

void motor_preset_position(float position);
void motor_preset_speed(float speed);

void motor_run_at_velocity(float velocity);
void motor_run_forward(void);
void motor_run_reverse(void);
void motor_move_to(float position, float speed);
void motor_move_to_pos(float position);
void motor_move_by(float distance, float speed);
void motor_move_by_dist(float distance);
void motor_hold_torque(float torque);
void motor_set_max_speed(float speed);
void motor_set_min_speed(float speed);
void motor_set_max_torque(float torque);

void motor_timer_init(void);
void motor_ppi_init(void);
void motor_pwm_init(void);
void motor_saadc_init(void);
void motor_start_position_timer(void);
void motor_stop_position_timer(void);
void motor_measurement_timer_start(void);
void motor_measurement_timer_stop(void);
	
// PIDコントローラの係数
void motor_set_speed_pid_p(float p);
void motor_set_speed_pid_i(float i);
void motor_set_speed_pid_d(float d);
void motor_set_position_pid_p(float p);

#endif // MOTOR_CONTROL_H

