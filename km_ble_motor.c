/* Copyright (c) 2013 Nordic Semiconductor. All Rights Reserved.
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the license.txt file.
 */

#include "km_ble_motor.h"
#include "ble_srv_common.h"
#include "ble_err.h"
#include "sdk_common.h"
#include "SEGGER_RTT.h"
#include "legacy_command_control.h"

#include "type_utility.h" // float byte[] の変換

#define NRF_LOG_MODULE_NAME "BLE_MOTOR"
#include "nrf_log.h"

/* KM_MOTOR_SERVICE */
// Forward declaration of the ble_km_t type. 
typedef struct ble_km_s ble_km_t;


/**@brief KM Motor Service structure. This structure contains various status information for the service. */
struct ble_km_s
{
    uint16_t                    service_handle;      /**< Handle of Motor Service (as provided by the BLE stack). */
    ble_gatts_char_handles_t    control_char_handles; /* <  */
    ble_gatts_char_handles_t    log_char_handles;    /**< Handles related to the LED Characteristic. */
    ble_gatts_char_handles_t    led_char_handles;    /**< Handles related to the LED Characteristic. */
    ble_gatts_char_handles_t    measurement_char_handles; /*  */
    ble_gatts_char_handles_t    imu_measurement_char_handles; /*  */
    ble_gatts_char_handles_t    legacy_control_char_handles;    /**< Handles related to the MOTOR_CONTROL_MC Characteristic. */
    ble_gatts_char_handles_t    legacy_control_mode_char_handles;    /**< Handles related to the MOTOR_CONTROL_MC Characteristic. */

    uint8_t                     uuid_type;           /**< UUID type for the KM Motor Service. */
    uint16_t                   conn_handle;         /**< Handle of the current connection (as provided by the BLE stack). BLE_CONN_HANDLE_INVALID if not in a connection. */
    
   
    bool is_control_notification_enabled; /**< Variable to indicate if the peer has enabled notification of the Control characteristic.*/
    bool is_meas_notification_enabled; /**< Variable to indicate if the peer has enabled notification of the Measurement characteristic.*/
    bool is_imu_meas_notification_enabled; /**< Variable to indicate if the peer has enabled notification of the Imu Measurement characteristic.*/
    
    bool is_measure_position_enabled; /**< Variable to indicate if the peer has enabled measurement of the Position. */
    bool is_measure_speed_enabled; /**< Variable to indicate if the peer has enabled measurement of the Speed. */
    bool is_measure_torque_enabled; /**< Variable to indicate if the peer has enabled measurement of the torque. */

};


ble_km_t p_km; // Keigan Motor Service 構造体

/**@brief Function for handling the Connect event.
 *
 * @param[in] p_km      KM Motor Service structure.
 * @param[in] p_ble_evt  Event received from the BLE stack.
 *
 */

static void on_connect(ble_evt_t * p_ble_evt)
{
    p_km.conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}


/**@brief Function for handling the Disconnect event.
 *
 * @param[in] p_km      KM Motor Service structure.
 * @param[in] p_ble_evt  Event received from the BLE stack.
 */
static void on_disconnect(ble_evt_t * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_km.conn_handle = BLE_CONN_HANDLE_INVALID;
}


/**@brief Function for handling the Write event.
 *
 * @param[in] p_km      KM Motor Service structure.
 * @param[in] p_ble_evt  Event received from the BLE stack.
 */
static void on_write(ble_evt_t * p_ble_evt)
{
	NRF_LOG_DEBUG("on_write.\n\r");
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;
    
    /* KM Motor Control */
    if (p_evt_write->handle == p_km.control_char_handles.value_handle)
    {            
        // TODO
    }
    /* KM Motor Legacy Control */
    else if (p_evt_write->handle == p_km.legacy_control_char_handles.value_handle)
    {            
		ble_legacy_control_handler(p_evt_write->data, p_evt_write->len);
    }     
	/* KM Motor Legacy Control Mode */
    else if (p_evt_write->handle == p_km.legacy_control_mode_char_handles.value_handle){ 
        NRF_LOG_DEBUG("ble_legacy_control_mode_handler\r\n");
        ble_legacy_control_mode_handler(p_evt_write->data, p_evt_write->len);            
    } 	
    /* KM Motor LED */
    else if (p_evt_write->handle == p_km.led_char_handles.value_handle){
		NRF_LOG_DEBUG("ble_led_led_handler\r\n");
        ble_legacy_led_handler(p_evt_write->data, p_evt_write->len); //TODO
	}
    /* KM Motor Measurement CCCD */
    else if ((p_evt_write->handle == p_km.measurement_char_handles.cccd_handle) && (p_evt_write->len == 2)){ 
        
        if (ble_srv_is_notification_enabled(p_evt_write->data))
        {
            NRF_LOG_DEBUG("KM Motor Measurement CCCD notification enabled \n");
            p_km.is_meas_notification_enabled = true;
        }
        else
        {
            NRF_LOG_DEBUG("KM Motor Measurement CCCD notification disabled \n");
            p_km.is_meas_notification_enabled = false;
        }              
    }	
	    /* KM Motor IMU Measurement CCCD */
    else if ((p_evt_write->handle == p_km.imu_measurement_char_handles.cccd_handle) && (p_evt_write->len == 2)){ 
        
        if (ble_srv_is_notification_enabled(p_evt_write->data))
        {
            NRF_LOG_DEBUG("KM IMU Measurement CCCD notification enabled \n");
            p_km.is_imu_meas_notification_enabled = true;
        }
        else
        {
            NRF_LOG_DEBUG("KM IMU Measurement CCCD notification disabled \n");
            p_km.is_imu_meas_notification_enabled = false;
        }              
    }
}


void ble_motor_on_ble_evt(ble_evt_t * p_ble_evt)
{
    //NRF_LOG_DEBUG("ble_km_on_ble_evt\n");
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_DEBUG("BLE_GAP_EVT_CONNECTED\n");
            on_connect(p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_DEBUG("BLE_GAP_EVT_DISCONNECTED\n");
            on_disconnect(p_ble_evt);
            break;
            
        case BLE_GATTS_EVT_WRITE:
            NRF_LOG_DEBUG("BLE_GATTS_EVT_WRITE\n");
            on_write(p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}




/**@brief モーター制御用のキャラクタリスティクス
 *
 * @param[in] p_km      KM Motor Service structure.
 * @param[in] p_km_init KM Motor Service initialization structure.
 *
 * @retval NRF_SUCCESS on success, else an error value from the SoftDevice
 */
static uint32_t motor_control_char_add(void)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.vloc = BLE_GATTS_VLOC_STACK;
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1; // read を許可
    char_md.char_props.write = 1; // write を許可
    char_md.char_props.write_wo_resp = 1; // write without response を許可
    char_md.char_props.notify = 1; // notify を許可
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_km.uuid_type;
    ble_uuid.uuid = KM_UUID_MOTOR_CONTROL_CHAR; 
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    // BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&attr_md.read_perm); ここでセキュリティモードを規定
    // BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&attr_md.write_perm);
    
    // BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 1; //sizeof(uint16_t); /* <-- uint8_t KM*/
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = KM_MOTOR_CONTROL_CHAR_MAX_VLEN; //sizeof(uint32_t); /* <-- uint8_t KM*/
    attr_char_value.p_value      = NULL;

    return sd_ble_gatts_characteristic_add(p_km.service_handle,
                                               &char_md,
                                               &attr_char_value,
                                               &p_km.control_char_handles);
}

/**@brief Function for adding the Motor LOG Characteristic.
 *
 * @param[in] p_km      KM Motor Service structure.
 * @param[in] p_km_init KM Motor Service initialization structure.
 *
 * @retval NRF_SUCCESS on success, else an error value from the SoftDevice
 */
static uint32_t motor_log_char_add(void)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read   = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_km.uuid_type;
    ble_uuid.uuid = KM_UUID_MOTOR_LOG_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = sizeof(uint8_t);
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = KM_MOTOR_LOG_CHAR_MAX_VLEN;
    attr_char_value.p_value      = NULL;

    return sd_ble_gatts_characteristic_add(p_km.service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_km.log_char_handles);
}


/**@brief Function for adding the Motor LED Characteristic.
 *
 * @param[in] p_km      KM Motor Service structure.
 * @param[in] p_km_init KM Motor Service initialization structure.
 *
 * @retval NRF_SUCCESS on success, else an error value from the SoftDevice
 */
static uint32_t motor_led_char_add(void)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.read   = 1;
    char_md.char_props.write  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_km.uuid_type;
    ble_uuid.uuid = KM_UUID_MOTOR_LED_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = sizeof(uint8_t);
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = KM_MOTOR_LED_CHAR_MAX_VLEN;
    attr_char_value.p_value      = NULL;

    return sd_ble_gatts_characteristic_add(p_km.service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_km.led_char_handles);
}


/**@brief Function for adding the Motor Measurement Characteristic.
 *
 * @param[in] p_km      KM Motor Service structure.
 * @param[in] p_km_init KM Motor Service initialization structure.
 *
 * @retval NRF_SUCCESS on success, else an error value from the SoftDevice
 */
static uint32_t motor_measurement_char_add(void)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.vloc = BLE_GATTS_VLOC_STACK;
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1; // read を許可
    char_md.char_props.notify = 1; // notify を許可
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_km.uuid_type;
    ble_uuid.uuid = KM_UUID_MOTOR_MEASUREMENT_CHAR; 
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    // BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&attr_md.read_perm); ここでセキュリティモードを規定
    // BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&attr_md.write_perm);
    
    // BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 1;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = KM_MOTOR_MEASUREMENT_CHAR_MAX_VLEN;
    attr_char_value.p_value      = NULL;

    return sd_ble_gatts_characteristic_add(p_km.service_handle,
                                               &char_md,
                                               &attr_char_value,
                                               &p_km.measurement_char_handles);
}


/**@brief Function for adding the Motor Measurement Characteristic.
 *
 * @param[in] p_km      KM Motor Service structure.
 * @param[in] p_km_init KM Motor Service initialization structure.
 *
 * @retval NRF_SUCCESS on success, else an error value from the SoftDevice
 */
static uint32_t motor_imu_measurement_char_add(void)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.vloc = BLE_GATTS_VLOC_STACK;
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1; // read を許可
    char_md.char_props.notify = 1; // notify を許可
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_km.uuid_type;
    ble_uuid.uuid = KM_UUID_MOTOR_IMU_MEASUREMENT_CHAR; 
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    // BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&attr_md.read_perm); ここでセキュリティモードを規定
    // BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&attr_md.write_perm);
    
    // BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 1;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = KM_MOTOR_IMU_MEASUREMENT_CHAR_MAX_VLEN;
    attr_char_value.p_value      = NULL;

    return sd_ble_gatts_characteristic_add(p_km.service_handle,
                                               &char_md,
                                               &attr_char_value,
                                               &p_km.imu_measurement_char_handles);
}



/**@brief Function for adding the Motor Control Characteristic.
 *
 * @param[in] p_km      KM Motor Service structure.
 * @param[in] p_km_init KM Motor Service initialization structure.
 *
 * @retval NRF_SUCCESS on success, else an error value from the SoftDevice
 */
static uint32_t motor_legacy_control_char_add(void)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.vloc = BLE_GATTS_VLOC_STACK;
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1; // read を許可
    char_md.char_props.write = 1; // write を許可
    char_md.char_props.write_wo_resp = 1; // write without response を許可
    char_md.char_props.notify = 1; // notify を許可
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_km.uuid_type;
    ble_uuid.uuid = KM_UUID_MOTOR_LEGACY_CONTROL_CHAR; 
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    // BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&attr_md.read_perm); ここでセキュリティモードを規定
    // BLE_GAP_CONN_SEC_MODE_SET_ENC_WITH_MITM(&attr_md.write_perm);
    
    // BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 1; //sizeof(uint16_t); /* <-- uint8_t KM*/
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = KM_MOTOR_LEGACY_CONTROL_CHAR_MAX_VLEN; //sizeof(uint32_t); /* <-- uint8_t KM*/
    attr_char_value.p_value      = NULL;

    return sd_ble_gatts_characteristic_add(p_km.service_handle,
                                               &char_md,
                                               &attr_char_value,
                                               &p_km.legacy_control_char_handles);
}


/**@brief Function for adding the Motor Control Mode Characteristic.
 *
 * @param[in] p_km      KM Motor Service structure.
 * @param[in] p_km_init KM Motor Service initialization structure.
 *
 * @retval NRF_SUCCESS on success, else an error value from the SoftDevice
 */
static uint32_t motor_legacy_control_mode_char_add(void)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
    
    memset(&char_md, 0, sizeof(char_md));
    
    char_md.char_props.read   = 1; // read を許可
    char_md.char_props.write = 1; // write を許可
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = NULL;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_km.uuid_type;
    ble_uuid.uuid = KM_UUID_MOTOR_LEGACY_CONTROL_MODE_CHAR; 
    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 1;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = KM_MOTOR_LEGACY_CONTROL_MODE_CHAR_MAX_VLEN;
    attr_char_value.p_value      = NULL;

    return sd_ble_gatts_characteristic_add(p_km.service_handle,
                                               &char_md,
                                               &attr_char_value,
                                               &p_km.legacy_control_mode_char_handles);
}



uint32_t ble_motor_init(void)
{
    uint32_t   err_code;
    ble_uuid_t ble_uuid;

    // Initialize service structure.
    p_km.conn_handle       = BLE_CONN_HANDLE_INVALID;  
    p_km.is_measure_position_enabled = true;
    p_km.is_measure_speed_enabled = true;
    p_km.is_measure_torque_enabled = true;
    
    // Add service.
    ble_uuid128_t base_uuid = {KM_UUID_MOTOR_BASE};
    err_code = sd_ble_uuid_vs_add(&base_uuid, &p_km.uuid_type);
    VERIFY_SUCCESS(err_code);

    ble_uuid.type = p_km.uuid_type;
    ble_uuid.uuid = KM_UUID_MOTOR_SERVICE;

    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_km.service_handle);
    VERIFY_SUCCESS(err_code);

    // Add characteristics.
    err_code = motor_control_char_add();
    VERIFY_SUCCESS(err_code);

	err_code = motor_log_char_add();
    VERIFY_SUCCESS(err_code);
    
	err_code = motor_led_char_add();
    VERIFY_SUCCESS(err_code);
	
    err_code = motor_measurement_char_add();
    VERIFY_SUCCESS(err_code);

	err_code = motor_imu_measurement_char_add();
    VERIFY_SUCCESS(err_code);
	
    // Add characteristics.
    err_code = motor_legacy_control_char_add();
    VERIFY_SUCCESS(err_code);
	
	err_code = motor_legacy_control_mode_char_add();
    VERIFY_SUCCESS(err_code);
    
    return NRF_SUCCESS;
}


/**@brief Function for encoding Motor Measurement.
 *
 * @param[in]   p_km              KM Motor Service structure.
 * @param[in]   heart_rate         Measurement to be encoded.
 * @param[out]  p_encoded_buffer   Buffer where the encoded data will be written.
 *
 * @return      Size of encoded data.
 */
static uint8_t motor_meas_encode(float position, float speed, float torque, uint8_t * p_encoded_buffer)
{
    /* flags の廃止 */
    //uint8_t flags = 0;
    uint8_t len   = 0;//1; // flags byte

    // Set sensor contact related flags
    if (p_km.is_measure_position_enabled == true)
    {
        //flags |= KM_FLAG_MASK_MEASURE_POSITION;
        len   += float_big_encode(position, &p_encoded_buffer[len]);
    }
    if (p_km.is_measure_speed_enabled == true)
    {
        //flags |= KM_FLAG_MASK_MEASURE_SPEED;
        len   += float_big_encode(speed, &p_encoded_buffer[len]);
    }
    if (p_km.is_measure_torque_enabled == true)
    {
        //flags |= KM_FLAG_MASK_MEASURE_TORQUE;
        len   += float_big_encode(torque, &p_encoded_buffer[len]);
    }

    // Add flags
    //p_encoded_buffer[0] = flags;

    return len;
}




void ble_motor_measurement_update(float position, float speed, float torque)
{
    uint32_t err_code;

    // Send value if connected and notifying
    if (p_km.conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        uint8_t                 encoded_meas[20];
        uint16_t               len;
        uint16_t               hvx_len;
        ble_gatts_hvx_params_t hvx_params;

        len     = motor_meas_encode(position, speed, torque, encoded_meas);
        hvx_len = len;

        memset(&hvx_params, 0, sizeof(hvx_params));

        hvx_params.handle = p_km.measurement_char_handles.value_handle;
        hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
        hvx_params.offset = 0;
        hvx_params.p_len  = &hvx_len;
        hvx_params.p_data = encoded_meas;

        err_code = sd_ble_gatts_hvx(p_km.conn_handle, &hvx_params);
        if ((err_code == NRF_SUCCESS) && (hvx_len != len))
        {
            err_code = NRF_ERROR_DATA_SIZE;
        }
    }
    
    else
    {
        err_code = NRF_ERROR_INVALID_STATE;
    }

    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING))
    {
        APP_ERROR_HANDLER(err_code);
    }
}

