/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef DEMO_H
#define DEMO_H

void demo_start(void);

#endif // DEMO_H
