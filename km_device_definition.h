#ifndef KM_DEVICE_DEFINITION_H
#define KM_DEVICE_DEFINITION_H

#define MANUFACTURER_NAME          "Keigan Inc."   /* 製造者名 */

//アドバタイジング、GAPのデバイス名
#define DEVICE_NAME                "Keigan Motor"  // TODO add 乱数

// シリアルナンバーは初回起動時（マジックナンバー指定時）に自動設定

// ハードウェアバージョン
#define HARDWARE_REVISION_STRING    "rev 1.0"  // ハードウェアのリビジョンを表す文字列。Device Information Serviceで使います。

// ファームウェアは、機能が同じであるならば、同じ番号を用いる。
// FIRMWARE_REVISIONは、ファームウェアのリビジョン。先頭1バイトがメジャーバージョン、後ろ1バイトがマイナーバージョン 0xJJMN の表記。
// FIRMWARE_REVISION_STRINGは、ファームウェアのリビジョンを表す文字列。Device Information Serviceで使います
#define	FIRMWARE_REVISION           0x0080
#define FIRMWARE_REVISION_STRING    "rev 0.80"

#endif /* KM_DEVICE_DEFINITION_H */
