#ifndef km_gatt_mtu_h
#define km_gatt_mtu_h

#include "ble.h"

void gatt_mtu_init(void);
void gatt_mtu_on_ble_evt(ble_evt_t * p_ble_evt);

#endif

