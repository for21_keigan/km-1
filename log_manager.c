#include "log_manager.h"

// ログ管理のための構造体
typedef struct log_manager {
    
    bool is_output_echo_enabled;
    bool is_output_result_enabled;
    bool is_output_register_enabled;
    bool is_output_measurement_enabled;

} log_manager_t;

log_manager_t log_manager = {

    .is_output_echo_enabled = true,
    .is_output_result_enabled = true,
    .is_output_register_enabled = true,
    .is_output_measurement_enabled = false

};

void set_log_output(uint8_t bit_flag){

    if(bit_flag & LOG_OUTPUT_BIT_ECHO){


    }
    if(bit_flag & LOG_OUTPUT_BIT_RESULT){


    }
    if(bit_flag & LOG_OUTPUT_BIT_REGISTER){


    }
    if(bit_flag & LOG_OUTPUT_BIT_MEASUREMENT){


    }
}