#ifndef km_ble_definition_h
#define km_ble_definition_h

#include <app_util.h>

// KM1の動作振る舞いを決めるグローバルな定数を定義

/* Scheduler のパラメータ */
//  Maximum size of events data in the application scheduler queue aligned to 32 bits
//                                            MAX(BLE_STACK_HANDLER_SCHED_EVT_SIZE)) \

#define SCHED_MAX_EVENT_DATA_SIZE   (CEIL_DIV( \
                                        MAX( \
                                            MAX(sizeof(uint32_t), BLE_STACK_HANDLER_SCHED_EVT_SIZE) \
                                            ,sizeof(app_timer_event_t) ) \
                                        , sizeof(uint32_t)) \
                                      * sizeof(uint32_t))

#define SCHED_QUEUE_SIZE                100 // 20

#endif /* km1_ble_definition_h */
