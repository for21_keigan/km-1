/*
 * Copyright (c) 2016 Keigan Inc. All Rights Reserved.
 */

#ifndef MOTION_PLANNER_H
#define MOTION_PLANNER_H

#include "app_util_platform.h"

/** Counts number of elements inside the array
 */
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

typedef enum {				// mp->curve_type values
	
	CURVE_TYPE_TRAPEZOID,
    CURVE_TYPE_S_CURVE
	
} mp_curve_type_t ;

typedef enum {				
	
	DIRECTION_FORWARD,
    DIRECTION_REVERSE
	
} mp_direction_t ;

typedef enum {
	
	SHAPE_HEAD_BODY_TAIL,
	SHAPE_HEAD_TAIL
	
} mp_position_shape_t;


// TODO 廃止？　検討必要
typedef struct motion_plan_velocity {			// モーションプランニングのバッファ：速度

    mp_curve_type_t curve_type;
	
	float time; //経過時間 
    float acc; //加速度 ticks/ms^2
    float dec; //減速度 ticks/ms^2

    float acc_duration; //加速時間 ms
    float dec_duration; //減速時間 ms
	
    float total_duration; //モーショントータル時間 ms
	float head_duration; //速度カーブにおける台形の頭部分 ms
	float body_duration; //速度カーブにおける胴体部分 ms
	float tail_duration; //速度カーブにおける台形の尻尾部分 ms
	
    float initial_velocity;			// 初期速度 ticks/ms
	float target_velocity;			// 目標速度 ticks/ms
	
	mp_direction_t direction; // 進行方向

} mp_v_t;

typedef struct motion_plan_position {			// モーションプランニングのバッファ：位置

    mp_curve_type_t curve_type;
	mp_position_shape_t shape;
	
	float time; //経過時間 
    float acc; //加速度 ticks/ms^2
    float dec; //減速度 ticks/ms^2

    float acc_duration; //加速時間 ms
	float cruise_duration; //定速度移動時間 ms
    float dec_duration; //減速時間 ms	
    float total_duration; //モーショントータル時間 ms
	
    float entry_velocity;			// 初期速度 ticks/ms
	float cruise_velocity;			// 定速移動時の目標速度 ticks/ms
	
	float initial_position; // 初期位置  ticks
	float target_position; // 目標位置 ticks
	float distance; // 位置差分
	
	float acc_front_jerk_ratio;
	float acc_back_jerk_ratio;
	float dec_front_jerk_ratio;
	float dec_back_jerk_ratio;
	
	/* s-curve における時間領域 */
	float sec_1_duration; // 加速 jerk 前側
	float sec_2_duration; // 加速 一定 
	float sec_3_duration; // 加速 jerk 後側
	float sec_4_duration; // 定速
	float sec_5_duration; // 減速 jerk 前側
	float sec_6_duration; // 減速 一定
	float sec_7_duration; // 減速 jerk 後側
	
	mp_direction_t direction; // 進行方向

} mp_p_t;


void calc_motion_plan_velocity(mp_v_t *mpv);
float calc_target_velocity(const mp_v_t *mpv);

void calc_motion_plan_position(mp_p_t *mpp);
float calc_target_position(const mp_p_t *mpp);

void calc_motion_plan_position_s(mp_p_t *mpp);
float calc_target_position_s(const mp_p_t *mpp);

#endif // MOTION_PLANNER_H

