#include "motion_control.h"
#include "motor_control.h"
#include "app_timer.h"
#include "motion_planner.h"

#define APP_TIMER_PRESCALER             0  
#define MOTION_INT_MS 1
APP_TIMER_DEF(app_motion_timer_id); 
uint32_t motion_count;

mp_p_t mpp = {
	.acc = 10,
	.dec = 10,
	.cruise_velocity = 50,
};

	

void motion_timer_handler(void * p_context)
{
    float target_position = calc_target_position(&mpp);
	motor_set_target_position(target_position);

	if(motion_count % 50 == 0) {
		//NRF_LOG_PRINTF("speed, current, target = %d, %d, %d\r\n",W_ref , abs_position, target_position);
		motion_count = 0;
	}
	
	motion_count ++;
    mpp.time += 1;
}


void motion_timer_init()
{
	ret_code_t err_code;
	err_code = app_timer_create(&app_motion_timer_id, APP_TIMER_MODE_REPEATED, motion_timer_handler);
	APP_ERROR_CHECK(err_code);
}

void motion_timer_start(void)
{
    uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_start(app_motion_timer_id, APP_TIMER_TICKS(MOTION_INT_MS), NULL); 
	APP_ERROR_CHECK(err_code);
}

void motion_timer_stop(void)
{
    uint32_t err_code = NRF_SUCCESS;
	err_code = app_timer_stop(app_motion_timer_id);
    APP_ERROR_CHECK(err_code);
}


void motion_set_position(float position, float speed){
	
	motion_timer_stop();
	mpp.time = 0;
	mpp.initial_position = motor_get_position();
	mpp.target_position = position;
	//mpp.cruise_velocity = speed;
	
	calc_motion_plan_position(&mpp);
	motion_timer_start();
}


void motion_set_velocity(float velocity){
	
}