#ifndef KM_ERROR_H
#define KM_ERROR_H

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup KM_ERRORS_BASE Error Codes Base number definitions
 * @{ */
#define KM_ERROR_BASE_NUM      (0x0)       ///< Global error base
#define KM_ERROR_SDM_BASE_NUM  (0x1000)    ///< SDM error base
#define KM_ERROR_SOC_BASE_NUM  (0x2000)    ///< SoC error base
#define KM_ERROR_STK_BASE_NUM  (0x3000)    ///< STK error base
/** @} */

#define KM_SUCCESS                           (KM_ERROR_BASE_NUM + 0)  ///< Successful command
#define KM_ERROR_SVC_HANDLER_MISSING         (KM_ERROR_BASE_NUM + 1)  ///< SVC handler is missing
#define KM_ERROR_SOFTDEVICE_NOT_ENABLED      (KM_ERROR_BASE_NUM + 2)  ///< SoftDevice has not been enabled
#define KM_ERROR_INTERNAL                    (KM_ERROR_BASE_NUM + 3)  ///< Internal Error
#define KM_ERROR_NO_MEM                      (KM_ERROR_BASE_NUM + 4)  ///< No Memory for operation
#define KM_ERROR_NOT_FOUND                   (KM_ERROR_BASE_NUM + 5)  ///< Not found
#define KM_ERROR_NOT_SUPPORTED               (KM_ERROR_BASE_NUM + 6)  ///< Not supported
#define KM_ERROR_INVALID_PARAM               (KM_ERROR_BASE_NUM + 7)  ///< Invalid Parameter
#define KM_ERROR_INVALID_STATE               (KM_ERROR_BASE_NUM + 8)  ///< Invalid state, operation disallowed in this state
#define KM_ERROR_INVALID_LENGTH              (KM_ERROR_BASE_NUM + 9)  ///< Invalid Length
#define KM_ERROR_INVALID_FLAGS               (KM_ERROR_BASE_NUM + 10) ///< Invalid Flags
#define KM_ERROR_INVALID_DATA                (KM_ERROR_BASE_NUM + 11) ///< Invalid Data
#define KM_ERROR_DATA_SIZE                   (KM_ERROR_BASE_NUM + 12) ///< Invalid Data size
#define KM_ERROR_TIMEOUT                     (KM_ERROR_BASE_NUM + 13) ///< Operation timed out
#define KM_ERROR_NULL                        (KM_ERROR_BASE_NUM + 14) ///< Null Pointer
#define KM_ERROR_FORBIDDEN                   (KM_ERROR_BASE_NUM + 15) ///< Forbidden Operation
#define KM_ERROR_INVALID_ADDR                (KM_ERROR_BASE_NUM + 16) ///< Bad Memory Address
#define KM_ERROR_BUSY                        (KM_ERROR_BASE_NUM + 17) ///< Busy
#define KM_ERROR_CONN_COUNT                  (KM_ERROR_BASE_NUM + 18) ///< Maximum connection count exceeded.
#define KM_ERROR_RESOURCES                   (KM_ERROR_BASE_NUM + 19) ///< Not enough resources for operation

#define KM_ERROR_MOTOR_DISABLE               (KM_ERROR_BASE_NUM + 20) ///< Not enough resources for operation
#define KM_ERROR_MOTOR_OVER_TEMPERATURE      (KM_ERROR_BASE_NUM + 21) ///< Not enough resources for operation

#ifdef __cplusplus
}
#endif
#endif // KM_ERROR_H__



