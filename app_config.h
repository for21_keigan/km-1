#ifndef APP_CONFIG_H
#define APP_CONFIG_H

// sdk_config.h に対する差分で設定をまとめる。
// このファイルはsdk_config.hから読み込まれる。そのために、C/C++で、USE_APP_CONFIG を定義しておくこと。

// IO設定を読み込む。#include "km1_io_definition.h"


// ====
// BLE Services by Nordic
// device information serviceを使う。
#define BLE_DIS_ENABLED 1
// peer manager を使う。
#define PEER_MANAGER_ENABLED 1
// ble advertising を使う。
#define BLE_ADVERTISING_ENABLED 1

// ===
// SAADC nRF52のSAADCを使用する。
#define SAADC_ENABLED 1 // 電流取得

// TIMERを使用する。
#define TIMER_ENABLED 1

//#define TIMER0_ENABLED  1 // --> SoftDevice によりブロックされている
#define TIMER1_ENABLED  1 // CURRENT
#define TIMER2_ENABLED  1 // POSITION (AS5048A encoder)
#define TIMER3_ENABLED  1 // CONTROL

// PPIを使用する。
#define PPI_ENABLED 1

// PWMを使用する。
#define PWM_ENABLED 1
#define PWM0_ENABLED 1 // モーターPWM用
#define PWM1_ENABLED 1 // LED WS2812B用

// ===
// SPIを使用する。
#define SPI_ENABLED  1
#define SPI0_ENABLED 1
#define SPI0_USE_EASY_DMA 1
#define SPI1_ENABLED 1
#define SPI1_USE_EASY_DMA 1

// <33554432=> 125 kHz
// <67108864=> 250 kHz
// <134217728=> 500 kHz
// <268435456=> 1 MHz
// <536870912=> 2 MHz
// <1073741824=> 4 MHz
// <2147483648=> 8 MHz

// ===
// TWIS。(TWI Slave : I2C compatible)
#define TWIS_ENABLED 1
// 周波数。26738688=> 100k, 67108864=> 250k, 104857600=> 400k
//#define TWI_DEFAULT_CONFIG_FREQUENCY 104857600
#define TWI_DEFAULT_CONFIG_FREQUENCY 26738688

// TWIS0を使用
#define TWIS0_ENABLED 0

// UARTを使用
#define UART_ENABLED 1

// ===
// ライブラリ
// スケジューラ
#define APP_SCHEDULER_ENABLED 1
#define APP_TIMER_CONFIG_USE_SCHEDULER 0 // timer でスケジューラを使う→ playback 時の fatal 問題のためどうするか
// FSTORAGE
#define FSTORAGE_ENABLED 1
// FDS
#define FDS_ENABLED 1
// 乱数生成
#define RNG_ENABLED 1
#define NRF_QUEUE_ENABLED 1 // nrf_drv_rng.hで使用

// ===
// ログ
#define NRF_LOG_ENABLED 1
#define NRF_LOG_DEFERRED 1 //1 // ログの処理をidle時に行う （= mainループで NRF_LOG_FLUSH()が必要）
#define NRF_LOG_BACKEND_SERIAL_USES_UART 0
#define NRF_LOG_BACKEND_SERIAL_USES_RTT  1
#define NRF_LOG_DEFERRED_BUFSIZE 512
// <0=> Off
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
#define NRF_LOG_DEFAULT_LEVEL 4

#endif // APP_CONFIG_H
