#ifndef factory_settings_h
#define factory_settings_h
#include "app_util_platform.h"

uint32_t fa_settings_check();
void fa_settings_phase_diff();

#endif // factory_settings_h