#ifndef SPI_IMU_MPU6500_H
#define SPI_IMU_MPU6500_H

#include "nrf_drv_spi.h"
#include "km_io_definition.h"
#include "app_util_platform.h"

//nrf_drv_spi_evt_handler_t mpu6500_spi_event_handler(nrf_drv_spi_evt_t const * event);


bool imu_mpu6500_get_sample(void);
bool imu_mpu6500_init(void);

/*
void km_mpu6500_write(uint8_t register_address, uint8_t value);
void km_mpu6500_read(uint8_t register_address, uint8_t * destination, uint8_t number_of_bytes);

void km_mpu6500_read_who();
*/

#endif // SPI_IMU_MPU6500_H

