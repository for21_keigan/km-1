/*
#include "spi2_manager.h"
#include "spi_imu_mpu6500.h"
#include "nrf_error.h"
#include "nrf_log.h"


static const nrf_drv_spi_t spi2 = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE_FLASH_S25FL_IMU_MPU6500); // SPI instance. 

const nrf_drv_spi_t *spi2_get_pointer(void){
	
	return &spi2;
}


typedef struct
{
    spi2_device_t device;

} spi2_state_t;


spi2_state_t spi2_state = {

    .device = SPI2_DEVICE_UNINIT
};


nrf_drv_spi_config_t s25fl_spi_config = {

    .sck_pin = SPI2_SCK_PIN,
    .mosi_pin = SPI2_MOSI_PIN,
    .miso_pin = SPI2_MISO_PIN,
    .ss_pin = SPI2_SS_PIN_S25FL,
    .irq_priority = APP_IRQ_PRIORITY_LOW,
    .orc = 0xCC,
    .frequency = NRF_DRV_SPI_FREQ_8M, // up to max 108MHz
    .mode = SPI_MODE_S25FL,
    .bit_order = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
};

nrf_drv_spi_config_t mpu6500_spi_config = {
	.sck_pin  = SPI2_SCK_PIN,
	.mosi_pin = SPI2_MOSI_PIN,
	.miso_pin = SPI2_MISO_PIN,
	.ss_pin   =  SPI2_SS_PIN_MPU6500, //NRF_DRV_SPI_PIN_NOT_USED,
	.irq_priority = APP_IRQ_PRIORITY_LOW,
	.orc          = 0xCC,
	.frequency    = NRF_DRV_SPI_FREQ_1M,
	.mode         = SPI_MODE_MPU6500,
	.bit_order    = NRF_DRV_SPI_BIT_ORDER_MSB_FIRST,
};


void spi2_init_with_device(spi2_device_t device)
{

    if (device == SPI2_DEVICE_FLASH_S25FL)
    {

        if (spi2_state.device == SPI2_DEVICE_FLASH_S25FL)
        {
            NRF_LOG_INFO("SPI2 is already configured for FLASH_S25FL.\r\n");
        }
        else
        {
            nrf_drv_spi_uninit(&spi2);
            //  "event_handler" 引数が NULL の場合は、ブロッキングモードとなる。（transferが終わるまで他の操作をしない）
            APP_ERROR_CHECK(nrf_drv_spi_init(&spi2, &s25fl_spi_config, NULL, NULL)); //s25fl_spi_event_handler)); //
            spi2_state.device = SPI2_DEVICE_FLASH_S25FL;
        }
    } 
    else if (device == SPI2_DEVICE_IMU_MPU6500)
    {
        
        if (spi2_state.device == SPI2_DEVICE_IMU_MPU6500)
        {
            NRF_LOG_INFO("SPI2 is already configured for IMU_MPU6500.\r\n");
        }
        else
        {
            nrf_drv_spi_uninit(&spi2);
            APP_ERROR_CHECK(nrf_drv_spi_init(&spi2, &mpu6500_spi_config, mpu6500_spi_event_handler, NULL)); //s25fl_spi_event_handler)); //
            spi2_state.device = SPI2_DEVICE_IMU_MPU6500;
        }

    } else {
            nrf_drv_spi_uninit(&spi2);
            spi2_state.device = SPI2_DEVICE_UNINIT;
    }
}
*/

