/**
 * @brief Keigan Motor KM-1 main file.
 *
 * This file contains the source code for a sample server application using the LED Button service.
 */

#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h" 
#include "ble_conn_params.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "app_button.h"

// Implement peer manager
#include "fstorage.h"
#include "fds.h"
#include "peer_manager.h"
#include "ble_conn_state.h"

#include "app_util_platform.h"

#include "nrf_drv_clock.h"
#include "nrf_delay.h"

#include "nrf_drv_gpiote.h"

#include "fast_math.h"
#include "km_io_definition.h"

#include "km_ble_definition.h"
#include "km_ble_motor.h"
#include "buttonless_dfu_service.h" 
#include "device_information_service.h"

#include "km_gap_params.h"
#include "km_conn_params.h"
#include "km_ble_stack.h"
#include "km_gatt_mtu.h"
#include "km_peer_manager.h"
#include "km_advertising.h"
#include "km_ble_stack.h"

#include "km_buttons.h"
#include "led_ws2812b.h"
#include "spi_encoder_as5048a.h"

#include "spi2_manager.h"
//#include "spi_imu_mpu6500.h"
#include "spi_flash_s25fl.h"
#include "test_flash_s25fl.h"

#include "motor_control.h"
#include "teaching_playback.h"
#include "demo.h"

#include "factory_settings.h"

// #include <app_scheduler.h>

#include "test_mode_definition.h"

#define NRF_LOG_MODULE_NAME "APP"
#include "nrf_log.h"
#include "nrf_log_ctrl.h" 

#include "SEGGER_RTT.h"

#define DEAD_BEEF                       0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */



/**@brief Function for DCDC initialization.
 *
 * @details Enable DCDC Converter used by the application.
 */

static void dcdc_init(void)
{
    // DCDC コンバータを起動直後にONにする
    // board_km1.h 内で定義　#define DCDC_ENABLE_PIN 30
    // #include "nrf_drv_gpiote.h"
    nrf_gpio_cfg_output(DCDC_ENABLE_PIN);
    NRF_GPIO->OUTSET = 1 << DCDC_ENABLE_PIN;
    nrf_gpio_cfg_output(DCDC_ENABLE_PIN2);
    NRF_GPIO->OUTSET = 1 << DCDC_ENABLE_PIN2;
}



/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}



/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    ret_code_t err_code;

    //err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    //APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    //err_code = bsp_btn_ble_sleep_mode_prepare();
    //APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}




/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the scheduler in the main loop after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
// BLEイベントを各モジュールに分配する
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    // on_ble_evt(p_ble_evt);　不要？
	ble_conn_state_on_ble_evt(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    ble_motor_on_ble_evt(p_ble_evt);
	pm_on_ble_evt(p_ble_evt);
	ble_advertising_on_ble_evt(p_ble_evt);
    gatt_mtu_on_ble_evt(p_ble_evt);    
    buttonless_dfu_on_ble_event(p_ble_evt);
}

/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
    // Dispatch the system event to the fstorage module, where it will be
    // dispatched to the Flash Data Storage (FDS) module.
    fs_sys_event_handler(sys_evt);

    // Dispatch to the Advertising module last, since it will check if there are any
    // pending flash operations in fstorage. Let fstorage process system events first,
    // so that it can report correctly to the Advertising module.
    ble_advertising_on_sys_evt(sys_evt);
}



static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);
}



/**@brief Function for application main entry.
 */
int main(void)
{
	nrf_gpio_cfg_output(30);
    NRF_GPIO->OUTSET = 1 << 30;

    uint32_t err_code;

    err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);


    nrf_clock_lf_cfg_t clock_lf_cfg ={.source        = NRF_CLOCK_LF_SRC_XTAL,            \
                                 .rc_ctiv       = 0,                                \
                                 .rc_temp_ctiv  = 0,                                \
                                 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM};

    SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);

	NRF_LOG_INFO("KM-1 started.\r\n");
	char *p_my_text = "Testing string 11111111";
    NRF_LOG_INFO(" %s \r\n", (uint32_t)p_my_text);
			nrf_delay_ms(100);

	char *p_my_text2 = "Testing string 22222222";
	SEGGER_RTT_WriteString(0, p_my_text2);
	
//	ret_code_t err_code;
//	
//    // 電源供給
//    dcdc_init();
//	
//	// ログ
//	log_init();
/*
	
	
    //char *p_my_text = "Testing string";
    //NRF_LOG_INFO(" %s \r\n", (uint32_t)p_my_text);
	
	// タイマーモジュール、スケジューラ設定。
#ifdef APP_SCHEDULER_ENABLED
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
#endif
	// app_timer でスケジューラを有効にする場合、sdk_config.h または app_config.h で、 APP_TIMER_CONFIG_USE_SCHEDULER　を　1 にすれば以下の宣言だけでよい
    app_timer_init(); //SDK13より APP_TIMER_INIT から変更。

	
	// 各デバイス
	encoder_as5048a_init();
	//flash_s25fl_init_with_nonblocking(); //フラッシュをノンブロッキングモードで初期化
	led_ws2812b_init();
	
	// ボタン
    buttons_init();

    //三角函数テーブル作成
	init_fast_math(); 

    // 高周波外部クロック
    NRF_LOG_INFO("Clock init.\r\n");
	err_code = nrf_drv_clock_init();
    nrf_drv_clock_hfclk_request(NULL);

    // BLEサービスの準備
	NRF_LOG_INFO("fs init.\r\n");
	err_code = fs_init(); // peer_manager 初期化前に実行必要
    APP_ERROR_CHECK(err_code);
	
	NRF_LOG_INFO("ble stack init.\r\n");	
	ble_stack_init(sys_evt_dispatch, ble_evt_dispatch);

	NRF_LOG_INFO("gap params init.\r\n");
    gap_params_init();
	gatt_mtu_init();	
	
	NRF_LOG_INFO("Advertising init.\r\n");
    advertising_init();
	

	// 各BLEサービスの初期化
	NRF_LOG_INFO("Services init.\r\n");
	ble_motor_init();
	
	device_information_service_init();
	
	buttonless_dfu_service_init();
    
    conn_params_init();
	
	NRF_LOG_INFO("peer manager init.\r\n");
	peer_manager_init(true); // fs_init()　の後でなければならない
    
	// アドバタイジングを開始する。
	NRF_LOG_INFO("Advertiging start..\r\n");
    advertising_start();
    
	
    // モーター制御周り初期化
    motor_gpio_config();
	motor_timer_init();
    motor_pwm_init();
    motor_saadc_init();
    motor_ppi_init();
	teaching_playback_init();
	
	// モーター測定値の取得スタート
    motor_start_position_timer(); 
	motor_measurement_timer_start();
	
	
	


// 工場出荷 初期状態の際にチェックを行い、必要な値をフラッシュに書き込む
	fa_settings_check();
	
#ifdef MOTOR_TEST

    motor_enable(); // TODO ここにはないほうが
	float target_velocity = RPM_TO_RADPERSEC(SPEED_TEST_RPM);
	NRF_LOG_INFO("target_speed: %d\r\n", (int)(target_velocity*10000));
    motor_run_at_velocity(target_velocity); // (float)RPM_TO_TPMS(SPEED_TEST_RPM);

#endif 

#ifdef FLASH_TEST

    

#endif
	*/
	
    // Enter main loop.
    for (;;)
    {
		//NRF_LOG_FLUSH(); // たまったログを idle 状態のときに吐き出していく
		
        if (NRF_LOG_PROCESS() == false)
        {
			//アプリケーション割り込みイベント待ち (sleep状態)
			ret_code_t err_code = sd_app_evt_wait();
			APP_ERROR_CHECK(err_code);
			
			    char *p_my_text = "Testing string 11111111";
    NRF_LOG_INFO(" %s \r\n", (uint32_t)p_my_text);
			nrf_delay_ms(100);
        }
		// スケジューラのタスク実行
        //app_sched_execute();
    }


	
}


/**
 * @}
 */
