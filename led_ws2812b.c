#include "led_ws2812b.h"
#include "km_io_definition.h"
#include "km_motor_definition.h"
#include "nrf_drv_pwm.h"
#include "nrf_log.h"

rgb_led_t rgb_led = {
    .green = 0,
    .red = 0,
    .blue = 0};

/* WS2812B LED PWM */
static nrf_drv_pwm_t led_pwm1 = NRF_DRV_PWM_INSTANCE(1);

uint16_t led_seq_values[RGB_BUFFER_SIZE] = {0};

/*
uint16_t led_seq_values[] = 
{
    B_Z, B_Z, B_Z, B_Z, B_Z, B_Z, B_Z, B_Z,
    B_Z, B_Z, B_Z, B_Z, B_Z, B_Z, B_Z, B_Z,
    B_Z, B_Z, B_Z, B_Z, B_Z, B_Z, B_Z, B_Z
};



uint16_t led_seq_values[] = 
{
    B_L, B_L, B_L, B_L, B_L, B_L, B_L, B_L,
    B_H, B_H, B_H, B_H, B_H, B_H, B_H, B_H, 
    B_L, B_L, B_L, B_L, B_L, B_L, B_L, B_L
};
*/

static nrf_pwm_sequence_t const l_seq =
    {
        .values.p_common = led_seq_values,
        .length = NRF_PWM_VALUES_LENGTH(led_seq_values),
        .repeats = 0,
        .end_delay = 0};

void led_ws2812b_init()
{
    uint32_t err_code;
    nrf_drv_pwm_config_t const config0 =
        {
            .output_pins =
                {
                    LED_WS2812B_PIN | NRF_DRV_PWM_PIN_INVERTED, // channel 0 NRF_DRV_PWM_PIN_INVERTED: 1st edge is falling (Hi to Lo)
                    NRF_DRV_PWM_PIN_NOT_USED,                   // | NRF_DRV_PWM_PIN_INVERTED, // channel 1
                    NRF_DRV_PWM_PIN_NOT_USED,                   // | NRF_DRV_PWM_PIN_INVERTED, // channel 2
                    NRF_DRV_PWM_PIN_NOT_USED},
            .irq_priority = APP_IRQ_PRIORITY_LOW,
            .base_clock = NRF_PWM_CLK_16MHz,
            .count_mode = NRF_PWM_MODE_UP,
            .top_value = LED_PWM_TOP, // Must be more than each sequence value (TODO),
            .load_mode = NRF_PWM_LOAD_COMMON,
            .step_mode = NRF_PWM_STEP_AUTO, // When the NRF_PWM_TASK_NEXTSTEP task is triggered.
        };

    err_code = nrf_drv_pwm_init(&led_pwm1, &config0, NULL);
    APP_ERROR_CHECK(err_code);

	led_ws2812b_drive_set_color(LED_DEF_R, LED_DEF_G, LED_DEF_B);
    //nrf_drv_pwm_simple_playback(&led_pwm1, &l_seq, 1, NRF_DRV_PWM_FLAG_STOP);
}

void led_ws2812b_uninit(void)
{
    nrf_drv_pwm_uninit(&led_pwm1);
}

void ws2812b_led_pwm_update(void)
{
    nrf_drv_pwm_simple_playback(&led_pwm1, &l_seq, 1, NRF_DRV_PWM_FLAG_STOP);
}

/*
  LED RGB 0～255の各値 を PWMシーケンスに変換する
*/
/*
void ws2812b_led_convert_rgb_to_seq(rgb_led_t * led)
{
     rgb_led_t * p = led;	
        NRF_LOG_PRINTF("RGB: %d, %d, %d\n ", led->red, led->green, led->blue);
     uint32_t ledBit = ((p->green) << 16) | ((p->red) << 8) | (p->blue) ;
    NRF_LOG_PRINTF("ledBit: %d\n ", ledBit);
    for (uint8_t i = RGB_BUFFER_SIZE; i > 0 ; i--){
        if( (ledBit >> (i -1)) & 1 ){
            led_seq_values[RGB_BUFFER_SIZE - i] = B_H;
        } else {
            led_seq_values[RGB_BUFFER_SIZE - i] = B_L;
        }
    }  
}
*/
void led_ws2812b_convert_rgb_to_seq(uint8_t red, uint8_t green, uint8_t blue)
{
    uint32_t ledBit = (green << 16) | (red << 8) | (blue);
    //NRF_LOG_PRINTF("RGB: %d, %d, %d\n ", red, green, blue);
    //NRF_LOG_PRINTF("ledBit: %d\n ", ledBit);
    for (uint8_t i = RGB_BUFFER_SIZE; i > 0; i--)
    {
        if ((ledBit >> (i - 1)) & 1)
        {
            led_seq_values[RGB_BUFFER_SIZE - i] = B_H;
        }
        else
        {
            led_seq_values[RGB_BUFFER_SIZE - i] = B_L;
        }
    }
}

void led_ws2812b_drive_set_color(uint8_t red, uint8_t green, uint8_t blue)
{

    //NRF_LOG_PRINTF("RGB: %d, %d, %d\n ", red, green, blue);
    rgb_led.green = green;
    rgb_led.red = red;
    rgb_led.blue = blue;
    //NRF_LOG_PRINTF("address: %d\r\n", p);

    led_ws2812b_convert_rgb_to_seq(red, green, blue);
    //NRF_LOG("LED SET COLOR\n");

    /*
	for(uint8_t i = 0; i < RGB_BUFFER_SIZE; i++ ){
       NRF_LOG_PRINTF("%d\n ", led_seq_values[i]);
    }
	*/

    ws2812b_led_pwm_update();
}

void led_ws2812b_drive_set_color_default(void){
	
	led_ws2812b_drive_set_color(LED_DEF_R, LED_DEF_G, LED_DEF_B);
	
}

void led_ws2812b_drive_clear()
{
    led_ws2812b_drive_set_color(0, 0, 0);
}

/* dim を乗じて配列の値を更新していく */
void led_ws2812b_drive_dim(rgb_led_t *led_array, uint16_t num_leds, float dim)
{
    rgb_led_t *p = led_array;
    p->green *= dim;
    p->red *= dim;
    p->blue *= dim;
}
